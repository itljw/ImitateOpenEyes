package com.itljw.openeyes

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.multidex.MultiDexApplication
import com.itljw.openeyes.util.CrashHandler
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import com.scwang.smartrefresh.header.DeliveryHeader
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.footer.ClassicsFooter
import java.util.*
import kotlin.properties.Delegates


/**
 * Created by JW on on 2018/7/10 22:24.
 * Email : 1481013718@qq.com
 * Description :
 */
class AppContext : MultiDexApplication() {

    companion object {
        var mContext: Context by Delegates.notNull()
            private set

        val mActivityStack: Stack<Activity> by lazy {
            Stack<Activity>()
        }

        /**
         * 退出应用
         */
        fun exit() {
            mActivityStack.forEach {
                it.finish()
            }
            mActivityStack.clear() // 清空栈
        }
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this

        initLogger()
        initSmartRefreshLayout()
        initCrashHandler()

        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity?) {}

            override fun onActivityResumed(activity: Activity?) {}

            override fun onActivityStarted(activity: Activity?) {}

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {}

            override fun onActivityStopped(activity: Activity?) {}

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                addActivity(activity)
            }

            override fun onActivityDestroyed(activity: Activity?) {
                finishActivity(activity)
            }
        })

    }

    private fun initCrashHandler() {
        CrashHandler.instance.init(mContext)
    }

    private fun initSmartRefreshLayout() {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context, _ ->
            //            layout.setPrimaryColorsId(R.color.colorPrimary, android.R.color.white)//全局设置主题颜色
            DeliveryHeader(context)
        }
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
            //指定为经典Footer，默认是 BallPulseFooter
            ClassicsFooter(context).setTextSizeTitle(14f)
        }
    }

    /**
     * init logger
     */
    private fun initLogger() {
        val formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)  // (Optional) Whether to show thread info or not. Default true
                .methodCount(3)          // 决定打印多少行（每一行代表一个方法）默认：2
                .methodOffset(7)        // (Optional) Hides internal method calls up to offset. Default 5
                .tag(AppContext::class.java.simpleName)   // (Optional) Global tag for every log. Default PRETTY_LOGGER
                .build()

        Logger.addLogAdapter(AndroidLogAdapter(formatStrategy))
    }

    /**
     * 添加Activity
     */
    private fun addActivity(activity: Activity?) {
        mActivityStack.add(activity)
    }

    /**
     * 关闭Activity
     */
    private fun finishActivity(activity: Activity?) {
        mActivityStack.remove(activity)
        activity?.finish()
    }


}