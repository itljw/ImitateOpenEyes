package com.itljw.openeyes.constants

/**
 * Created by JW on on 2018/7/14 13:27.
 * Email : 1481013718@qq.com
 * Description :
 */
object Const {
    const val BASE_URL = "http://baobab.kaiyanapp.com/api/"

    // 专题
    const val URL_SPECIAL_TOPICS    =       BASE_URL + "v3/specialTopics"
    // 全部作者
    const val URL_AUTHOR_LIST       =       BASE_URL + "v4/pgcs/all"
    // 我的关注
    const val URL_USER_FOLLOW       =       BASE_URL + "v2/follow/newlist?uid="
    // 首页Tab的List数据
    const val URL_HOME_TAB_LIST     =       BASE_URL + "v5/index/tab/list"
    // 排行榜Tab数据
    const val URL_RANK_TAB_LIST     =       BASE_URL + "v4/rankList"

}