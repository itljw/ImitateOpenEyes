package com.itljw.openeyes.constants

import android.content.Context
import android.net.Uri
import com.itljw.openeyes.event.NotifyEvent
import com.itljw.openeyes.ui.common.ContainerWithBackActivity
import com.itljw.openeyes.ui.common.FragmentContainerActivity
import com.itljw.openeyes.ui.common.TabPagerActivity
import com.itljw.openeyes.ui.home.AllCategoriesActivity
import com.itljw.openeyes.ui.home.AuthorDetailActivity
import com.itljw.openeyes.ui.home.CategoryDetailActivity
import com.itljw.openeyes.ui.home.CommentActivity
import com.itljw.openeyes.ui.video.VideoTagsActivity
import isNumeric
import org.greenrobot.eventbus.EventBus

/**
 * Created by JW on on 2019/4/21 01:19.
 * Email : 1481013718@qq.com
 * Description :
 */
object ActionType {

    private const val TYPE_ACTION_CATEGORIES = "categories" // 查看全部分类
    const val TYPE_ACTION_CAMPAIGN = "campaign" // 专题
    private const val TYPE_ACTION_RANK_LIST = "ranklist" // 排行榜
    private const val TYPE_ACTION_TAG = "tag" // 标签
    private const val TYPE_ACTION_CATEGORY = "category" // 分类
    private const val TYPE_ACTION_WEBVIEW = "webview" // WebView
    private const val TYPE_ACTION_AUTHOR = "pgcs" // 全部作者
    private const val TYPE_ACTION_FEED = "feed" // 首页信息流
    private const val TYPE_ACTION_COMMON = "common" // 携带url
    private const val TYPE_ACTION_PGC = "pgc" // 作者详情
    private const val TYPE_ACTION_USER_FOLLOW = "userFollow" // 我的关注
    private const val TYPE_ACTION_REPLIES = "replies" // 评论回复

    fun handleAction(mContext: Context, actionUrl: String?) {
        actionUrl?.apply {
            val parseUri = Uri.parse(this)

            when (parseUri.authority) {
                ActionType.TYPE_ACTION_CATEGORIES -> { // 全部分类
                    AllCategoriesActivity.launch(mContext)
                }

                ActionType.TYPE_ACTION_CAMPAIGN -> { // 专题
                    val title = parseUri.getQueryParameter("title")
                    ContainerWithBackActivity.launch(mContext, title, Const.URL_SPECIAL_TOPICS)
                }

                ActionType.TYPE_ACTION_RANK_LIST -> { // 排行
                    TabPagerActivity.launch(mContext, Const.URL_RANK_TAB_LIST, "Eyepetizer")
                }

                ActionType.TYPE_ACTION_TAG -> { // 标签
                    val pathSegments = parseUri.pathSegments
                    if (pathSegments.size > 0) {
                        val tagId = pathSegments[0]
                        if (tagId.isNumeric()) {
                            VideoTagsActivity.launch(mContext, tagId.toInt())
                        }
                    }
                }

                ActionType.TYPE_ACTION_CATEGORY -> { // 分类
                    val categoryId = parseUri.lastPathSegment
                    if (categoryId.isNumeric()) {
                        CategoryDetailActivity.launch(mContext, categoryId.toInt())
                    }
                }

                ActionType.TYPE_ACTION_WEBVIEW -> { // webview
                    // eyepetizer://webview/?title=&url=https%3A%2F%2Fwww.kaiyanapp.com%2Ftopic_article.html%3Fnid%3D90%26cookie%3D%26shareable%3Dtrue
                    val title = parseUri.getQueryParameter("title") ?: ""
                    val url = parseUri.getQueryParameter("url")
                    FragmentContainerActivity.launch(mContext, title, url)
                }

                ActionType.TYPE_ACTION_AUTHOR -> { // 全部作者
                    ContainerWithBackActivity.launch(mContext, "全部作者", Const.URL_AUTHOR_LIST)
                }

                ActionType.TYPE_ACTION_FEED -> { // 首页信息流
                    val tabIndex = parseUri.getQueryParameter("tabIndex")
                    EventBus.getDefault().post(NotifyEvent(tabIndex.toInt()))
                }

                ActionType.TYPE_ACTION_COMMON -> { // common
                    val title = parseUri.getQueryParameter("title")
                    val url = parseUri.getQueryParameter("url")

                    val jumpType = parseUri.lastPathSegment
                    if ("tab" == jumpType) { // 如最受欢迎
                        TabPagerActivity.launch(mContext, url, title)
                    } else { // 猜你喜欢
                        ContainerWithBackActivity.launch(mContext, title, url)
                    }

                }

                ActionType.TYPE_ACTION_PGC -> { // 作者详情
                    // eyepetizer://pgc/detail/2162/?title=开眼广告精选&userType=PGC&tabIndex=1
                    val userType = parseUri.getQueryParameter("userType")
                    val tabIndexStr = parseUri.getQueryParameter("tabIndex")

                    val authorId = parseUri.lastPathSegment

                    if (authorId.isNumeric()) {
                        AuthorDetailActivity.launch(
                                mContext, authorId.toInt(),
                                userType,
                                if (tabIndexStr.isNumeric()) tabIndexStr.toInt() else 0
                        )
                    }
                }

                ActionType.TYPE_ACTION_USER_FOLLOW -> { // 我的关注
                    // eyepetizer://userFollow/301178695
                    val uid = parseUri.lastPathSegment

                    if (uid.isNumeric()) {
                        TabPagerActivity.launch(mContext, Const.URL_USER_FOLLOW + uid, "所有关注")
                    }
                }

                ActionType.TYPE_ACTION_REPLIES -> { // 评论回复
                    val videoId = parseUri.getQueryParameter("videoId")
                    val top = parseUri.getQueryParameter("top")
                    val videoTitle = parseUri.getQueryParameter("videoTitle")
                    val type = parseUri.lastPathSegment
                    if ("video" == type) {
                        CommentActivity.launch(mContext, videoId, top, type, videoTitle)
                    }

                }
            }

        }
    }

}