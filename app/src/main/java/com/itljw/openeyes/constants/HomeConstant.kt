package com.itljw.openeyes.constants

/**
 * Created by JW on on 2018/7/21 22:54.
 * Email : 1481013718@qq.com
 * Description :
 */
object HomeConstant {

    const val TYPE_SQUARE_CARD_COLLECTION = "squareCardCollection"
    const val TYPE_BANNER = "banner"
    const val TYPE_TEXT_CARD = "textCard"
    const val TYPE_PICTURE_FOLLOW_CARD = "pictureFollowCard"
    const val TYPE_FOLLOW_CARD = "followCard"
    const val TYPE_VIDEO_SMALL_CARD = "videoSmallCard"
    const val TYPE_HORIZONTAL_SCROLL_CARD = "horizontalScrollCard"
    const val TYPE_BRIEF_CARD = "briefCard"
    const val TYPE_VIDEO_COLLECTION_WITH_BRIEF = "videoCollectionWithBrief"
    const val TYPE_DYNAMIC_INFO_CARD = "DynamicInfoCard"
    const val TYPE_AUTO_PLAY_FOLLOW_CARD = "autoPlayFollowCard"
    const val TYPE_REPLY = "reply"

    const val ITEM_TYPE_SQUARE_CARD = 0x11
    const val ITEM_TYPE_BANNER = 0x22
    const val ITEM_TYPE_TEXT_CARD = 0x33
    const val ITEM_TYPE_PICTURE_FOLLOW_CARD = 0x44
    const val ITEM_TYPE_FOLLOW_CARD = 0x55
    const val ITEM_TYPE_VIDEO_SMALL_CARD = 0x66
    const val ITEM_TYPE_HORIZONTAL_SCROLL_CARD = 0x77
    const val ITEM_TYPE_BRIEF_CARD = 0x88
    const val ITEM_TYPE_VIDEO_COLLECTION_WITH_BRIEF = 0x99
    const val ITEM_TYPE_DYNAMIC_INFO_CARD = 0x100
    const val ITEM_TYPE_AUTO_PLAY_FOLLOW_CARD = 0x110
    const val ITEM_TYPE_REPLY = 0x120


}