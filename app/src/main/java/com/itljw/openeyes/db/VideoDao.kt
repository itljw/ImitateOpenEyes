package com.itljw.openeyes.db

import android.arch.persistence.room.*

/**
 * Created by JW on on 2018/5/28 21:17.
 * Email : 1481013718@qq.com
 * Description :
 */
@Dao
interface VideoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addVideo(video: Video)

    @Query("SELECT * FROM video_table ORDER BY browse_time DESC")
    fun queryAll(): List<Video>

    @Update
    fun updateVideo(video: Video)

    @Delete
    fun deleteVideo(video: Video)

    @Query("SELECT * FROM video_table ORDER BY browse_time DESC LIMIT ((:page - 1) * :limit),:limit")
    fun queryLimitVideo(page: Int, limit: Int): List<Video>

}