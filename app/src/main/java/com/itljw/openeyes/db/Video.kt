package com.itljw.openeyes.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by JW on on 2018/5/28 21:03.
 * Email : 1481013718@qq.com
 * Description :
 */
@Entity(tableName = "video_table")
data class Video(
    /**
     * 视频id
     */
    @PrimaryKey @ColumnInfo(name = "video_id") val videoId: Int,
    /**
     * 视频标题
     */
    @ColumnInfo(name = "video_title") val videoTitle: String,
    /**
     * 封面图
     */
    @ColumnInfo(name = "cover_url") val coverUrl: String,
    /**
     * 最近浏览时间
     */
    @ColumnInfo(name = "browse_time") val browseTime: Long
)