package com.itljw.openeyes.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

/**
 * Created by JW on on 2018/5/28 21:10.
 * Email : 1481013718@qq.com
 * Description :
 */
@Database(
        entities = [Video::class],
        version = 1
)
abstract class AppRoomDatabase : RoomDatabase() {

    companion object {

        private const val DB_NAME = "openEyes.db"

        @Volatile
        private var instance: AppRoomDatabase? = null

        // 单例获取
        fun getInstance(context: Context): AppRoomDatabase =
                instance ?: synchronized(this) {
                    instance ?: buildDatabase(context).apply { instance = this }
                }

        private fun buildDatabase(context: Context): AppRoomDatabase {
            return Room.databaseBuilder(
                    context,
                    AppRoomDatabase::class.java,
                    DB_NAME
            ).build()
        }
    }

    abstract fun videoDao(): VideoDao

}