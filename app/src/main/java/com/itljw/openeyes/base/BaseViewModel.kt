package com.itljw.openeyes.base

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.HttpResult
import com.itljw.openeyes.http.IObserver
import com.itljw.openeyes.http.ResponseSource

/**
 * Created by JW on on 2018/7/15 10:54.
 * Email : 1481013718@qq.com
 * Description :
 */
open class BaseViewModel : ViewModel() {

    private val mResourceList = ArrayList<ResponseSource<*>>()

    fun <T> addResponseResource(
            liveData: LiveData<HttpResult<T>>,
            observer: IObserver<T> = object : BaseObserver<T>() {}
    ) {
        mResourceList.add(ResponseSource(liveData, observer).also { it.addObserve() })
    }

    override fun onCleared() {
        super.onCleared()
        for (resource in mResourceList) {
            resource.removeObserve()
        }
    }

}