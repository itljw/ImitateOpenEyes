package com.itljw.openeyes.base.fragmentation

import android.os.Bundle
import me.yokeyword.fragmentation.SwipeBackLayout
import me.yokeyword.fragmentation_swipeback.core.ISwipeBackActivity
import me.yokeyword.fragmentation_swipeback.core.SwipeBackActivityDelegate


/**
 * Created by JW on on 2018/7/29 22:54.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class SwipeBackActivity : BaseSupportActivity(), ISwipeBackActivity {

    private val mSwipeBackDelegate by lazy { SwipeBackActivityDelegate(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSwipeBackDelegate.onCreate(savedInstanceState)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        mSwipeBackDelegate.onPostCreate(savedInstanceState)
    }

    override fun getSwipeBackLayout(): SwipeBackLayout = mSwipeBackDelegate.swipeBackLayout

    override fun setEdgeLevel(edgeLevel: SwipeBackLayout.EdgeLevel?) =
            mSwipeBackDelegate.setEdgeLevel(edgeLevel)

    override fun setEdgeLevel(widthPixel: Int) = mSwipeBackDelegate.setEdgeLevel(widthPixel)

    /**
     * 限制SwipeBack的条件,默认栈内Fragment数 <= 1时 , 优先滑动退出Activity , 而不是Fragment
     * @return true: Activity优先滑动退出;  false: Fragment优先滑动退出
     */
    override fun swipeBackPriority(): Boolean = mSwipeBackDelegate.swipeBackPriority()

    /**
     * 是否可滑动
     * @param enable
     */
    override fun setSwipeBackEnable(enable: Boolean) = mSwipeBackDelegate.setSwipeBackEnable(enable)
}