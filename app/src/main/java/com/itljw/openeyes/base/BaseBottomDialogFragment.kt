package com.itljw.openeyes.base

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itljw.openeyes.R
import com.itljw.openeyes.base.interf.IDialog

/**
 * Created by JW on on 2019/3/8 00:08.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class BaseBottomDialogFragment : DialogFragment(),IDialog {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setStyle(STYLE_NO_TITLE, R.style.BottomDialogStyle)
        return inflater.inflate(getLayoutId(),container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doBusiness()
    }

}