package com.itljw.openeyes.base.interf

import android.support.annotation.LayoutRes

/**
 * Created by JW on on 2019/3/8 00:09.
 * Email : 1481013718@qq.com
 * Description :
 */
interface IDialog {

    /**
     * 获取布局Id
     */
    @LayoutRes
    fun getLayoutId(): Int

    /**
     * 处理业务
     */
    fun doBusiness()

}