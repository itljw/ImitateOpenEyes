package com.itljw.openeyes.base.fragmentation

import android.view.View
import me.yokeyword.fragmentation.SwipeBackLayout
import me.yokeyword.fragmentation_swipeback.core.ISwipeBackFragment
import me.yokeyword.fragmentation_swipeback.core.SwipeBackFragmentDelegate
import android.os.Bundle
import android.support.annotation.Nullable


/**
 * Created by JW on on 2018/7/31 23:11.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class SwipeBackFragment : SupportFragment(), ISwipeBackFragment {

    private val mSwipeBackDelegate by lazy { SwipeBackFragmentDelegate(this) }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSwipeBackDelegate.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mSwipeBackDelegate.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        mSwipeBackDelegate.onDestroyView()
        super.onDestroyView()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        mSwipeBackDelegate.onHiddenChanged(hidden)
    }

    override fun getSwipeBackLayout(): SwipeBackLayout =
            mSwipeBackDelegate.swipeBackLayout


    override fun attachToSwipeBack(view: View?): View =
            mSwipeBackDelegate.attachToSwipeBack(view)


    override fun setEdgeLevel(edgeLevel: SwipeBackLayout.EdgeLevel?) =
            mSwipeBackDelegate.setEdgeLevel(edgeLevel)


    override fun setEdgeLevel(widthPixel: Int) =
            mSwipeBackDelegate.setEdgeLevel(widthPixel)

    override fun setParallaxOffset(offset: Float) =
            mSwipeBackDelegate.setParallaxOffset(offset)

    override fun setSwipeBackEnable(enable: Boolean) =
            mSwipeBackDelegate.setSwipeBackEnable(enable)

}