package com.itljw.openeyes.base

import android.content.Intent
import android.os.Bundle
import com.itljw.openeyes.R
import com.itljw.openeyes.base.fragmentation.BaseSupportActivity
import com.itljw.openeyes.base.interf.IActivity
import com.jaeger.library.StatusBarUtil

/**
 * Created by JW on on 2018/7/9 23:36.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class BaseActivity: BaseSupportActivity(), IActivity {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        doBeforeContentView()
        setContentView(getLayoutId())

        setStatusBar()
        handleIntent(intent)
        handleObserver()
        doBusiness()
    }

    /**
     * 处理意图Intent
     */
    open fun handleIntent(intent: Intent) {}

    /**
     * 处理观察者监听
     */
    open fun handleObserver() {}

    open fun doBeforeContentView(){}

    open fun setStatusBar(){
        StatusBarUtil.setColor(this, resources.getColor(R.color.color_black))
    }

}