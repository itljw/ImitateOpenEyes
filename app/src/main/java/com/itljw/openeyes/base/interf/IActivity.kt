package com.itljw.openeyes.base.interf

import android.support.annotation.LayoutRes

/**
 * Created by JW on on 2018/7/10 00:28.
 * Email : 1481013718@qq.com
 * Description :
 */
interface IActivity {

    /**
     * 获取布局Id
     */
    @LayoutRes fun getLayoutId():Int

    /**
     * 处理业务
     */
    fun doBusiness()

}