package com.itljw.openeyes.base.fragmentation

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import me.yokeyword.fragmentation.ExtraTransaction
import me.yokeyword.fragmentation.ISupportFragment
import me.yokeyword.fragmentation.SupportFragmentDelegate
import me.yokeyword.fragmentation.SupportHelper
import me.yokeyword.fragmentation.anim.FragmentAnimator


/**
 * Created by JW on on 2018/7/29 23:03.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class SupportFragment : Fragment(), ISupportFragment {

    private val mDelegate by lazy { SupportFragmentDelegate(this) }
    protected var mContext: Context? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
        mDelegate.onAttach(activity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDelegate.onCreate(savedInstanceState)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mDelegate.onActivityCreated(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mDelegate.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        mDelegate.onResume()
    }

    override fun onPause() {
        super.onPause()
        mDelegate.onPause()
    }

    override fun onDestroyView() {
        mDelegate.onDestroyView()
        super.onDestroyView()
    }

    override fun onDestroy() {
        mDelegate.onDestroy()
        super.onDestroy()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        mDelegate.onHiddenChanged(hidden)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        mDelegate.setUserVisibleHint(isVisibleToUser)
    }

    override fun getSupportDelegate(): SupportFragmentDelegate = mDelegate

    override fun setFragmentResult(resultCode: Int, bundle: Bundle?) {
        mDelegate.setFragmentResult(resultCode, bundle)
    }

    override fun onSupportInvisible() = mDelegate.onSupportInvisible()


    override fun onNewBundle(args: Bundle?) = mDelegate.onNewBundle(args)

    /**
     * Perform some extra transactions.
     * 额外的事务：自定义Tag，添加SharedElement动画，操作非回退栈Fragment
     */
    override fun extraTransaction(): ExtraTransaction = mDelegate.extraTransaction()

    override fun onCreateFragmentAnimator(): FragmentAnimator =
            mDelegate.onCreateFragmentAnimator()


    override fun enqueueAction(runnable: Runnable?) =
            mDelegate.enqueueAction(runnable)


    override fun onFragmentResult(requestCode: Int, resultCode: Int, data: Bundle?) =
            mDelegate.onFragmentResult(requestCode, resultCode, data)


    override fun setFragmentAnimator(fragmentAnimator: FragmentAnimator?) {
        mDelegate.fragmentAnimator = fragmentAnimator
    }

    override fun onLazyInitView(savedInstanceState: Bundle?) =
            mDelegate.onLazyInitView(savedInstanceState)


    override fun getFragmentAnimator(): FragmentAnimator =
            mDelegate.fragmentAnimator


    override fun isSupportVisible(): Boolean =
            mDelegate.isSupportVisible


    override fun onEnterAnimationEnd(savedInstanceState: Bundle?) =
            mDelegate.onEnterAnimationEnd(savedInstanceState)


    override fun onSupportVisible() =
            mDelegate.onSupportVisible()


    override fun onBackPressedSupport(): Boolean =
            mDelegate.onBackPressedSupport()


    override fun putNewBundle(newBundle: Bundle?) =
            mDelegate.putNewBundle(newBundle)


    override fun post(runnable: Runnable?) =
            mDelegate.post(runnable)

    /****************************************以下为可选方法(Optional methods)******************************************************/
    // 自定制Support时，可移除不必要的方法

    /**
     * 隐藏软键盘
     */
    protected fun hideSoftInput() =
            mDelegate.hideSoftInput()


    /**
     * 显示软键盘,调用该方法后,会在onPause时自动隐藏软键盘
     */
    protected fun showSoftInput(view: View) =
            mDelegate.showSoftInput(view)

    /**
     * 加载根Fragment, 即Activity内的第一个Fragment 或 Fragment内的第一个子Fragment
     *
     * @param containerId 容器id
     * @param toFragment  目标Fragment
     */
    fun loadRootFragment(containerId: Int, toFragment: ISupportFragment) =
            mDelegate.loadRootFragment(containerId, toFragment)

    fun loadRootFragment(containerId: Int, toFragment: ISupportFragment,
                         addToBackStack: Boolean, allowAnim: Boolean) =
            mDelegate.loadRootFragment(containerId, toFragment, addToBackStack, allowAnim)

    fun start(toFragment: ISupportFragment) =
            mDelegate.start(toFragment)

    /**
     * @param launchMode Similar to Activity's LaunchMode.
     */
    fun start(toFragment: ISupportFragment, @ISupportFragment.LaunchMode launchMode: Int) =
            mDelegate.start(toFragment, launchMode)

    /**
     * Launch an fragment for which you would like a result when it poped.
     */
    fun startForResult(toFragment: ISupportFragment, requestCode: Int) =
            mDelegate.startForResult(toFragment, requestCode)

    /**
     * Launch a fragment while poping self.
     */
    fun startWithPop(toFragment: ISupportFragment) =
            mDelegate.startWithPop(toFragment)

    fun replaceFragment(toFragment: ISupportFragment, addToBackStack: Boolean) =
            mDelegate.replaceFragment(toFragment, addToBackStack)

    fun pop() = mDelegate.pop()


    /**
     * Pop the last fragment transition from the manager's fragment
     * back stack.
     *
     *
     * 出栈到目标fragment
     *
     * @param targetFragmentClass   目标fragment
     * @param includeTargetFragment 是否包含该fragment
     */
    fun popTo(targetFragmentClass: Class<*>, includeTargetFragment: Boolean) =
            mDelegate.popTo(targetFragmentClass, includeTargetFragment)

    /**
     * 获取栈内的fragment对象
     */
    fun <T : ISupportFragment> findChildFragment(fragmentClass: Class<T>): T =
            SupportHelper.findFragment(childFragmentManager, fragmentClass)


}