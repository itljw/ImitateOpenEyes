package com.itljw.openeyes.base.interf

import android.support.annotation.LayoutRes

/**
 * Created by JW on on 2018/7/12 23:46.
 * Email : 1481013718@qq.com
 * Description :
 */
interface IFragment {

    /**
     * 获取布局Id
     */
    @LayoutRes fun getLayoutId():Int

    /**
     * 处理业务
     */
    fun doBusiness()

}