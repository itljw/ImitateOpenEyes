package com.itljw.openeyes.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.classic.common.MultipleStatusView
import com.itljw.openeyes.base.fragmentation.SwipeBackFragment
import com.itljw.openeyes.base.interf.IFragment

/**
 * Created by JW on on 2018/7/31 23:42.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class BaseSwipeBackFragment : SwipeBackFragment(), IFragment {
    protected var mMultipleStatusLayout: MultipleStatusView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleArgument(arguments)
        handleObserver()
        doBusiness()

        mMultipleStatusLayout?.setOnRetryClickListener { handleRetryEvent() }
    }

    /**
     * 处理参数传递
     */
    open fun handleArgument(arguments: Bundle?) {}

    /**
     * 处理观察者监听
     */
    open fun handleObserver() {}

    /**
     * 重新加载
     */
    open fun handleRetryEvent() {}
}