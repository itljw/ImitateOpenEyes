package com.itljw.openeyes.base

import android.content.Intent
import android.os.Bundle
import com.itljw.openeyes.R
import com.itljw.openeyes.base.interf.IActivity
import com.jaeger.library.StatusBarUtil
import me.yokeyword.fragmentation_swipeback.SwipeBackActivity

/**
 * Created by JW on on 2018/7/29 23:00.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class BaseSwipeBackActivity:SwipeBackActivity(),IActivity {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())

        StatusBarUtil.setColor(this, resources.getColor(R.color.color_black))

        handleIntent(intent)
        doBusiness()
    }

    /**
     * 处理意图Intent
     */
    open fun handleIntent(intent: Intent) {}

}