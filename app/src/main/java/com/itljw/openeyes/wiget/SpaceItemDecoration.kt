package com.itljw.openeyes.wiget

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by JW on on 2018/7/24 20:37.
 * Email : 1481013718@qq.com
 * Description : 设置Item间距
 */
class SpaceItemDecoration(private val mSpace:Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.getItemOffsets(outRect, view, parent, state)

        outRect?.left = mSpace
        outRect?.right = mSpace
    }


}