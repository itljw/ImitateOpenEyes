package com.itljw.openeyes.wiget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.itljw.openeyes.R
import kotlinx.android.synthetic.main.layout_top_bottom_text.view.*

/**
 * Created by JW on on 2019/5/15 21:20.
 * Email : 1481013718@qq.com
 * Description :
 */
class TopBottomTextView : ConstraintLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var mRootView: View = LayoutInflater.from(context).inflate(R.layout.layout_top_bottom_text, this)

    fun setTopText(topText: String): TopBottomTextView {
        mRootView.tvTopText.text = topText
        return this
    }

    fun setBottomText(bottomText: String): TopBottomTextView {
        mRootView.tvBottomText.text = bottomText
        return this
    }

}