package com.itljw.openeyes.wiget.webview

import android.annotation.SuppressLint
import android.os.Build
import android.webkit.WebViewClient
import bundleOf
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_web.*
import kotlinx.android.synthetic.main.layout_title_bar.*

/**
 * Created by JW on on 2019/4/14 20:37.
 * Email : 1481013718@qq.com
 * Description :
 */
class WebFragment : BaseFragment() {

    private var title = ""
    private var url = ""

    override fun getLayoutId(): Int =
            R.layout.fragment_web

    companion object {

        private const val ARG_TITLE = "arg_title"
        private const val ARG_URL = "arg_url"

        fun newInstance(title: String, url: String): WebFragment {
            return WebFragment().apply {
                arguments = bundleOf(
                        ARG_TITLE to title,
                        ARG_URL to url
                )
            }
        }
    }

    override fun doBusiness() {

        arguments?.apply {
            url = getString(ARG_URL)
            title = getString(ARG_TITLE)
        }

        // 设置标题
        tvTitle.text = title

        initWebView()

        mWebView.loadUrl(url)

        ivBack.setOnClickListener {
            activity?.finish()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val settings = mWebView.settings

        settings.javaScriptEnabled = true

        //将图片调整到适合WebView的大小
        settings.useWideViewPort = true
        // 缩放至屏幕的大小
        settings.loadWithOverviewMode = true

        mWebView.webViewClient = object : WebViewClient() {}

        // API大于19时，如果多张图片url相同时，只会加载一个IMAGE所以直接加载
        settings.loadsImagesAutomatically = Build.VERSION.SDK_INT > 19

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            settings.mixedContentMode = android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
    }
}