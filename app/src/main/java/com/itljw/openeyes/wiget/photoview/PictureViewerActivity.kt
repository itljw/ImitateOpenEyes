package com.itljw.openeyes.wiget.photoview

import android.content.Intent
import android.os.Build
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.FragmentActivity
import android.view.View
import com.itljw.openeyes.AppContext.Companion.mContext
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import kotlinx.android.synthetic.main.activity_picture_viewer.*

/**
 * Created by JW on on 2018/7/26 22:58.
 * Email : 1481013718@qq.com
 * Description :
 */
class PictureViewerActivity : BaseActivity() {

    companion object {
        const val EXTRA_IMAGE_URL = "extra_image_url"
        const val EXTRA_MULTI_IMAGE_URL = "extra_multi_image_url"
        const val EXTRA_SELECT_IMAGE_POSITION = "extra_select_image_position"

        fun launch(activity: FragmentActivity, targetView: View, imageUrl: String) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mContext.startActivity(Intent(activity, PictureViewerActivity::class.java).putExtra(
                        PictureViewerActivity.EXTRA_IMAGE_URL, imageUrl))
            } else {
                val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, targetView, activity.getString(R.string.image_transition_name))
                activity.startActivity(Intent(mContext, PictureViewerActivity::class.java).putExtra(
                        PictureViewerActivity.EXTRA_IMAGE_URL,
                        imageUrl),
                        optionsCompat.toBundle())
            }
        }

        fun launchWithMultiImage(activity: FragmentActivity, targetView: View, imageUrls: ArrayList<String>) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                mContext.startActivity(Intent(activity, PictureViewerActivity::class.java).putStringArrayListExtra(
                        PictureViewerActivity.EXTRA_MULTI_IMAGE_URL, imageUrls))
            } else {
                val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, targetView, activity.getString(R.string.image_transition_name))
                activity.startActivity(Intent(mContext, PictureViewerActivity::class.java).putStringArrayListExtra(
                        PictureViewerActivity.EXTRA_MULTI_IMAGE_URL,
                        imageUrls),
                        optionsCompat.toBundle())
            }
        }
    }

    private var imageUrls: List<String>? = null

    override fun getLayoutId(): Int = R.layout.activity_picture_viewer

    private var currentSelectedPosition: Int = 0

    override fun handleIntent(intent: Intent) {
        imageUrls = intent.getStringArrayListExtra(EXTRA_MULTI_IMAGE_URL)
        currentSelectedPosition = intent.getIntExtra(EXTRA_SELECT_IMAGE_POSITION, 0)

        val imageUrl = intent.getStringExtra(EXTRA_IMAGE_URL)
        if (!imageUrl.isNullOrEmpty()) {
            imageUrls = arrayListOf<String>(imageUrl)
        }

        if (imageUrls == null) throw  IllegalStateException("imageUrls cannot be null")

    }

    private val mPictureViewerAdapter by lazy {
        PictureViewerAdapter(this, imageUrls!!)
    }

    override fun doBusiness() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition()  //延迟动画
        }

        mVpPictureViewer.adapter = mPictureViewerAdapter
        mVpPictureViewer.currentItem = currentSelectedPosition // 设置选中Position
        mPictureViewerAdapter.setSelectedPosition(currentSelectedPosition)
    }

}