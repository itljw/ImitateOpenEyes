package com.itljw.openeyes.wiget

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.util.AttributeSet
import com.itljw.openeyes.R
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer
import com.shuyu.gsyvideoplayer.video.base.GSYBaseVideoPlayer
import kotlinx.android.synthetic.main.layout_simple_video_player.view.*

/**
 * Created by JW on on 2018/9/11 21:03.
 * Email : 1481013718@qq.com
 * Description :
 */
class SimpleVideoPlayer : StandardGSYVideoPlayer, LifecycleObserver {

    constructor(context: Context, fullFlag: Boolean?) : super(context, fullFlag!!)

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    private var currentPosition: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.layout_simple_video_player
    }

    override fun init(context: Context?) {
        super.init(context)

        ivVideoMore.setOnClickListener {
            onVideoPlayerClickListener?.onMoreClick()
        }
    }

    override fun onPrepared() {
        super.onPrepared()
        seekTo(currentPosition.toLong())
    }

    /**
     * 设置进度条
     */
    fun setCurrentPosition(position: Int) {
        currentPosition = position
    }

    override fun cloneParams(from: GSYBaseVideoPlayer?, to: GSYBaseVideoPlayer?) {
        super.cloneParams(from, to)
        val st = to as SimpleVideoPlayer

        st.ivVideoMore?.let {
            it.setOnClickListener {
                onVideoPlayerClickListener?.onMoreClick()
            }
        }
    }

    fun hideAllWidgets() {
        hideAllWidget()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPauseEvent() {
        onVideoPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResumeEvent() {
        onVideoResume()
    }

//    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
//    fun onDestroyEvent() {
//        GSYVideoManager.releaseAllVideos()
//    }

    var onVideoPlayerClickListener: OnVideoPlayerClickListener? = null

    interface OnVideoPlayerClickListener {
        fun onMoreClick()
    }

}
