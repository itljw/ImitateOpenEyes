package com.itljw.openeyes.wiget

import android.content.Context
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.*
import android.widget.RelativeLayout
import com.itljw.openeyes.R
import com.itljw.openeyes.ui.video.SimpleVideoCallBack
import dp2px
import inRangeOfView
import kotlinx.android.synthetic.main.layout_float_video.view.*


/**
 * Created by JW on on 2019/5/26 14:06.
 * Email : 1481013718@qq.com
 * Description :
 */
class FloatVideoView : RelativeLayout {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var mPaint = Paint()

    private var touchSlop = 0
    private var lastX: Float = 0f
    private var lastY: Float = 0f

    private var oldOffsetX: Int = 0
    private var oldOffsetY: Int = 0

    private var limitHeight = 0
    private var limitWidth = 0

    private val mParams = WindowManager.LayoutParams()
    private val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    private var mRootView: View = LayoutInflater.from(context).inflate(R.layout.layout_float_video, this)

    init {
        touchSlop = ViewConfiguration.get(context).scaledTouchSlop
        mPaint.isAntiAlias = true
        mPaint.style = Paint.Style.FILL
    }

    /**
     * 开始播放视频
     */
    fun startPlayVideo(playUrl: String, currentPosition: Int = 0) {

        mRootView.mEmptyControlVideo.setUp(playUrl, true, "")
        mRootView.mEmptyControlVideo.setCurrentPosition(currentPosition)
        mRootView.mEmptyControlVideo.startPlayLogic()

        mRootView.mEmptyControlVideo.setVideoAllCallBack(object : SimpleVideoCallBack {
            override fun onAutoComplete(url: String, vararg objects: Any) {
                super.onAutoComplete(url, *objects)
                remove() // 播放完后自动remove
            }
        })
    }

    /**
     * 初始化参数和视频数据
     */
    fun initWindowParams(
            visibleHeight: Int,
            visibleWidth: Int,
            videoWidth: Int,
            videoHeight: Int,
            offsetBottom: Int = context.dp2px(100).toInt()
    ) {
        //显示比例调整
        val screenWidth = resources.displayMetrics.widthPixels / 2
        val screenHeight = resources.displayMetrics.heightPixels / 3

        val ratio = videoWidth.toDouble() / videoHeight.toDouble()
        if (videoWidth >= videoHeight) { // 横屏
            mParams.width = screenWidth
            mParams.height = (screenWidth / ratio).toInt()
        } else {
            mParams.height = screenHeight
            mParams.width = (screenHeight / ratio).toInt()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //Android O调整
            mParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            @Suppress("DEPRECATION")
            mParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
        }
        mParams.gravity = Gravity.BOTTOM or Gravity.END
        mParams.x = 0
        mParams.y = offsetBottom // 底部偏移量
        mParams.flags = (WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)

        limitHeight = visibleHeight - mParams.height
        limitWidth = visibleWidth - mParams.width

        mRootView.ivCancel.setOnClickListener { remove() }
    }

    override fun performClick(): Boolean {
        return super.performClick()
    }

    // 拦截事件
    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return true
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            val currentX = event.x
            val currentY = event.y

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    lastX = currentX
                    lastY = currentY

                    oldOffsetX = mParams.x // 偏移量
                    oldOffsetY = mParams.y // 偏移量
                }
                MotionEvent.ACTION_MOVE -> {

                    if (lastX == 0f) lastX = currentX
                    if (lastY == 0f) lastY = currentY

                    val dx = ((lastX - currentX) / 3).toInt() // 抖动处理
                    val dy = ((lastY - currentY) / 3).toInt()

                    updateLocation(Math.min(Math.max(0, mParams.x + dx), limitWidth),
                            Math.min(Math.max(0, mParams.y + dy), limitHeight))
                }

                MotionEvent.ACTION_UP -> {

                    // 拖动距离小，则认为是点击
                    if (isDragSmallRange(oldOffsetX - mParams.x)
                            && isDragSmallRange(oldOffsetY - mParams.y)) {

                        // 取消按钮的点击范围
                        if (mRootView.ivCancel.inRangeOfView(event)) { // cancel
                            remove()
                        } else {
                            performClick()
                        }
                    }

                }
            }
        }

        return true
    }

    /**
     * 拖动距离小
     */
    private fun isDragSmallRange(offset: Int) =
            Math.abs(offset) <= touchSlop

    /**
     * 更新显示位置
     */
    private fun updateLocation(x: Int, y: Int) {
        mParams.x = x
        mParams.y = y
        windowManager.updateViewLayout(this, mParams)
    }

    private var isShow = false

    /**
     * 显示悬浮控件
     */
    fun show() {
        if (!isShow) {
            windowManager.addView(this, mParams)
            isShow = true
        }
    }

    /**
     * 移除控件
     */
    fun remove() {
        if (isShow) {
            windowManager.removeViewImmediate(this)
            isShow = false
            mRootView.mEmptyControlVideo.release()
        }
    }

    /**
     * 当前播放进度
     */
    fun getCurrentPosition(): Int {
        return mRootView.mEmptyControlVideo.currentPositionWhenPlaying

    }

    /*private val mRectF = RectF()

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        mRectF.left = paddingLeft.toFloat()
        mRectF.top = paddingTop.toFloat()
        mRectF.right = (paddingLeft + width).toFloat()
        mRectF.bottom = (paddingTop + height).toFloat()

        canvas?.drawRoundRect(mRectF, context.dp2px(30), context.dp2px(30), mPaint)
    }*/


}