package com.itljw.openeyes.wiget

import android.animation.ValueAnimator
import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import getPredictLineCount

/**
 * Created by JW on on 2019/5/23 21:04.
 * Email : 1481013718@qq.com
 * Description :
 */
class WordForWordTextView : AppCompatTextView {

    private var textCount = 1
    private val duration = 500L
    private var currentIndex = -1
    private var resultText: String = ""

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    /**
     * 设置文本内容并开启动画
     */
    fun setShowText(showText: String?) {

        showText?.apply {
            textCount = length
            resultText = this
            text = this

            minLines = getPredictLineCount() // 设置最小行数
            startWordAnimation() // 开启动画
        }

    }

    /**
     * 开启动画
     */
    private fun startWordAnimation() {
        visibility = View.VISIBLE
        val valueAnimator = ValueAnimator.ofInt(0, textCount)
//        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.duration = duration
        valueAnimator.addUpdateListener {
            val index = it.animatedValue as Int
            if (index != currentIndex) {

                currentIndex = index
                text = resultText.subSequence(0, currentIndex)
            }
        }

        valueAnimator.start()
    }

}