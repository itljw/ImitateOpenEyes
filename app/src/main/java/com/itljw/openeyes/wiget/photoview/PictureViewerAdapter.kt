package com.itljw.openeyes.wiget.photoview

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v4.app.ActivityCompat.startPostponedEnterTransition
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.github.chrisbanes.photoview.PhotoView
import com.itljw.openeyes.R
import com.itljw.openeyes.util.GlideUtil
import toast

/**
 * Created by JW on on 2018/7/26 22:59.
 * Email : 1481013718@qq.com
 * Description :
 */
class PictureViewerAdapter(private val activity: PictureViewerActivity, private val imageUrls: List<String>) : PagerAdapter() {

    private var currentSelectedPosition: Int = 0

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = imageUrls.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val photoView = PhotoView(container.context)

        photoView.scaleType = ImageView.ScaleType.FIT_CENTER
        GlideUtil.displayImageWithListener(photoView.context, imageUrls[position], object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                photoView.setImageBitmap(resource)

                if (currentSelectedPosition == position) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        photoView.transitionName = activity.getString(R.string.image_transition_name)
                        startPostponedEnterTransition(activity)
                    }
                }
            }

            override fun onLoadFailed(errorDrawable: Drawable?) {
                super.onLoadFailed(errorDrawable)
                if (currentSelectedPosition == position) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        photoView.transitionName = activity.getString(R.string.image_transition_name)
                        startPostponedEnterTransition(activity)
                    }
                }
                activity.toast("图片加载失败，请检查网络后重试！")

            }

        })

        photoView.setOnClickListener {
            activity.onBackPressed()
        }

        container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        return photoView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    /**
     * 设置选中位置
     */
    fun setSelectedPosition(currentSelectedPosition: Int) {
        this.currentSelectedPosition = currentSelectedPosition
    }
}