package com.itljw.openeyes.wiget

import android.graphics.Rect
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.View


/**
 * Created by JW on on 2019/4/20 22:49.
 * Email : 1481013718@qq.com
 * Description :
 */
class GridSpacingItemDecoration(private val mSpace: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        super.getItemOffsets(outRect, view, parent, state)

        val itemPosition = (view?.layoutParams as RecyclerView.LayoutParams).viewLayoutPosition

        val spanCount = getSpanCount(parent)
        val childCount = parent?.adapter?.itemCount ?: return

        when {
            isLastRaw(parent, itemPosition, spanCount, childCount) -> // 如果是最后一行，则不需要绘制底部
                outRect?.set(0, 0, mSpace, 0)
            isLastColumn(parent, itemPosition, spanCount, childCount) -> // 如果是最后一列，则不需要绘制右边
                outRect?.set(mSpace, 0, 0, mSpace)
            else -> outRect?.set(0, 0, mSpace, mSpace)
        }

    }

    private fun getSpanCount(parent: RecyclerView?): Int {
        // 列数
        var spanCount = -1
        val layoutManager = parent?.layoutManager
        if (layoutManager is GridLayoutManager) {
            spanCount = layoutManager.spanCount
        } else if (layoutManager is StaggeredGridLayoutManager) {
            spanCount = layoutManager
                    .spanCount
        }
        return spanCount
    }

    private fun isLastColumn(parent: RecyclerView?, pos: Int, spanCount: Int, childCount: Int): Boolean {
        val layoutManager = parent?.layoutManager
        if (layoutManager is GridLayoutManager) {
            if ((pos + 1) % spanCount == 0) { // 如果是最后一列，则不需要绘制右边
                return true
            }
        } else if (layoutManager is StaggeredGridLayoutManager) {
            val orientation = layoutManager.orientation
            if (orientation == StaggeredGridLayoutManager.VERTICAL) {
                if ((pos + 1) % spanCount == 0) { // 如果是最后一列，则不需要绘制右边
                    return true
                }
            } else {
                val result = childCount - childCount % spanCount
                if (pos >= result) // 如果是最后一列，则不需要绘制右边
                    return true
            }
        }
        return false
    }

    private fun isLastRaw(parent: RecyclerView?, pos: Int, spanCount: Int,
                          childCount: Int): Boolean {
        var resultChildCount = childCount
        val layoutManager = parent?.layoutManager
        if (layoutManager is GridLayoutManager) {
            resultChildCount -= resultChildCount % spanCount
            if (pos >= resultChildCount) // 如果是最后一行，则不需要绘制底部
                return true
        } else if (layoutManager is StaggeredGridLayoutManager) {
            val orientation = layoutManager
                    .orientation
            // StaggeredGridLayoutManager 且纵向滚动
            if (orientation == StaggeredGridLayoutManager.VERTICAL) {
                resultChildCount -= resultChildCount % spanCount
                // 如果是最后一行，则不需要绘制底部
                if (pos >= resultChildCount)
                    return true
            } else
            // StaggeredGridLayoutManager 且横向滚动
            {
                // 如果是最后一行，则不需要绘制底部
                if ((pos + 1) % spanCount == 0) {
                    return true
                }
            }
        }
        return false
    }


}