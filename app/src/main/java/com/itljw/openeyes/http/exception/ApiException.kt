package com.itljw.openeyes.http.exception

/**
 * Created by JW on 2017/12/29 15:19
 * Email : 1481013718@qq.com
 * Description :
 */
class ApiException(var ret: Int, var msg: String?) : Exception()
