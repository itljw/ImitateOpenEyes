package com.itljw.openeyes.http

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer

/**
 * Created by JW on on 2019/5/24 22:29.
 * Email : 1481013718@qq.com
 * Description :
 */
class ResponseSource<T>(
        private val liveData: LiveData<HttpResult<T>>?, val observer: IObserver<T>?
) : Observer<HttpResult<T>> {

    override fun onChanged(t: HttpResult<T>?) {
        if (true == liveData?.value?.isSuccessful) {
            observer?.onSuccess(liveData.value?.data)
        } else {
            observer?.onError(liveData?.value?.code, liveData?.value?.msg)
        }
    }

    /**
     * 添加观察者
     */
    fun addObserve() {
        liveData?.observeForever(this)
    }

    /**
     * 移除观察者
     */
    fun removeObserve() {
        liveData?.removeObserver(this)
    }

}