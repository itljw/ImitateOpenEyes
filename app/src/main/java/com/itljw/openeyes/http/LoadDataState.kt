package com.itljw.openeyes.http

/**
 * author : created by JW on 2019/5/24 18:27
 * package : com.itljw.openeyes.http
 * email : 1481013718@qq.com
 * description :
 */
enum class LoadDataState {
    /**
     * 加载中
     */
    LOADING,
    /**
     * 数据为空
     */
    EMPTY,
    /**
     * 错误
     */
    ERROR,
    /**
     * 加载更多
     */
    LOAD_MORE,
    /**
     * 没有更多数据
     */
    NO_MORE_DATA
}