package com.itljw.openeyes.http

import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.MediatorLiveData
import appToast
import com.itljw.openeyes.AppContext
import com.itljw.openeyes.R
import com.itljw.openeyes.http.exception.ApiException
import com.itljw.openeyes.util.RxSchedulers
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

/**
 * Created by JW on on 2018/9/1 23:38.
 * Email : 1481013718@qq.com
 * Description :
 */
abstract class NetworkHandler<T> {

    constructor(isAutoShowToast: Boolean = true) {
        run(isAutoShowToast)
    }

    constructor(dialog: Dialog, isAutoShowToast: Boolean = true) {
        run(dialog, isAutoShowToast)
    }

    /**
     * 开始请求
     */
    private fun run(isAutoShowToast: Boolean) {
        getCompositeDisposable()?.add(
                fetchNetwork().compose(RxSchedulers.ioMain())
                        .subscribe(
                                { data ->
                                    // 构造HttpResult
                                    getLiveData().value = HttpResult.success(HttpResult.CODE_SUCCESS, data)
                                },
                                { error ->
                                    // 错误处理
                                    handleException(error, isAutoShowToast)
                                }
                        )
        )
    }

    private fun run(dialog: Dialog, isAutoShowToast: Boolean) {

        getCompositeDisposable()?.add(
                fetchNetwork().compose(RxSchedulers.ioMain())
                        .subscribe(
                                { data ->
                                    // 构造HttpResult
                                    getLiveData().value = HttpResult.success(HttpResult.CODE_SUCCESS, data)
                                },
                                { error ->
                                    // 错误处理
                                    handleException(error, isAutoShowToast)
                                },
                                {
                                    // completed
                                    dialog.dismiss()
                                },
                                {
                                    // onSubscribe
                                    dialog.show()
                                }
                        )
        )
    }

    /**
     * 不使用ViewModel
     */
    @SuppressLint("CheckResult")
    fun runWithNoViewModel() {
        fetchNetwork().compose(RxSchedulers.ioMain())
                .subscribe(
                        { data ->
                            // 构造HttpResult
                            getLiveData().value = HttpResult.success(HttpResult.CODE_SUCCESS, data)
                        },
                        { error ->
                            // 错误处理
                            handleException(error, true)
                        }
                )
    }

    /**
     * 异常处理
     */
    private fun handleException(e: Throwable?, isAutoShowToast: Boolean) {
        when (e) {
            is HttpException -> { // 数据加载失败,请检查网络
                getLiveData().value = HttpResult.error(HttpResult.NETWORK_ERROR, AppContext.mContext.getString(R.string.error_network))
                if (isAutoShowToast) appToast(AppContext.mContext.getString(R.string.error_network))
            }
            is ApiException -> { // 接收返回错误
                getLiveData().value = HttpResult.error(e.ret, e.msg ?: "")
                if (isAutoShowToast) appToast(e.msg ?: "")
            }
            else -> {
                getLiveData().value = HttpResult.error(HttpResult.UNKNOW_ERROR, AppContext.mContext.getString(R.string.error_unknown))
                if (isAutoShowToast) appToast(AppContext.mContext.getString(R.string.error_unknown))
                e?.printStackTrace()
            }
        }
    }

    abstract fun fetchNetwork(): Flowable<T>

    abstract fun getCompositeDisposable(): CompositeDisposable?

    abstract fun getLiveData(): MediatorLiveData<HttpResult<T>>

}