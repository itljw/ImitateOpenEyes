package com.itljw.openeyes.http

/**
 * Created by JW on 2017/12/29 14:44
 * Email : 1481013718@qq.com
 * Description : 对返回数据解析
 */
class HttpResult<T> {

    var code: Int = 0
    var data: T? = null
    var msg: String? = null

    val isSuccessful: Boolean
        get() = CODE_SUCCESS == code

    constructor(code: Int, msg: String) {
        this.code = code
        this.msg = msg
    }

    constructor(code: Int, data: T) {
        this.code = code
        this.data = data
    }

    companion object {

        const val NETWORK_ERROR = 4444
        const val UNKNOW_ERROR = 7777
        const val CODE_SUCCESS = 200

        fun <T> error(code: Int, msg: String): HttpResult<T> {
            return HttpResult(code, msg)
        }

        fun <T> success(code: Int, data: T): HttpResult<T> {
            return HttpResult(code, data)
        }
    }

}
