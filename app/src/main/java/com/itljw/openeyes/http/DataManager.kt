package com.itljw.openeyes.http

import android.arch.lifecycle.LiveData
import com.google.common.base.Joiner
import com.google.gson.JsonElement
import com.itljw.openeyes.ui.video.entity.VideoTagsTabEntity
import com.itljw.openeyes.ui.common.entity.TabPagerEntity
import com.itljw.openeyes.ui.home.entity.AuthorDetailEntity
import com.itljw.openeyes.ui.home.entity.CategoryDetailEntity
import com.itljw.openeyes.ui.home.entity.Data
import com.itljw.openeyes.ui.home.entity.RecommendEntity
import com.itljw.openeyes.ui.notification.entity.OfficialNotificationEntity
import com.itljw.openeyes.util.PreferencesManager
import com.itljw.openeyes.util.SharedPreferencesHelper
import kotlinx.coroutines.*

/**
 * Created by JW on on 2018/7/14 15:10.
 * Email : 1481013718@qq.com
 * Description :
 */
object DataManager : ApiService, PreferencesManager {

    /**
     * 获取回复评论数据
     */
    override fun getRepliesData(videoId: Int, type: String, top: String): LiveData<HttpResult<RecommendEntity>> =
            mApiService.getRepliesData(videoId, type, top)

    /**
     * 获取tab数据
     */
    override fun getTabPagerData(url: String): LiveData<HttpResult<TabPagerEntity>> =
            mApiService.getTabPagerData(url)

    /**
     * 获取作者详情数据
     */
    override fun getAuthorDetailData(id: Int, userType: String): LiveData<HttpResult<AuthorDetailEntity>> =
            mApiService.getAuthorDetailData(id, userType)

    /**
     * 获取排行榜Tab数据
     */
    override fun getRankListTabData(): LiveData<HttpResult<TabPagerEntity>> =
            mApiService.getRankListTabData()

    /**
     * 获取全部分类数据
     */
    override fun getAllCategories(): LiveData<HttpResult<RecommendEntity>> =
            mApiService.getAllCategories()

    /**
     * 获取视频评论数据
     */
    override fun getVideoCommentData(videoId: Int): LiveData<HttpResult<RecommendEntity>> =
            mApiService.getVideoCommentData(videoId)

    /**
     * 获取视频详情tag中的tab数据
     */
    override fun getVideoTagsTabData(id: Int): LiveData<HttpResult<VideoTagsTabEntity>> =
            mApiService.getVideoTagsTabData(id)

    /**
     * 获取视频数据
     */
    override fun getVideoData(id: Int): LiveData<HttpResult<Data>> =
            mApiService.getVideoData(id)

    override fun getVideoRelatedData(id: Int): LiveData<HttpResult<RecommendEntity>> =
            mApiService.getVideoRelatedData(id)

    override fun getCategoryDetailById(id: Int): LiveData<HttpResult<CategoryDetailEntity>> =
            mApiService.getCategoryDetailById(id)

    override fun getCategoryListData(): LiveData<HttpResult<RecommendEntity>> =
            mApiService.getCategoryListData()

    override fun getSearchByKeyword(keyword: String): LiveData<HttpResult<RecommendEntity>> {
        return mApiService.getSearchByKeyword(keyword)
    }

    override fun getSearchHotKeyData(): LiveData<HttpResult<JsonElement>> {
        return mApiService.getSearchHotKeyData()
    }

    override fun getHomeCategoryData(url: String): LiveData<HttpResult<RecommendEntity>> {
        return mApiService.getHomeCategoryData(url)
    }

    /**
     * 获取官方通知数据
     */
    override fun getOfficialNotificationData(): LiveData<HttpResult<OfficialNotificationEntity>> {
        return mApiService.getOfficialNotificationData()
    }

    /**
     * 获取关注Tab数据
     */
    override fun getFollowListData(): LiveData<HttpResult<TabPagerEntity>> {
        return mApiService.getFollowListData()
    }

    /*****************************  PreferencesManager  ***************************/

    override fun getOverlayWindowStatus(): Boolean =
            SharedPreferencesHelper.getBoolean("overlayWindowStatus", true)

    override fun saveOverlayWindowStatus(isOpen: Boolean) {
        SharedPreferencesHelper.putBoolean("overlayWindowStatus", isOpen)
    }

    /**
     * 清空搜索历史
     */
    override fun clearSearchHistoryKeyword() {
        SharedPreferencesHelper.putString("searchHistoryKeyword", "")
    }

    /**
     * 保存搜索关键字
     */
    override fun putSearchHistoryKeyword(keyword: String) = runBlocking<Unit> {
        GlobalScope.launch {
            if (keyword.isNotEmpty()) {
                // 判断是否已存在
                val searchHistoryKeywordList = getSearchHistoryKeyword()
                if (!searchHistoryKeywordList.contains(keyword)) {
                    searchHistoryKeywordList.add(0, keyword)
                    SharedPreferencesHelper.putString("searchHistoryKeyword",
                            Joiner.on(",").join(searchHistoryKeywordList))
                }
            }
        }
    }


    /**
     * 获取搜索历史
     */
    override fun getSearchHistoryKeyword(): MutableList<String> = runBlocking {
        val result = async {
            val searchHistoryKeyword = SharedPreferencesHelper.getString("searchHistoryKeyword", "")
            if (searchHistoryKeyword.isNotBlank()) {
                searchHistoryKeyword.split(",").toMutableList()
            } else {
                mutableListOf()
            }
        }
        result.await()
    }


    /**
     * 保存错误日志
     */
    override fun saveErrorInfo(errorInfo: String) {
        SharedPreferencesHelper.putString("errorInfo", errorInfo)
    }

    /**
     * 获取错误日志
     */
    override fun getErrorInfo(): String {
        return SharedPreferencesHelper.getString("errorInfo", "")
    }


    private val mApiService: ApiService by lazy {
        RetrofitManager.mApiService
    }

}