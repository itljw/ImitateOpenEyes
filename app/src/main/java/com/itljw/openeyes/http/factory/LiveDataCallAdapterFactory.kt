package com.itljw.openeyes.http.factory

import android.arch.lifecycle.LiveData
import com.itljw.openeyes.http.HttpResult
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * author : created by JW on 2019/5/24 14:16
 * package : com.itljw.openeyes.http.factory
 * email : 1481013718@qq.com
 * description :
 */
class LiveDataCallAdapterFactory :CallAdapter.Factory(){

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        val clazz = getRawType(returnType)
        if(clazz != LiveData::class.java){
            return null
        }

        // 获取泛型实际类型的第一个类型
        val observableType = getParameterUpperBound(0, returnType as ParameterizedType)

        if(getRawType(observableType) != HttpResult::class.java){
            return null
        }

        if (observableType !is ParameterizedType) {
            throw IllegalArgumentException("resource must be parameterized")
        }

        val bodyType = getParameterUpperBound(0, observableType)

        return LiveDataCallAdapter<Any>(bodyType)
    }

}