package com.itljw.openeyes.http.factory

import android.arch.lifecycle.LiveData
import com.itljw.openeyes.AppContext
import com.itljw.openeyes.http.HttpResult
import retrofit2.*
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean

/**
 * author : created by JW on 2019/5/24 14:19
 * package : com.itljw.openeyes.http.factory
 * email : 1481013718@qq.com
 * description :
 */
class LiveDataCallAdapter<R>(
        private val returnType: Type
) : CallAdapter<R, LiveData<HttpResult<R>>> {

    override fun adapt(call: Call<R>): LiveData<HttpResult<R>> {
        return object : LiveData<HttpResult<R>>() {
            private var started = AtomicBoolean(false) // 设置初始值，用于避免高并发访问问题
            override fun onActive() {
                super.onActive()

                // 如果初始值是为false，则修改false为true，并返回true
                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : Callback<R> {
                        override fun onFailure(call: Call<R>, t: Throwable) {

                            when (t) {
                                is HttpException -> { // 数据加载失败,请检查网络
                                    postValue(
                                            HttpResult.error(
                                                    HttpResult.NETWORK_ERROR,
                                                    AppContext.mContext.getString(com.itljw.openeyes.R.string.error_network)
                                            )
                                    )
                                }
                                else -> {
                                    postValue(
                                            HttpResult.error(
                                                    HttpResult.UNKNOW_ERROR,
                                                    AppContext.mContext.getString(com.itljw.openeyes.R.string.error_unknown)
                                            )
                                    )
                                    t.printStackTrace()
                                }
                            }
                        }

                        override fun onResponse(call: Call<R>, response: Response<R>) {

                            val body = response.body()

                            if (body == null || response.code() == 204) {
                                postValue(
                                        HttpResult.error(
                                                HttpResult.UNKNOW_ERROR,
                                                AppContext.mContext.getString(com.itljw.openeyes.R.string.error_empty)
                                        )
                                )
                            } else {
                                postValue(
                                        HttpResult.success(HttpResult.CODE_SUCCESS, body)
                                )
                            }
                        }
                    })
                }
            }
        }
    }

    override fun responseType(): Type = returnType
}