package com.itljw.openeyes.http

import android.arch.lifecycle.LiveData
import com.google.gson.JsonElement
import com.itljw.openeyes.ui.video.entity.VideoTagsTabEntity
import com.itljw.openeyes.ui.common.entity.TabPagerEntity
import com.itljw.openeyes.ui.home.entity.AuthorDetailEntity
import com.itljw.openeyes.ui.home.entity.CategoryDetailEntity
import com.itljw.openeyes.ui.home.entity.Data
import com.itljw.openeyes.ui.home.entity.RecommendEntity
import com.itljw.openeyes.ui.notification.entity.OfficialNotificationEntity
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * Created by JW on on 2018/7/14 10:49.
 * Email : 1481013718@qq.com
 * Description :
 */
interface ApiService {

    /**
     * 获取回复评论数据
     */
    @GET("v2/replies/video")
    fun getRepliesData(
            @Query("videoId") videoId: Int,
            @Query("type") type: String,
            @Query("top") top: String
    ): LiveData<HttpResult<RecommendEntity>>

    /**
     * 获取tab数据
     */
    @GET
    fun getTabPagerData(@Url url: String): LiveData<HttpResult<TabPagerEntity>>

    /**
     * 获取作者详情数据
     */
    @GET("v5/userInfo/tab")
    fun getAuthorDetailData(
            @Query("id") id: Int, @Query("userType") userType: String
    ): LiveData<HttpResult<AuthorDetailEntity>>

    /**
     * 获取排行榜Tab数据
     */
    @GET("v4/rankList")
    fun getRankListTabData(): LiveData<HttpResult<TabPagerEntity>>

    /**
     * 获取全部分类数据
     */
    @GET("v4/categories/all")
    fun getAllCategories(): LiveData<HttpResult<RecommendEntity>>

    /**
     * 获取视频评论数据
     */
    @GET("v2/replies/video")
    fun getVideoCommentData(
            @Query("videoId") videoId: Int
    ): LiveData<HttpResult<RecommendEntity>>

    /**
     * 获取视频详情tag中的tab数据
     */
    @GET("v1/tag/index")
    fun getVideoTagsTabData(@Query("id") id: Int): LiveData<HttpResult<VideoTagsTabEntity>>

    /**
     * 获取视频数据
     */
    @GET("v2/video/{id}")
    fun getVideoData(
            @Path("id") id: Int
    ): LiveData<HttpResult<Data>>

    /**
     * 获取视频相关推荐数据
     */
    @GET("v4/video/related")
    fun getVideoRelatedData(
            @Query("id") id: Int
    ): LiveData<HttpResult<RecommendEntity>>

    /**
     * 根据id获取分类详情数据
     */
    @GET("v4/categories/detail/tab")
    fun getCategoryDetailById(
            @Query("id") id: Int
    ): LiveData<HttpResult<CategoryDetailEntity>>

    /**
     * 获取分类列表数据
     */
    @GET("v5/category/list")
    fun getCategoryListData(): LiveData<HttpResult<RecommendEntity>>

    /**
     * 根据关键字搜索
     */
    @GET("v3/search")
    fun getSearchByKeyword(
            @Query("query") keyword: String
    ): LiveData<HttpResult<RecommendEntity>>

    /**
     * 获取热门关键字
     */
    @GET("v3/queries/hot")
    fun getSearchHotKeyData(): LiveData<HttpResult<JsonElement>>

    /**
     * 获取关注Tab数据
     */
    @GET("v5/community/tab/list")
    fun getFollowListData(): LiveData<HttpResult<TabPagerEntity>>

    /**
     * 获取官方通知数据
     */
    @GET("v3/messages")
    fun getOfficialNotificationData(): LiveData<HttpResult<OfficialNotificationEntity>>

    @GET
    fun getHomeCategoryData(@Url url: String): LiveData<HttpResult<RecommendEntity>>

}