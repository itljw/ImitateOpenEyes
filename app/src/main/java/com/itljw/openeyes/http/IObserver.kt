package com.itljw.openeyes.http

/**
 * author : created by JW on 2019/5/24 10:45
 * package : com.itljw.openeyes.http
 * email : 1481013718@qq.com
 * description :
 */
interface IObserver<T> {

    /**
     * 成功回调
     */
    fun onSuccess(data: T?)

    /**
     * 错误回调
     */
    fun onError(code: Int?, msg: String?)

}