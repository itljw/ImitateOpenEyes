package com.itljw.openeyes.http

import appToast
import com.itljw.openeyes.AppContext

/**
 * author : created by JW on 2019/5/24 10:46
 * package : com.itljw.openeyes.http
 * email : 1481013718@qq.com
 * description :
 */
abstract class BaseObserver<T> : IObserver<T> {
    override fun onSuccess(data: T?) {
    }

    override fun onError(code: Int?, msg: String?) {
        AppContext.appToast(msg ?: "")
    }
}