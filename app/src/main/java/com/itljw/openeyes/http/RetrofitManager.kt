package com.itljw.openeyes.http

import com.itljw.openeyes.AppContext
import com.itljw.openeyes.constants.Const
import com.itljw.openeyes.http.factory.LiveDataCallAdapterFactory
import com.itljw.openeyes.util.AppUtils
import com.itljw.openeyes.util.NetworkUtil
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Created by JW on on 2018/7/14 10:52.
 * Email : 1481013718@qq.com
 * Description :
 */
object RetrofitManager {

    private var mRetrofit: Retrofit? = null

    val mApiService: ApiService by lazy {
        getRetrofit()!!.create(ApiService::class.java)
    }

    private fun getRetrofit(): Retrofit? {

        if (mRetrofit == null) {
            mRetrofit = Retrofit.Builder()
                    .baseUrl(Const.BASE_URL)
                    .client(getOkHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addCallAdapterFactory(LiveDataCallAdapterFactory())
//                    .addConverterFactory(FastJsonConverterFactory.create()) // fastJson和kotlin有冲突，data class不能正常转换，缺默认构造参数
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
        return mRetrofit
    }

    /**
     * 获取OkHttpClient
     */
    private fun getOkHttpClient(): OkHttpClient {

        // 添加log拦截器
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC // 设置日志级别为basic

        // 缓存路径
        val cacheFile = File(AppContext.mContext.cacheDir, "cache")
        val cache = Cache(cacheFile, 1024 * 1024 * 50) // 50m的缓存

        return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(addCacheInterceptor())
                .addInterceptor(addQueryParamInterceptor())
                .cache(cache)
                .connectTimeout(30L, TimeUnit.SECONDS)
                .readTimeout(30L, TimeUnit.SECONDS)
                .writeTimeout(30L, TimeUnit.SECONDS)
                .build()
    }

    /**
     * 添加缓存拦截器
     */
    private fun addCacheInterceptor(): Interceptor {
        return Interceptor {
            var request = it.request()

            // 如果没有网络，则使用缓存
            if (!NetworkUtil.isNetworkAvailable(AppContext.mContext)) {
                request = request.newBuilder()
                        .cacheControl(CacheControl.FORCE_CACHE)
                        .build()
            }

            var response = it.proceed(request)
            if (NetworkUtil.isNetworkAvailable(AppContext.mContext)) {
                // 有网络的情况下，不使用缓存
                val maxAge = 0
                // 有网络时, 不缓存, 最大保存时长为0
                response = response.newBuilder()
                        .header("Cache-Control", "public, max-age=$maxAge")
                        .removeHeader("Pragma")
                        .build()
            } else {
                // 无网络时，设置超时为4周
                val maxStale = 60 * 60 * 24 * 28
                response = response.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-age=$maxStale")
                        .removeHeader("Pragma")
                        .build()
            }
            response
        }
    }

    /**
     * 设置公共参数
     */
    private fun addQueryParamInterceptor(): Interceptor {
        return Interceptor {
            var request = it.request()
            // 设置公共参数
            val httpUrl = request.url().newBuilder()
                    .addQueryParameter("udid", "e46bdbec8ac1496ab536b9196532686e899646dd") // 用户id
                    .addQueryParameter("vc", "352") // 固定值
                    .addQueryParameter("deviceModel", AppUtils.getPhoneModel())
                    .addQueryParameter("first_channel", "eyepetizer_zhihuiyun_market")
                    .addQueryParameter("last_channel", "eyepetizer_zhihuiyun_market")
                    .build()

            println("httpUrl: ${httpUrl.url()}")

            request = request.newBuilder().url(httpUrl).build()
            it.proceed(request)
        }
    }

}