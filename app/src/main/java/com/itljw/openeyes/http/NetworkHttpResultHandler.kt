package com.itljw.openeyes.http

import android.app.Dialog
import android.arch.lifecycle.MediatorLiveData
import appToast
import com.itljw.openeyes.http.exception.ApiException
import com.itljw.openeyes.util.RxSchedulers
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException

/**
 * author : created by JW on 2018/9/6 18:50
 * package : com.itljw.openeyes.http
 * email : 1481013718@qq.com
 * description :
 */
abstract class NetworkHttpResultHandler<T> {

    /**
     * 开始请求
     */
    fun run() {
        getCompositeDisposable().add(
                fetchNetwork().compose(RxSchedulers.ioMain())
                        .subscribe(
                                { data ->
                                    getLiveData().value = data
                                },
                                { error ->
                                    // 错误处理
                                    handleException(error)
                                }
                        )
        )
    }

    fun run(dialog: Dialog) {

        getCompositeDisposable().add(
                fetchNetwork().compose(RxSchedulers.ioMain())
                        .subscribe(
                                { data ->
                                    // 构造HttpResult
                                    getLiveData().value = data
                                },
                                { error ->
                                    // 错误处理
                                    handleException(error)
                                },
                                {
                                    // completed
                                    dialog.dismiss()
                                },
                                {
                                    // onSubscribe
                                    dialog.show()
                                }
                        )
        )
    }

    fun runWithNoToast() {
        getCompositeDisposable().add(
                fetchNetwork().compose(RxSchedulers.ioMain())
                        .subscribe(
                                { data ->
                                    // 构造HttpResult
                                    getLiveData().value = data
                                },
                                { error ->
                                    // 错误处理
                                    handleException(error, false)
                                }
                        )
        )
    }

    /**
     * 异常处理
     */
    private fun handleException(e: Throwable?, isAutoShowToast: Boolean = true) {
        when (e) {
            is HttpException -> { // 数据加载失败,请检查网络
                if (isAutoShowToast) appToast("数据加载失败,请检查网络！")
            }
            is ApiException -> { // 接收返回错误
                if (isAutoShowToast) appToast(e.msg ?: "")
            }
            else -> {
                if (isAutoShowToast) appToast("出错啦，请联系开发人员处理哦！")
                e?.printStackTrace()
            }
        }
    }

    abstract fun fetchNetwork(): Flowable<T>

    abstract fun getCompositeDisposable(): CompositeDisposable

    abstract fun getLiveData(): MediatorLiveData<T>

}