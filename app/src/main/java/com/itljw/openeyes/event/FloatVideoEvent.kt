package com.itljw.openeyes.event

/**
 * Created by JW on on 2019/5/26 19:48.
 * Email : 1481013718@qq.com
 * Description :
 */
class FloatVideoEvent(
        val videoId: Int,
        val playUrl: String,
        val videoHeight: Int,
        val videoWidth: Int,
        val currentPosition: Int
)