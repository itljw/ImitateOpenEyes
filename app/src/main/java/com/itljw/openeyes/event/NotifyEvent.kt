package com.itljw.openeyes.event

/**
 * Created by JW on on 2019/5/18 16:44.
 * Email : 1481013718@qq.com
 * Description :
 */
class NotifyEvent<T>(val arg: T)