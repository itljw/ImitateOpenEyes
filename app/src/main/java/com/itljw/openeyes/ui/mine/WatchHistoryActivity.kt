package com.itljw.openeyes.ui.mine

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.db.AppRoomDatabase
import com.itljw.openeyes.ui.mine.adapter.WatchHistoryAdapter
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import kotlinx.android.synthetic.main.activity_watch_history.*
import kotlinx.android.synthetic.main.layout_title_bar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import startActivity
import toast

/**
 * Created by JW on on 2019/5/28 21:17.
 * Email : 1481013718@qq.com
 * Description : 观看记录
 */
class WatchHistoryActivity : BaseActivity(), CoroutineScope by CoroutineScope(Dispatchers.Default) {

    private var page = 1
    private val adapter = WatchHistoryAdapter()

    override fun getLayoutId(): Int =
            R.layout.activity_watch_history

    companion object {
        private const val LIMIT = 10
        fun launch(mContext: Context?) =
                mContext?.startActivity<WatchHistoryActivity>()
    }

    override fun doBusiness() {
        tvTitle.text = "观看记录"
        ivBack.setOnClickListener { finish() }

        initRecyclerView()

        msvWatchHistory.showLoading()
        loadWatchHistoryData()
    }

    /**
     * 获取观看记录数据
     */
    private fun loadWatchHistoryData() {

        launch(Dispatchers.IO) {
            val videoDataList = AppRoomDatabase.getInstance(this@WatchHistoryActivity)
                    .videoDao().queryLimitVideo(page, LIMIT)

            withContext(Dispatchers.Main) {

                if (1 == page) { // 下拉刷新
                    srlWatchHistory.finishRefresh()

                    if (videoDataList.isNullOrEmpty()) {
                        msvWatchHistory.showEmpty()
                        return@withContext
                    }

                    msvWatchHistory.showContent()
                    adapter.replaceData(videoDataList)
                } else { // 加载更多

                    if (videoDataList.isNullOrEmpty()) {
                        toast("没有更多数据！")
                        srlWatchHistory.finishLoadMoreWithNoMoreData()
                        return@withContext
                    }

                    adapter.addData(adapter.data.size, videoDataList)
                    srlWatchHistory.finishLoadMore()
                }

            }
        }
    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {
        rvWatchHistory.layoutManager = LinearLayoutManager(this@WatchHistoryActivity)
        rvWatchHistory.adapter = adapter

        rvWatchHistory.addItemDecoration(
                DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL)
                        .apply {
                            ContextCompat.getDrawable(mContext, R.drawable.shape_divider)
                                    ?.let { setDrawable(it) }
                        })

        srlWatchHistory.setOnRefreshLoadMoreListener(mRefreshListener)

        // 事件
        adapter.setOnItemClickListener { _, _, position ->
            VideoDetailActivity.launch(mContext, adapter.data[position].videoId)
        }

    }

    // 刷新
    private val mRefreshListener = object : OnRefreshLoadMoreListener {
        override fun onLoadMore(refreshLayout: RefreshLayout) {
            page++
            loadWatchHistoryData()
        }

        override fun onRefresh(refreshLayout: RefreshLayout) {
            page = 1
            srlWatchHistory.setNoMoreData(false)
            loadWatchHistoryData()
        }

    }
}