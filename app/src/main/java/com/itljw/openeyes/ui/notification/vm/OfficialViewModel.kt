package com.itljw.openeyes.ui.notification.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.notification.entity.OfficialNotificationEntity

/**
 * Created by JW on on 2018/7/20 22:37.
 * Email : 1481013718@qq.com
 * Description :
 */
class OfficialViewModel : BaseViewModel() {

    val officialLiveData by lazy {
        MediatorLiveData<OfficialNotificationEntity>()
    }

    /**
     * 获取官方通知数据
     */
    fun getOfficialData() {

        addResponseResource(DataManager.getOfficialNotificationData(),
                object : BaseObserver<OfficialNotificationEntity>() {
                    override fun onSuccess(data: OfficialNotificationEntity?) {
                        officialLiveData.value = data
                    }
                })

    }

}