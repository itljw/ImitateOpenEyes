package com.itljw.openeyes.ui.search

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.support.v4.app.Fragment
import android.transition.Transition
import android.transition.TransitionInflater
import android.view.KeyEvent
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseSwipeBackActivity
import com.itljw.openeyes.ui.search.interf.OnSearchItemClickListener
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.fragment_home.*
import startActivity
import textChange
import toast

/**
 * Created by JW on on 2018/7/28 22:01.
 * Email : 1481013718@qq.com
 * Description : 搜索
 */
class SearchActivity : BaseSwipeBackActivity(), OnSearchItemClickListener, TextView.OnEditorActionListener {

    private var mSearchFragment: SearchFragment? = null
    private var mSearchResultFragment: SearchResultFragment? = null
    private var currentKeyword: String = ""

    override fun getLayoutId(): Int = R.layout.activity_search

    companion object {

        const val TYPE_SEARCH = 0x11
        const val TYPE_SEARCH_RESULT = 0x22

        fun launch(mContext: Context?) {
            mContext.startActivity<SearchActivity>()
        }
    }

    override fun doBusiness() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            setWindowAnimations()
//        }

        switchFragment(TYPE_SEARCH)

        etSearch.textChange(onTextChanged = { s, _, _, _ ->
            if (s != null && s.isNotEmpty()) {
                ivSearchCancel.visibility = View.VISIBLE
            } else {
                ivSearchCancel.visibility = View.GONE
                switchFragment(TYPE_SEARCH)
            }
        })

        etSearch.setOnEditorActionListener(this)

        // 搜索
        tvSearch.setOnClickListener {
            currentKeyword = etSearch.text.toString().apply {
                if (isNotBlank()) {
                    mSearchFragment?.putSearchHistoryKeyword(this)
                } else {
                    toast(getString(R.string.input_search_content))
                    return@setOnClickListener
                }
            }

            switchFragment(TYPE_SEARCH_RESULT)
        }

        // 取消搜索
        ivSearchCancel.setOnClickListener {
            // cancel
            etSearch.setText("") // 清空
            switchFragment(TYPE_SEARCH)
        }

        // 返回
        ivSearchBack.setOnClickListener { finish() }

    }

    private var currentFragment: Fragment? = null

    override fun onSearchItemClick(content: String) {
        currentKeyword = content
        etSearch.setText(currentKeyword)
        etSearch.setSelection(currentKeyword.length)
        switchFragment(TYPE_SEARCH_RESULT)
    }

    /**
     * 切换Fragment
     */
    private fun switchFragment(type: Int) {

        val transaction = supportFragmentManager.beginTransaction()
        currentFragment?.let {
            transaction.hide(it)
        }

        when (type) {
            TYPE_SEARCH -> { // 搜索
                mSearchFragment?.let {
                    transaction.show(it)
                } ?: SearchFragment.newInstance().let {
                    transaction.add(R.id.fl_search, it).show(it)
                    it.onSearchItemClickListener = this@SearchActivity
                    mSearchFragment = it
                }
                currentFragment = mSearchFragment
            }

            TYPE_SEARCH_RESULT -> { // 搜索结果
                mSearchResultFragment?.let {
                    it.setCurrentKeyword(currentKeyword)
                    transaction.show(it)
                } ?: SearchResultFragment.newInstance(currentKeyword).let {
                    transaction.add(R.id.fl_search, it).show(it)
                    mSearchResultFragment = it
                }

                currentFragment = mSearchResultFragment
            }
        }

        transaction.commitAllowingStateLoss()
    }


    @TargetApi(value = 21)
    private fun setWindowAnimations() {
        setupEnterAnimations()
        setupExitAnimations()
    }

    @TargetApi(value = 21)
    private fun setupEnterAnimations() {
        val transition = TransitionInflater.from(this)
                .inflateTransition(R.transition.changebounds_with_arcmotion)
        window.sharedElementEnterTransition = transition
        transition.addListener(object : Transition.TransitionListener {

            override fun onTransitionResume(transition: Transition?) {}
            override fun onTransitionPause(transition: Transition?) {}
            override fun onTransitionCancel(transition: Transition?) {}
            override fun onTransitionStart(transition: Transition?) {}

            override fun onTransitionEnd(transition: Transition?) {
                transition?.removeListener(this)
                ivHomeSearch.visibility = View.GONE
                animateRevealShow(rlSearchRoot)
            }
        })
    }

    /**
     * 展示动画
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun animateRevealShow(viewRoot: View) {
        val cx = (viewRoot.left + viewRoot.right) / 2
        val cy = (viewRoot.top + viewRoot.bottom) / 2
        val finalRadius = Math.max(viewRoot.width, viewRoot.height)

        val anim = ViewAnimationUtils.createCircularReveal(viewRoot, cx, cy, 0f, finalRadius.toFloat())
        viewRoot.visibility = View.VISIBLE
        anim.duration = resources.getInteger(R.integer.anim_duration_long).toLong()
        anim.interpolator = AccelerateInterpolator()
        anim.start()
    }

    private fun setupExitAnimations() {

    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {

        // 搜索
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {

            currentKeyword = etSearch.text.toString().apply {
                if (isNotBlank()) {
                    mSearchFragment?.putSearchHistoryKeyword(this)
                } else {
                    toast(getString(R.string.input_search_content))
                    return true
                }
            }

            switchFragment(TYPE_SEARCH_RESULT)

            return true
        }
        return false
    }

}