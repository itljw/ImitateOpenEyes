package com.itljw.openeyes.ui.video.adapter

import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.R
import com.itljw.openeyes.entity.TextCardMultiItem
import com.itljw.openeyes.entity.VideoSmallCardMultiItem
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.util.GlideUtil
import dp2px

/**
 * Created by JW on on 2018/9/22 19:51.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoDetailAdapter :
        BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder>(arrayListOf()) {

    init {
        addItemType(HomeConstant.ITEM_TYPE_TEXT_CARD, R.layout.item_text_card)
        addItemType(HomeConstant.ITEM_TYPE_VIDEO_SMALL_CARD, R.layout.item_video_small_card)
    }

    override fun convert(helper: BaseViewHolder?, item: MultiItemEntity?) {
        when (helper?.itemViewType) {
            HomeConstant.ITEM_TYPE_TEXT_CARD -> { // 文本
                if (item is TextCardMultiItem) {

                    // 隐藏关注、隐藏arrow
                    helper.getView<ImageView>(R.id.ivArrow).visibility = View.GONE
                    helper.getView<TextView>(R.id.tvFollow).visibility = View.GONE
                    // 设置padding
                    helper.getView<RelativeLayout>(R.id.rlRoot).apply {
                        setPadding(mContext.dp2px(15).toInt(), mContext.dp2px(15).toInt(), 0, 0)
                    }
                    // 设置title
                    helper.getView<TextView>(R.id.tvTitle).apply {
                        setTextColor(mContext.resources.getColor(R.color.color_white))
                        text = item.data.text
                        textSize = 12f
                    }

                }
            }
            HomeConstant.ITEM_TYPE_VIDEO_SMALL_CARD -> { // 左图片右文本
                if (item is VideoSmallCardMultiItem) {
                    val data = item.data
                    // 设置video背景图
                    if (data.cover != null) {
                        GlideUtil.displayRoundImage(mContext, data.cover.detail, helper.getView(R.id.ivVideoCover), 5, true)
                    }
                    // 设置video时长
                    val duration = data.duration

                    // 设置时长
                    helper.getView<TextView>(R.id.tvVideoDuration).apply {
                        // 设置透明度
                        background.mutate().alpha = 188
                        text = String.format("%s:%s", String.format("%02d", duration / 60), String.format("%02d", duration % 60))
                    }

                    // 设置类别
                    helper.getView<TextView>(R.id.tvVideoCategory).apply {
                        text = String.format("#%s", data.category)
                        setTextColor(ContextCompat.getColor(mContext, R.color.color_white))
                    }

                    // 设置title
                    helper.getView<TextView>(R.id.tvVideoTitle).apply {
                        setTextColor(ContextCompat.getColor(mContext, R.color.color_DDDDDD))
                        text = data.description
                    }

                    // 点击事件
                    helper.addOnClickListener(R.id.rlVideoInfo)
                }
            }
        }
    }
}