package com.itljw.openeyes.ui.common.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.common.entity.TabPagerEntity

/**
 * Created by JW on on 2019/5/20 22:30.
 * Email : 1481013718@qq.com
 * Description :
 */
class TabPagerViewModel : BaseViewModel() {

    val tabPagerLiveData by lazy { MediatorLiveData<TabPagerEntity>() }

    fun getPagerDataByUrl(apiUrl: String) {

        addResponseResource(DataManager.getTabPagerData(apiUrl),
                object : BaseObserver<TabPagerEntity>() {
                    override fun onSuccess(data: TabPagerEntity?) {
                        tabPagerLiveData.value = data
                    }
                })
    }

}