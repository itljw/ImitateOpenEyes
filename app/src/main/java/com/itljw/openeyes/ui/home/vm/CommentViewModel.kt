package com.itljw.openeyes.ui.home.vm

import android.arch.lifecycle.MediatorLiveData
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.home.entity.RecommendEntity
import com.itljw.openeyes.util.CategoryDealDataHelper.dealData

/**
 * Created by JW on on 2019/5/22 21:30.
 * Email : 1481013718@qq.com
 * Description :
 */
class CommentViewModel : BaseViewModel() {

    val mRefreshDataLiveData by lazy { MediatorLiveData<List<MultiItemEntity>>() }
    val mLoadMoreLiveData by lazy { MediatorLiveData<List<MultiItemEntity>>() }
    val mCommentLoadState by lazy { MediatorLiveData<LoadDataState>() }
    private var nextPageUrl = ""

    /**
     * 获取评论回复数据
     */
    fun getCommentRepliesData(videoId: Int, type: String, top: String) {
        addResponseResource(DataManager.getRepliesData(videoId, type, top), object : BaseObserver<RecommendEntity>() {
            override fun onSuccess(data: RecommendEntity?) {
                data?.let {
                    val dealDataList = dealData(it.itemList)
                    if (dealDataList.isEmpty()) {
                        mCommentLoadState.value = LoadDataState.EMPTY
                        return@let
                    }
                    mRefreshDataLiveData.value = dealDataList
                }
            }
        })
    }

    /**
     * 获取下一页评论数据
     */
    fun getNextCommentData() {
        if (nextPageUrl.isNotEmpty()) {
            mCommentLoadState.value = LoadDataState.LOAD_MORE
            addResponseResource(DataManager.getHomeCategoryData(nextPageUrl),
                    object : BaseObserver<RecommendEntity>() {
                        override fun onSuccess(data: RecommendEntity?) {
                            data?.let {
                                val dealDataList = dealData(it.itemList)
                                if (dealDataList.isEmpty()) {
                                    mCommentLoadState.value = LoadDataState.NO_MORE_DATA
                                    return@let
                                }
                                mLoadMoreLiveData.value = dealDataList
                            }
                        }
                    })
        } else {
            mCommentLoadState.value = LoadDataState.NO_MORE_DATA
        }
    }

}