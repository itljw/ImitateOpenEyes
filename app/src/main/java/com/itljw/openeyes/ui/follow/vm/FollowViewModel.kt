package com.itljw.openeyes.ui.follow.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.common.entity.TabPagerEntity

/**
 * Created by JW on on 2018/7/15 01:30.
 * Email : 1481013718@qq.com
 * Description :
 */
class FollowViewModel : BaseViewModel() {

    val mFollowListLiveData by lazy { MediatorLiveData<TabPagerEntity>() }

    /**
     * 获取关注列表
     */
    fun getFollowList() {

        addResponseResource(DataManager.getFollowListData(),
                object : BaseObserver<TabPagerEntity>() {
                    override fun onSuccess(data: TabPagerEntity?) {
                        mFollowListLiveData.value = data
                    }
                })

    }

}