package com.itljw.openeyes.ui.notification.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.widget.LinearLayoutManager
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.ui.notification.adapter.OfficialAdapter
import com.itljw.openeyes.ui.notification.vm.OfficialViewModel
import get
import kotlinx.android.synthetic.main.fragment_official_notification.*

/**
 * Created by JW on on 2018/7/20 22:13.
 * Email : 1481013718@qq.com
 * Description : 官方通知
 */
class OfficialNotificationFragment : BaseFragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get<OfficialViewModel>()
    }

    private val adapter by lazy { OfficialAdapter() }

    override fun getLayoutId(): Int {
        return R.layout.fragment_official_notification
    }

    companion object {
        fun newInstance(): OfficialNotificationFragment =
                OfficialNotificationFragment()
    }

    override fun doBusiness() {

        mRvOfficial.layoutManager = LinearLayoutManager(mContext)
        mRvOfficial.adapter = adapter

        viewModel.getOfficialData()
    }

    override fun handleObserver() {
        viewModel.officialLiveData.observe(this, Observer { data ->
            data?.run {
                adapter.replaceData(messageList)
            }
        })
    }
}