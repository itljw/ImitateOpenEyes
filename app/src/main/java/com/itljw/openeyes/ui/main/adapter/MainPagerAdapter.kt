package com.itljw.openeyes.ui.main.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.itljw.openeyes.ui.follow.FollowFragment
import com.itljw.openeyes.ui.home.HomeFragment
import com.itljw.openeyes.ui.mine.MineFragment
import com.itljw.openeyes.ui.notification.fragment.NotificationFragment

/**
 * Created by JW on on 2018/7/14 00:12.
 * Email : 1481013718@qq.com
 * Description :
 */
class MainPagerAdapter(
        fm: FragmentManager?,
        private var titles: Array<String>
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> {
                HomeFragment.newInstance()
            }
            1 -> {
                FollowFragment.newInstance()
            }
            2 -> {
                NotificationFragment.newInstance()
            }
            else -> {
                MineFragment.newInstance()
            }

        }
    }

    override fun getCount(): Int = titles.size

    override fun getPageTitle(position: Int): CharSequence? =
            titles[position]

}