package com.itljw.openeyes.ui.home.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.ui.home.entity.Item
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_adapter_horizontal_scroll_card.view.*

/**
 * Created by JW on on 2018/7/22 22:23.
 * Email : 1481013718@qq.com
 * Description :
 */
class HorizontalScrollCardAdapter(datas: List<Item>) :
        BaseQuickAdapter<Item, BaseViewHolder>(R.layout.item_adapter_horizontal_scroll_card, datas) {
    override fun convert(helper: BaseViewHolder, item: Item?) {

        item?.data?.image?.let {
            GlideUtil.displayRoundImage(mContext, it, helper.itemView.ivHorizontalImage, 5, true)
        }
    }
}