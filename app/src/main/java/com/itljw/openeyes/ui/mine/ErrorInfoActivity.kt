package com.itljw.openeyes.ui.mine

import android.content.Intent
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import kotlinx.android.synthetic.main.activity_error_info.*
import kotlinx.android.synthetic.main.layout_title_bar.*

/**
 * Created by JW on on 2018/9/2 20:28.
 * Email : 1481013718@qq.com
 * Description :
 */
class ErrorInfoActivity : BaseActivity() {

    override fun getLayoutId(): Int =
            R.layout.activity_error_info

    companion object {
        const val EXTRA_INFO = "extra_info"
    }


    override fun handleIntent(intent: Intent) {
        mTvExceptionInfo.text = intent.getStringExtra(EXTRA_INFO)
    }

    override fun doBusiness() {
        tvTitle.text = "错误详情"
        ivBack.setOnClickListener { finish() }
    }

}