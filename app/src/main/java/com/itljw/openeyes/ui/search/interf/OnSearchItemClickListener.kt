package com.itljw.openeyes.ui.search.interf

/**
 * Created by JW on on 2018/8/27 22:31.
 * Email : 1481013718@qq.com
 * Description :
 */
interface OnSearchItemClickListener {

    fun onSearchItemClick(content: String)

}