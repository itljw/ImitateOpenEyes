package com.itljw.openeyes.ui.search.adapter

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.R
import com.itljw.openeyes.ui.search.entity.ContentMultiItem
import com.itljw.openeyes.ui.search.entity.TitleMultiItem

/**
 * Created by JW on on 2018/8/5 19:42.
 * Email : 1481013718@qq.com
 * Description :
 */
class SearchAdapter(data: List<MultiItemEntity>) :
        BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder>(data) {

    companion object {
        const val TYPE_ITEM_TITLE = 0x11
        const val TYPE_ITEM_CONTENT = 0x22
    }

    init {
        addItemType(TYPE_ITEM_TITLE, R.layout.item_search_title)
        addItemType(TYPE_ITEM_CONTENT, R.layout.item_search_content)
    }

    override fun convert(helper: BaseViewHolder?, item: MultiItemEntity?) {
        when (helper?.itemViewType) {
            TYPE_ITEM_CONTENT -> {
                if (item is ContentMultiItem) {
                    helper.setText(R.id.tv_item_search_keyword, item.content)
                }
            }
            TYPE_ITEM_TITLE -> {
                if (item is TitleMultiItem) {
                    if (TitleMultiItem.TYPE_SEARCH_HISTORY == item.type) {
                        helper.setText(R.id.tv_item_search_title, mContext.getString(R.string.search_history))
                                .setVisible(R.id.tv_item_search_delete,true)
                    }else{
                        helper.setText(R.id.tv_item_search_title, mContext.getString(R.string.search_keyword))
                                .setVisible(R.id.tv_item_search_delete,false)
                    }
                    helper.addOnClickListener(R.id.tv_item_search_delete)
                }
            }
        }
    }
}