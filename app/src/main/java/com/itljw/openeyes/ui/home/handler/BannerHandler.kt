package com.itljw.openeyes.ui.home.handler

import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.BannerMultiItem
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_banner.view.*

/**
 * Created by JW on on 2019/4/18 00:42.
 * Email : 1481013718@qq.com
 * Description :
 */
object BannerHandler {

    fun bindData(helper: BaseViewHolder, item: BannerMultiItem) {

        helper.itemView.apply {

            GlideUtil.displayRoundImage(context, item.data.image, ivBannerImage, 5, true)

            ivBannerImage.setOnClickListener {
                ActionType.handleAction(context, item.data.actionUrl)
            }
        }

    }

}