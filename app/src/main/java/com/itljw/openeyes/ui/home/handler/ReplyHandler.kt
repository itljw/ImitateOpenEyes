package com.itljw.openeyes.ui.home.handler

import android.text.format.DateUtils
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.ReplyMultiItem
import com.itljw.openeyes.util.DateUtil
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_reply.view.*

/**
 * Created by JW on on 2019/3/3 12:30.
 * Email : 1481013718@qq.com
 * Description :
 */
object ReplyHandler {
    fun bindData(helper: BaseViewHolder, item: ReplyMultiItem) {

        helper.itemView.apply {

            val data = item.data

            val user = data.user // 用户信息

            // 设置回复名称、内容等
            tvReplyName.text = user.nickname
            tvReplyLike.text = data.likeCount.toString()
            tvReplyContent.text = data.message

            // 设置头像
            GlideUtil.displayCircleImage(context, user.avatar, ivReplyAvatar)

            // 设置回复时间
            tvReplyTime.text = data.createTime.let {
                var pattern = "yyyy/MM/dd"
                if (DateUtils.isToday(it)) {  // 今天
                    pattern = "HH:mm"
                }
                DateUtil.millis2String(pattern, it)
            }

            ivReplyAvatar.setOnClickListener {
                ActionType.handleAction(context, user.actionUrl)
            }
        }


    }
}