package com.itljw.openeyes.ui.notification.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.itljw.openeyes.ui.notification.fragment.InteractionFragment
import com.itljw.openeyes.ui.notification.fragment.OfficialNotificationFragment

/**
 * Created by JW on on 2018/7/20 22:02.
 * Email : 1481013718@qq.com
 * Description :
 */
class NotificationPagerAdapter(
        fm: FragmentManager,
        private val titles: Array<String>
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (0 == position) {
            OfficialNotificationFragment.newInstance()
        } else {
            InteractionFragment.newInstance()
        }
    }

    override fun getCount(): Int = titles.size

    override fun getPageTitle(position: Int): CharSequence? =
            titles[position]

}