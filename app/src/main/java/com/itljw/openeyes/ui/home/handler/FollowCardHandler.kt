package com.itljw.openeyes.ui.home.handler

import android.widget.LinearLayout
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.FollowCardMultiItem
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_follow_card.view.*

/**
 * Created by JW on on 2018/7/25 00:14.
 * Email : 1481013718@qq.com
 * Description : FollowCard数据处理
 */
object FollowCardHandler {

    fun bindData(helper: BaseViewHolder, item: FollowCardMultiItem, isShowDivider: Boolean = false) {

        helper.itemView.apply {
            val header = item.header
            val content = item.content

            // 设置video背景
            content.data.cover?.run {
                GlideUtil.displayRoundImage(context, detail, ivVideoCover, 5, true)
            }

            val duration = content.data.duration

            // 设置时长
            tvVideoDuration.apply {
                background.mutate().alpha = 255 / 2 // 设置透明度
                text = String.format("%s:%s", String.format("%02d", duration / 60), String.format("%02d", duration % 60))
            }

            // 设置头像
            GlideUtil.displayCircleImageWithBorder(context, header.icon, ivAvatar)

            // 设置title、描述
            tvTitle.text = header.title
            tvReplyMsg.text = header.description

            if (isShowDivider) {
                // 显示下划线
                llRoot.showDividers = LinearLayout.SHOW_DIVIDER_END
            }

            // 事件处理
            rlInfo.setOnClickListener {
                ActionType.handleAction(context, header.actionUrl)
            }

            ivVideoCover.setOnClickListener {
                VideoDetailActivity.launch(context, content.data.id.toInt())
            }

        }

    }

}