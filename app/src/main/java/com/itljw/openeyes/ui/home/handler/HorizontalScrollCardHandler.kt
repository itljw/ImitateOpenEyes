package com.itljw.openeyes.ui.home.handler

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.HorizontalScrollCardMultiItem
import com.itljw.openeyes.ui.home.adapter.HorizontalScrollCardAdapter
import com.itljw.openeyes.wiget.SpaceItemDecoration
import dp2px
import kotlinx.android.synthetic.main.item_horizontal_scroll_card.view.*

/**
 * Created by JW on on 2018/7/25 23:34.
 * Email : 1481013718@qq.com
 * Description :
 */
object HorizontalScrollCardHandler {

    fun bindData(helper: BaseViewHolder, item: HorizontalScrollCardMultiItem) {

        helper.itemView.apply {

            val itemList = item.itemList

            rvHorizontalScroll.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            // 重复添加问题
            if (rvHorizontalScroll.itemDecorationCount == 0) { // 判断是否添加了分割线
                rvHorizontalScroll.addItemDecoration(SpaceItemDecoration(context.dp2px(3).toInt()))
                PagerSnapHelper().attachToRecyclerView(rvHorizontalScroll)
            }

            val adapter = HorizontalScrollCardAdapter(itemList)
            rvHorizontalScroll.adapter = adapter
            adapter.setOnItemClickListener { _, _, position ->
                val itemData = itemList[position]
                ActionType.handleAction(context, itemData.data.actionUrl)
            }

        }

    }
}