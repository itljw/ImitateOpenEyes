package com.itljw.openeyes.ui.home.adapter

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.ui.home.entity.Item
import com.itljw.openeyes.util.GlideUtil

/**
 * Created by JW on on 2019/4/18 23:58.
 * Email : 1481013718@qq.com
 * Description :
 */
class AllCategoriesAdapter : BaseQuickAdapter<Item, BaseViewHolder>(
        R.layout.item_all_categories
) {

    override fun convert(helper: BaseViewHolder, item: Item?) {
        item?.data?.apply {
            helper.setText(R.id.tv_item_all_categories_title, title)
            GlideUtil.displayImage(mContext, image, helper.getView(R.id.iv_item_all_categories_bg))
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        val layoutManager = recyclerView.layoutManager
        if (layoutManager is GridLayoutManager) {
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {

                    val dataType = data[position].type
                    return if ("rectangleCard" == dataType) {
                        layoutManager.spanCount
                    } else {
                        1
                    }
                }
            }
        }

    }

}