package com.itljw.openeyes.ui.home.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.home.entity.Item
import com.itljw.openeyes.ui.home.entity.RecommendEntity

/**
 * Created by JW on on 2019/4/18 23:27.
 * Email : 1481013718@qq.com
 * Description :
 */
class AllCategoriesViewModel : BaseViewModel() {

    val mCategoriesLiveData by lazy { MediatorLiveData<List<Item>>() }
    val mCategoriesLoadState by lazy { MediatorLiveData<LoadDataState>() }

    /**
     * 获取全部分类数据
     */
    fun getAllCategoriesData(isRefresh: Boolean = true) {

        if (!isRefresh) {
            mCategoriesLoadState.value = LoadDataState.LOADING
        }

        addResponseResource(DataManager.getAllCategories(), object : BaseObserver<RecommendEntity>() {
            override fun onSuccess(data: RecommendEntity?) {
                data?.let {
                    val itemList = data.itemList
                    if (itemList.isEmpty()) {
                        mCategoriesLoadState.value = LoadDataState.EMPTY
                        return@let
                    }
                    mCategoriesLiveData.value = itemList
                }
            }

            override fun onError(code: Int?, msg: String?) {
                super.onError(code, msg)
                mCategoriesLoadState.value = LoadDataState.ERROR
            }
        })
    }

}