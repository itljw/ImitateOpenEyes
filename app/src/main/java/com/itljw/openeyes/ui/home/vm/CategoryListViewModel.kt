package com.itljw.openeyes.ui.home.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.home.entity.RecommendEntity

/**
 * Created by JW on on 2018/9/5 22:00.
 * Email : 1481013718@qq.com
 * Description :
 */
class CategoryListViewModel : BaseViewModel() {

    val mCategoryListLiveData by lazy { MediatorLiveData<RecommendEntity>() }

    /**
     * 获取分类标签List数据
     */
    fun getCategoryListData() {

        addResponseResource(DataManager.getCategoryListData(),
                object : BaseObserver<RecommendEntity>() {
                    override fun onSuccess(data: RecommendEntity?) {
                        mCategoryListLiveData.value = data
                    }
                })
    }

}