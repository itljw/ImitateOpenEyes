package com.itljw.openeyes.ui.search.vm

import android.arch.lifecycle.MediatorLiveData
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.google.gson.JsonElement
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.home.entity.RecommendEntity
import com.itljw.openeyes.util.CategoryDealDataHelper.dealData

/**
 * Created by JW on on 2018/8/5 16:11.
 * Email : 1481013718@qq.com
 * Description :
 */
class SearchViewModel : BaseViewModel() {

    val searchHotKeyLiveData by lazy { MediatorLiveData<JsonElement>() }
    val hotKeyLoadState by lazy { MediatorLiveData<LoadDataState>() }

    val searchKeywordLiveData by lazy { MediatorLiveData<List<MultiItemEntity>>() }
    val keywordLoadState by lazy { MediatorLiveData<LoadDataState>() }

    private var keywordNextPageUrl = ""

    /**
     * 获取搜索热词
     */
    fun getSearchHotKeyData() {
        hotKeyLoadState.value = LoadDataState.LOADING

        addResponseResource(DataManager.getSearchHotKeyData(), object : BaseObserver<JsonElement>() {
            override fun onSuccess(data: JsonElement?) {
                searchHotKeyLiveData.value = data
            }

            override fun onError(code: Int?, msg: String?) {
                super.onError(code, msg)
                hotKeyLoadState.value = LoadDataState.ERROR
            }
        })
    }

    /**
     * 根据关键字搜索数据
     */
    fun getSearchByKeyword(keyword: String) {

        keywordLoadState.value = LoadDataState.LOADING
        addResponseResource(DataManager.getSearchByKeyword(keyword),
                object : BaseObserver<RecommendEntity>() {
                    override fun onSuccess(data: RecommendEntity?) {
                        handleKeywordResultData(data)
                    }

                    override fun onError(code: Int?, msg: String?) {
                        super.onError(code, msg)
                        keywordLoadState.value = LoadDataState.ERROR
                    }
                })
    }

    /**
     * 获取关键字下一页数
     */
    fun getNextSearchResult() {

        if (keywordNextPageUrl.isNotEmpty()) {
            keywordLoadState.value = LoadDataState.LOAD_MORE
            addResponseResource(DataManager.getHomeCategoryData(keywordNextPageUrl),
                    object : BaseObserver<RecommendEntity>() {
                        override fun onSuccess(data: RecommendEntity?) {
                            handleKeywordResultData(data)
                        }

                        override fun onError(code: Int?, msg: String?) {
                            super.onError(code, msg)
                            keywordLoadState.value = LoadDataState.ERROR
                        }
                    })
        } else { // 没有更多数据了
            keywordLoadState.value = LoadDataState.NO_MORE_DATA
        }
    }

    /**
     * 处理关键字结果数据
     */
    private fun handleKeywordResultData(data: RecommendEntity?) {
        data?.let {
            val dealDataList = dealData(it.itemList)
            if (dealDataList.isEmpty()) {
                keywordLoadState.value = LoadDataState.EMPTY
                return@let
            }

            keywordNextPageUrl = it.nextPageUrl ?: ""
            searchKeywordLiveData.value = dealDataList
        }
    }

}