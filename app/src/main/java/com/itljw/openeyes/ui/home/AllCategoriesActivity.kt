package com.itljw.openeyes.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.home.adapter.AllCategoriesAdapter
import com.itljw.openeyes.ui.home.vm.AllCategoriesViewModel
import com.itljw.openeyes.wiget.GridSpacingItemDecoration
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshListener
import dp2px
import get
import kotlinx.android.synthetic.main.activity_all_categories.*
import kotlinx.android.synthetic.main.layout_title_bar.*

/**
 * Created by JW on on 2019/4/18 23:19.
 * Email : 1481013718@qq.com
 * Description : 全部分类
 */
class AllCategoriesActivity : BaseActivity(), OnRefreshListener, BaseQuickAdapter.OnItemClickListener {

    private val mAllCategoriesViewModel by lazy {
        ViewModelProviders.of(this).get<AllCategoriesViewModel>()
    }

    private val adapter = AllCategoriesAdapter()

    private val spanCount = 2

    override fun getLayoutId(): Int =
            R.layout.activity_all_categories

    companion object {
        fun launch(mContext: Context?) {
            mContext?.startActivity(Intent(mContext, AllCategoriesActivity::class.java))
        }
    }

    override fun doBusiness() {

        tvTitle.text = getString(R.string.home_all_categories)
        ivBack.setOnClickListener { finish() }

        initRecyclerView()

        mAllCategoriesViewModel.getAllCategoriesData(false)

    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {
        rvAllCategories.layoutManager = GridLayoutManager(this, spanCount)
        rvAllCategories.addItemDecoration(GridSpacingItemDecoration(mContext.dp2px(1).toInt()))

        rvAllCategories.adapter = adapter

        srlAllCategories.setEnableLoadMore(false) // 禁用加载更多
        srlAllCategories.setOnRefreshListener(this)
        adapter.onItemClickListener = this@AllCategoriesActivity
    }

    override fun handleObserver() {
        super.handleObserver()

        mAllCategoriesViewModel.mCategoriesLoadState.observe(this, Observer { state ->
            when (state) {
                LoadDataState.LOADING ->
                    msvAllCategories?.showLoading()

                LoadDataState.EMPTY -> {
                    if (adapter.data.isEmpty()) {
                        msvAllCategories?.showEmpty()
                    }
                }

                LoadDataState.ERROR -> {
                    if (adapter.data.isEmpty()) {
                        msvAllCategories?.showError()
                    }
                    srlAllCategories.finishRefresh()
                }
                else -> {
                }
            }
        })

        mAllCategoriesViewModel.mCategoriesLiveData.observe(this, Observer { data ->

            msvAllCategories?.showContent()
            data?.let {
                adapter.replaceData(it)
                srlAllCategories.finishRefresh()
            }
        })
    }

    // 下拉刷新
    override fun onRefresh(refreshLayout: RefreshLayout) {
        mAllCategoriesViewModel.getAllCategoriesData()
    }

    // Item事件
    override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
        if (adapter != null && adapter is AllCategoriesAdapter) {
            ActionType.handleAction(mContext, adapter.data[position].data.actionUrl)
        }
    }

}