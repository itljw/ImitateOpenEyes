package com.itljw.openeyes.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import bundleOf
import com.chad.library.adapter.base.BaseQuickAdapter
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.entity.PictureFollowCardMultiItem
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.home.adapter.CategoryAdapter
import com.itljw.openeyes.ui.home.vm.CategoryViewModel
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.wiget.photoview.PictureViewerActivity
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import kotlinx.android.synthetic.main.fragment_recycler_view.*

/**
 * Created by JW on on 2018/7/26 21:59.
 * Email : 1481013718@qq.com
 * Description :
 */
class CategoryFragment : BaseFragment(), BaseQuickAdapter.OnItemChildClickListener {

    private  var apiUrl: String = ""

    private val mCategoryAdapter by lazy { CategoryAdapter() }

    private val mCategoryViewModel by lazy {
        ViewModelProviders.of(this).get(CategoryViewModel::class.java)
    }

    override fun getLayoutId(): Int =
            R.layout.fragment_recycler_view

    companion object {

        private const val ARG_API_URL = "arg_api_url"

        fun newInstance(apiUrl: String): CategoryFragment {
            return CategoryFragment().apply {
                arguments = bundleOf(
                        ARG_API_URL to apiUrl
                )
            }
        }
    }

    override fun handleArgument(arguments: Bundle?) {
        super.handleArgument(arguments)
        arguments?.run{
            apiUrl = getString(ARG_API_URL)
        }
    }

    override fun doBusiness() {

        initRecyclerView()

        mMultipleStatusLayout = msvStatus

        mCategoryViewModel.getCategoryData(apiUrl, false)
    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {
        rvRecycler.layoutManager = LinearLayoutManager(mContext)
        rvRecycler.adapter = mCategoryAdapter
        mCategoryAdapter.openLoadAnimation()

        srlRecycler.setOnRefreshLoadMoreListener(mRefreshListener)
        mCategoryAdapter.onItemChildClickListener = this
    }

    override fun handleObserver() {

        // 状态
        mCategoryViewModel.mCategoryLoadState.observe(this, Observer { state ->
            when (state) {
                LoadDataState.LOADING -> {
                    mMultipleStatusLayout?.showLoading()
                }

                LoadDataState.LOAD_MORE -> {
                    srlRecycler.setNoMoreData(false)
                }

                LoadDataState.EMPTY -> {
                    if (mCategoryAdapter.data.isEmpty()) {
                        mMultipleStatusLayout?.showEmpty()
                    }
                }

                LoadDataState.NO_MORE_DATA -> {
                    srlRecycler.setNoMoreData(true)
                }

                LoadDataState.ERROR -> {
                    if (mCategoryAdapter.data.isEmpty()) {
                        mMultipleStatusLayout?.showError()
                    }
                    srlRecycler.finishRefresh()
                    srlRecycler.finishLoadMore()
                }
            }
        })

        // 上拉
        mCategoryViewModel.mLoadMoreDataLiveData.observe(this, Observer { dataList ->
            dataList?.let {
                mCategoryAdapter.addData(mCategoryAdapter.data.size, dataList)
                srlRecycler.finishLoadMore()
            }
        })

        // 下拉
        mCategoryViewModel.mRefreshDataLiveData.observe(this, Observer { dataList ->

            dataList?.let {
                mMultipleStatusLayout?.showContent()
                mCategoryAdapter.replaceData(dataList)
                srlRecycler.finishRefresh()
            }

        })
    }

    // 刷新
    private val mRefreshListener = object : OnRefreshLoadMoreListener {
        override fun onLoadMore(refreshLayout: RefreshLayout) {
            mCategoryViewModel.getNextCategoryData()
        }

        override fun onRefresh(refreshLayout: RefreshLayout) {
            mCategoryViewModel.getCategoryData(apiUrl)
        }
    }

    override fun onItemChildClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {

        val item = mCategoryAdapter.data[position]

        when (view?.id) {
            R.id.ivVideoCover -> {
                if (item is PictureFollowCardMultiItem) {
                    val type = item.data.content.type
                    when (type) {
                        "ugcPicture" -> { // 图片
                            PictureViewerActivity.launch(activity!!, view, item.data.content.data.url)
                        }
                        "video" -> {
                            VideoDetailActivity.launch(mContext, item.data.id.toInt())
                        }
                    }
                }
            }

            R.id.ll_picture_follow_card -> {
                if (item is PictureFollowCardMultiItem) {

                    val type = item.data.content.type
                    when (type) {
                        "ugcPicture" -> { // 图片
                            PictureViewerActivity.launchWithMultiImage(activity!!, view, item.data.content.data.urls)
                        }
                        "video" -> {
                            VideoDetailActivity.launch(mContext, item.data.id.toInt())
                        }
                    }

                }
            }
        }
    }

}