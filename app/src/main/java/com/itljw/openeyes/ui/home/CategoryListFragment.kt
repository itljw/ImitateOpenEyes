package com.itljw.openeyes.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.callback.ItemDragAndSwipeCallback
import com.chad.library.adapter.base.listener.OnItemDragListener
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseSwipeBackFragment
import com.itljw.openeyes.ui.home.adapter.CategoryListAdapter
import com.itljw.openeyes.ui.home.vm.CategoryListViewModel
import kotlinx.android.synthetic.main.fragment_category_list.*
import kotlinx.android.synthetic.main.layout_title_bar.*


/**
 * Created by JW on on 2018/9/4 21:17.
 * Email : 1481013718@qq.com
 * Description ： 分类标签List
 */
class CategoryListFragment : BaseSwipeBackFragment(), BaseQuickAdapter.OnItemClickListener {

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get(CategoryListViewModel::class.java)
    }

    private val adapter = CategoryListAdapter()

    override fun getLayoutId(): Int =
            R.layout.fragment_category_list

    companion object {
        fun newInstance() =
                CategoryListFragment()
    }

    override fun doBusiness() {

        tvTitle.text = getString(R.string.home_category_list)

        initRecyclerView()

        mViewModel.getCategoryListData()

        // 返回
        ivBack.setOnClickListener { activity?.finish() }
    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {

        rvCategoryList.layoutManager = LinearLayoutManager(mContext)
        rvCategoryList.adapter = adapter

        val mItemDragAndSwipeCallback = ItemDragAndSwipeCallback(adapter)
        val mItemTouchHelper = ItemTouchHelper(mItemDragAndSwipeCallback)
        mItemTouchHelper.attachToRecyclerView(rvCategoryList)
        adapter.enableDragItem(mItemTouchHelper) // 设置可拖拽
        adapter.openLoadAnimation()

        adapter.onItemClickListener = this@CategoryListFragment

        adapter.setOnItemDragListener(object : OnItemDragListener {
            override fun onItemDragMoving(source: RecyclerView.ViewHolder?, from: Int, target: RecyclerView.ViewHolder?, to: Int) {
                (source as BaseViewHolder).itemView.setBackgroundResource(R.color.color_white)
            }

            override fun onItemDragStart(viewHolder: RecyclerView.ViewHolder?, pos: Int) {
                (viewHolder as BaseViewHolder).itemView.setBackgroundResource(R.color.color_white)
            }

            override fun onItemDragEnd(viewHolder: RecyclerView.ViewHolder?, pos: Int) {
                (viewHolder as BaseViewHolder).itemView.setBackgroundResource(R.color.color_white)
            }

        })
    }

    override fun handleObserver() {
        mViewModel.mCategoryListLiveData.observe(this, Observer { data ->

            data?.let {
                adapter.replaceData(it.itemList)
            }
        })
    }

    override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
        if (adapter != null && adapter is CategoryListAdapter) {
            CategoryDetailActivity.launch(mContext, adapter.data[position].data.id.toInt())
        }
    }

}