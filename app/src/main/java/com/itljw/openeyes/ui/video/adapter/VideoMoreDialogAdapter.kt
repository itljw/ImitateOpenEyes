package com.itljw.openeyes.ui.video.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.ui.home.entity.PlayInfo

/**
 * Created by JW on on 2019/3/8 00:27.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoMoreDialogAdapter<T>(data: List<T>?) : BaseQuickAdapter<T, BaseViewHolder>(
        R.layout.item_video_more_dialog, data
) {
    override fun convert(helper: BaseViewHolder?, item: T) {
        if (item is String) {
            helper?.setText(R.id.tv_item_video_more_content, item)
        } else if (item is PlayInfo) {
            helper?.setText(R.id.tv_item_video_more_content, item.name)
        }
    }


}