package com.itljw.openeyes.ui.video.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.ui.video.entity.VideoTagsTabEntity
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager

/**
 * Created by JW on on 2019/3/1 22:15.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoTagsViewModel : BaseViewModel() {

    val mVideoTagsLiveData by lazy { MediatorLiveData<VideoTagsTabEntity>() }

    /**
     * 获取标签视频数据
     */
    fun getVideoTagsTabData(id: Int) {

        addResponseResource(DataManager.getVideoTagsTabData(id),
                object : BaseObserver<VideoTagsTabEntity>() {
                    override fun onSuccess(data: VideoTagsTabEntity?) {
                        mVideoTagsLiveData.value = data
                    }
                })
    }

}