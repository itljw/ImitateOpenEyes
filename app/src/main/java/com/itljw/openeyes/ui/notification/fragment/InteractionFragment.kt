package com.itljw.openeyes.ui.notification.fragment

import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment

/**
 * Created by JW on on 2018/7/20 22:17.
 * Email : 1481013718@qq.com
 * Description : 互动
 */
class InteractionFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_interaction
    }

    companion object {
        fun newInstance(): InteractionFragment =
                InteractionFragment()

    }

    override fun doBusiness() {
    }
}