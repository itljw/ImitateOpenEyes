package com.itljw.openeyes.ui.notification.entity


data class OfficialNotificationEntity(
        val messageList: List<Message>,
        val updateTime: Long // 1520841600000
) {

    data class Message(
            val actionUrl: String, // eyepetizer://webview/?title=%E8%AF%9D%E9%A2%98%E4%BA%92%E5%8A%A8&url=http%3A%2F%2Fwww.kaiyanapp.com%2Ftopic_article.html%3Fnid%3D30%26cookie%3D%26shareable%3Dtrue
            val content: String, // 开眼话题互动：还有不到一周 2017 年就要正式成为你永久的回忆了，在这一年中有哪些让你难忘的事情，有哪些收获，你又是什么的状态呢？
            val date: Long, // 1514347200000
            val icon: String, // http://img.wdjimg.com/image/video/418d281e65bf010c38c7b07bdd7b6a94_0_0.png
            val id: Int, // 451098
            val ifPush: Boolean, // false
            val pushStatus: Int, // 0
            val title: String // 官方通知
    )
}