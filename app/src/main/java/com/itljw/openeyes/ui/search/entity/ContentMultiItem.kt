package com.itljw.openeyes.ui.search.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.ui.search.adapter.SearchAdapter

/**
 * Created by JW on on 2018/8/5 19:57.
 * Email : 1481013718@qq.com
 * Description :
 */
class ContentMultiItem(val content:String) : MultiItemEntity {

    override fun getItemType(): Int = SearchAdapter.TYPE_ITEM_CONTENT
}