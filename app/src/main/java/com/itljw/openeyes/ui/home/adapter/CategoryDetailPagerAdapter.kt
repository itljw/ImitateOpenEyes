package com.itljw.openeyes.ui.home.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.itljw.openeyes.ui.common.entity.TabInfo
import com.itljw.openeyes.ui.home.CategoryFragment

/**
 * Created by JW on on 2018/9/6 23:41.
 * Email : 1481013718@qq.com
 * Description :
 */
class CategoryDetailPagerAdapter(
        fm: FragmentManager,
        private val tabList: MutableList<TabInfo.Tab>
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment =
            CategoryFragment.newInstance(tabList[position].apiUrl)

    override fun getCount(): Int = tabList.size

    override fun getPageTitle(position: Int): CharSequence? =
            tabList[position].name

    /**
     * 替换数据
     */
    fun replaceData(data: Collection<TabInfo.Tab>) {
        // 不是同一个引用才清空列表
        if (data !== tabList) {
            tabList.clear()
            tabList.addAll(data)
        }
        notifyDataSetChanged()
    }

}