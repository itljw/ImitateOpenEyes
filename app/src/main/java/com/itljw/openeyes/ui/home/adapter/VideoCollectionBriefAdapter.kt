package com.itljw.openeyes.ui.home.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.ui.home.entity.Item
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_adapt_video_collection_with_brief.view.*

/**
 * Created by JW on on 2018/7/22 23:44.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoCollectionBriefAdapter(data: List<Item>) :
        BaseQuickAdapter<Item, BaseViewHolder>(R.layout.item_adapt_video_collection_with_brief, data) {
    override fun convert(helper: BaseViewHolder, item: Item?) {

        helper.itemView.apply {

            // 视频封面
            GlideUtil.displayRoundImage(mContext, item?.data?.cover?.detail
                    ?: "", ivVideoCover, 5, true)

            // 设置视频时长
            item?.data?.duration?.let {
                tvVideoDuration.apply {
                    background.mutate().alpha = 255 / 2 // 设置透明度
                    text = String.format("%s:%s", String.format("%02d", it / 60), String.format("%02d", it % 60))
                }
            }

            // 设置标题、分类
            tvVideoTitle.text = item?.data?.title
            tvVideoCategory.text = String.format("#%s", item?.data?.category)

            // 事件处理
            ivVideoCover.setOnClickListener {
                item?.data?.also {
                    VideoDetailActivity.launch(mContext, it.id.toInt()) // 视频
                }
            }

            rlVideoBottom.setOnClickListener {
                item?.data?.also {
                    VideoDetailActivity.launch(mContext, it.id.toInt())
                }
            }
        }
    }
}