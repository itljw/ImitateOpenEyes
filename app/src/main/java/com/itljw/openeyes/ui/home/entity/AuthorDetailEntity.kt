package com.itljw.openeyes.ui.home.entity

import com.itljw.openeyes.ui.common.entity.TabInfo

/**
 * Created by JW on on 2019/5/15 22:13.
 * Email : 1481013718@qq.com
 * Description :
 */
data class AuthorDetailEntity(
        val pgcInfo: PgcInfo?,
        val userInfo:PgcInfo?,
        val tabInfo: TabInfo
) {

    data class PgcInfo(
            val actionUrl: String, // eyepetizer://pgc/detail/596/?title=%E5%8D%81%E5%85%A8%E8%8F%9C%E8%B0%B1%20Allrecipes&userType=PGC&tabIndex=1
            val area: String,
            val brief: String, // 作品被喜欢 11074 次  /  作品被分享 4163 次
            val collectCount: Int, // 11074
            val cover: String?, // null
            val dataType: String, // PgcInfo
            val description: String, // 包罗万象的家常菜谱，及时变出一桌菜也不是问题！
            val expert: Boolean, // false
            val follow: Follow,
            val followCount: Int, // 4787
            val followCountActionUrl: String?, // eyepetizer://common/?title=%E6%89%80%E6%9C%89%E7%B2%89%E4%B8%9D&url=http%3A%2F%2Fbaobab.kaiyanapp.com%2Fapi%2Fv1%2Ffollow%2FmyFansList%3Fuid%3D301117138
            val gender: String,
            val icon: String, // http://img.kaiyanapp.com/fa6903496cd2a514d191c3074398980f.jpeg?imageMogr2/quality/60/format/jpg
            val id: Int, // 596
            val medalsActionUrl: String, // eyepetizer://myMedal/?uid=301117138
            val medalsNum: Int, // 0
            val myFollowCount: Int, // 19
            val myFollowCountActionUrl: String?, // eyepetizer://userFollow/301117138
            val name: String, // 十全菜谱 Allrecipes
            val registDate: Long, // -1
            val self: Boolean, // false
            val shareCount: Int, // 4163
            val shield: Shield,
            val videoCount: Int, // 272
            val videoCountActionUrl: String // eyepetizer://pgc/detail/596/?title=%E5%8D%81%E5%85%A8%E8%8F%9C%E8%B0%B1%20Allrecipes&userType=PGC&tabIndex=1
    ) {
        data class Shield(
                val itemId: Int, // 596
                val itemType: String, // author
                val shielded: Boolean // false
        )

        data class Follow(
                val followed: Boolean, // false
                val itemId: Int, // 596
                val itemType: String // author
        )
    }
}