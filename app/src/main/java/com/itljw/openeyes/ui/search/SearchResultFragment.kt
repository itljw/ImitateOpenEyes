package com.itljw.openeyes.ui.search

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import bundleOf
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.home.adapter.CategoryAdapter
import com.itljw.openeyes.ui.search.vm.SearchViewModel
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener
import get
import kotlinx.android.synthetic.main.fragment_search_result.*

/**
 * Created by JW on on 2018/8/27 22:13.
 * Email : 1481013718@qq.com
 * Description :
 */
class SearchResultFragment : BaseFragment() {

    private var keyword: String = ""
    private val mCategoryAdapter by lazy { CategoryAdapter() }
    private val mSearchResultViewModel by lazy {
        ViewModelProviders.of(this).get<SearchViewModel>()
    }

    override fun getLayoutId(): Int =
            R.layout.fragment_search_result

    companion object {

        private const val ARG_KEYWORD = "arg_keyword"

        fun newInstance(keyword: String): SearchResultFragment =
                SearchResultFragment().apply {
                    arguments = bundleOf(ARG_KEYWORD to keyword)
                }
    }

    override fun handleArgument(arguments: Bundle?) {
        super.handleArgument(arguments)
        keyword = arguments?.getString(ARG_KEYWORD) ?: ""
    }

    override fun doBusiness() {
        initRecyclerView()

        mMultipleStatusLayout = mStatusViewSearchResult

        mSearchResultViewModel.getSearchByKeyword(keyword)
    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {
        rvSearchResult.layoutManager = LinearLayoutManager(mContext)
        rvSearchResult.adapter = mCategoryAdapter
        srlSearchResult.setOnLoadMoreListener(mRefreshListener)
        srlSearchResult.setEnableRefresh(false) // 禁用下拉刷新
    }

    override fun handleObserver() {

        mSearchResultViewModel.keywordLoadState.observe(this, Observer { state ->
            when (state) {
                LoadDataState.LOADING -> { // 加载
                    mCategoryAdapter.data.clear()
                    mMultipleStatusLayout?.showLoading()
                }

                LoadDataState.LOAD_MORE -> { // 可加载更多
                    srlSearchResult.setNoMoreData(false)
                }

                LoadDataState.EMPTY -> // 数据为空
                    if (mCategoryAdapter.data.isEmpty()) { // 没有数据，显示空布局
                        mMultipleStatusLayout?.showEmpty()
                    }

                LoadDataState.NO_MORE_DATA -> { // 没有更多数据
                    srlSearchResult.setNoMoreData(true)
                }

                LoadDataState.ERROR -> {
                    if (mCategoryAdapter.data.isEmpty()) { // 显示错误布局
                        mMultipleStatusLayout?.showError()
                    }
                    srlSearchResult.finishLoadMore()
                }
                else -> {
                }
            }
        })

        mSearchResultViewModel.searchKeywordLiveData.observe(this, Observer { dataList ->
            mMultipleStatusLayout?.showContent()
            dataList?.let {
                mCategoryAdapter.addData(mCategoryAdapter.data.size, it)
                srlSearchResult.finishLoadMore()
            }
        })
    }

    /**
     * 设置关键字
     */
    fun setCurrentKeyword(currentKeyword: String) {
        mSearchResultViewModel.getSearchByKeyword(currentKeyword)
    }

    // 加载更多
    private val mRefreshListener = OnLoadMoreListener {
        mSearchResultViewModel.getNextSearchResult()
    }

}