package com.itljw.openeyes.ui.home.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.home.entity.AuthorDetailEntity

/**
 * Created by JW on on 2019/5/15 22:10.
 * Email : 1481013718@qq.com
 * Description :
 */
class AuthorDetailViewModel : BaseViewModel() {

    val mAuthorDetailLiveData by lazy { MediatorLiveData<AuthorDetailEntity>() }

    /**
     * 获取作者详情数据
     */
    fun getAuthorDetailData(id: Int, userType: String) {

        addResponseResource(DataManager.getAuthorDetailData(id, userType),
                object : BaseObserver<AuthorDetailEntity>() {
            override fun onSuccess(data: AuthorDetailEntity?) {
                mAuthorDetailLiveData.value = data
            }
        })
    }

}