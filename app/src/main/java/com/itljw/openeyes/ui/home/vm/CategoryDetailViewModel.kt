package com.itljw.openeyes.ui.home.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.home.entity.CategoryDetailEntity

/**
 * Created by JW on on 2018/9/6 23:25.
 * Email : 1481013718@qq.com
 * Description :
 */
class CategoryDetailViewModel : BaseViewModel() {

    val mCategoryDetailLiveData by lazy {
        MediatorLiveData<CategoryDetailEntity>()
    }

    /**
     * 通过id获取分类详情数据
     */
    fun getCategoryDetailById(id: Int) {

        addResponseResource(DataManager.getCategoryDetailById(id),
                object : BaseObserver<CategoryDetailEntity>() {
                    override fun onSuccess(data: CategoryDetailEntity?) {
                        mCategoryDetailLiveData.value = data
                    }
                })
    }

}