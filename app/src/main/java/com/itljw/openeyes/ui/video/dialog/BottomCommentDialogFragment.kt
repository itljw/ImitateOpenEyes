package com.itljw.openeyes.ui.video.dialog

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import bundleOf
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.itljw.openeyes.GlideApp
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseBottomDialogFragment
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.video.adapter.BottomCommentAdapter
import com.itljw.openeyes.util.CategoryDealDataHelper.dealData
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener
import dp2px
import getScreenWidth
import kotlinx.android.synthetic.main.dialog_bottom_comment.*
import kotlinx.android.synthetic.main.layout_video_reply_header.view.*


/**
 * Created by JW on on 2019/3/2 20:33.
 * Email : 1481013718@qq.com
 * Description :
 */
class BottomCommentDialogFragment : BaseBottomDialogFragment(), OnLoadMoreListener {

    private val videoHeight = 200
    private var videoId: Int = 0
    private var bgUrl: String = ""
    private var nextPageUrl = ""
    private val adapter by lazy { BottomCommentAdapter() }

    override fun getLayoutId(): Int =
            R.layout.dialog_bottom_comment

    companion object {

        private const val ARG_VIDEO_ID = "arg_video_id"
        private const val ARG_BG_URL = "arg_bg_url"

        fun newInstance(id: Int, bgUrl: String): BottomCommentDialogFragment {

            return BottomCommentDialogFragment()
                    .apply {
                        arguments = bundleOf(
                                ARG_VIDEO_ID to id,
                                ARG_BG_URL to bgUrl
                        )
                    }
        }
    }

    override fun doBusiness() {

        arguments?.run {
            videoId = getInt(ARG_VIDEO_ID)
            bgUrl = getString(ARG_BG_URL)
        }

        mRvBottomComment.layoutManager = LinearLayoutManager(activity)
        mRvBottomComment.adapter = adapter
        adapter.openLoadAnimation()

        mSrlBottomComment.setEnableRefresh(false) // 禁用下拉
        mSrlBottomComment.setOnLoadMoreListener(this@BottomCommentDialogFragment)

        val headerView = LayoutInflater.from(activity)
                .inflate(R.layout.layout_video_reply_header, mRvBottomComment.parent as ViewGroup, false)

        adapter.addHeaderView(headerView) // 添加头布局

        if (bgUrl.isNotEmpty()) {

            GlideApp.with(this)
                    .load(bgUrl)
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .transition(DrawableTransitionOptions().crossFade())
                    .into(mIvBottomCommentBg)
        }

        getVideoCommentData()

        // 点击事件
        headerView.ivReplyHeaderCancel.setOnClickListener {
            dismiss()
        }
    }

    /**
     * 获取视频评论数据
     */
    private fun getVideoCommentData() {

        DataManager.getVideoCommentData(videoId).observe(this, Observer { httpResult ->
            if (true == httpResult?.isSuccessful) {
                httpResult.data?.let {

                    val dealDataList = dealData(it.itemList)
                    adapter.replaceData(dealDataList)

                    nextPageUrl = it.nextPageUrl ?: ""
                }
            }
        })

    }

    // 加载更多
    override fun onLoadMore(refreshLayout: RefreshLayout) {
        if (nextPageUrl.isNotEmpty()) {
            DataManager.getHomeCategoryData(nextPageUrl).observe(this, Observer { httpResult ->
                if (true == httpResult?.isSuccessful) {
                    httpResult.data?.let {

                        val dealDataList = dealData(it.itemList)
                        adapter.addData(adapter.data.size, dealDataList)
                        mSrlBottomComment.finishLoadMore()

                        nextPageUrl = it.nextPageUrl ?: ""
                    }
                }
            })
        } else {
            mSrlBottomComment.setNoMoreData(true)
        }
    }

    override fun onStart() {
        super.onStart()

        activity?.apply {

            val rectangle = Rect()
            window.decorView.getWindowVisibleDisplayFrame(rectangle) // 获取显示区域的高度
            val dialogHeight = rectangle.height() - dp2px(videoHeight).toInt() // 减去视频的高度得到评论对话框的高度

            val window = dialog.window
            val attributes = (window.attributes as WindowManager.LayoutParams).apply {
                gravity = Gravity.BOTTOM
                dimAmount = 0f
                flags = flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                height = dialogHeight
                width = getScreenWidth()
                windowAnimations = R.style.BottomDialogAnimation
            }
            // 设置对话框为不可取消
            dialog.setCanceledOnTouchOutside(false)
            dialog.setCancelable(false)
            // 不设置BackgroundDrawable，设置的宽度填充屏幕宽度不生效
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            window.attributes = attributes
        }

    }

}