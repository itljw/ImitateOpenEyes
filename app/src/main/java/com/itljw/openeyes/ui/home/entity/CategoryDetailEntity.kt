package com.itljw.openeyes.ui.home.entity

import com.itljw.openeyes.ui.common.entity.TabInfo

/**
 * Created by JW on on 2018/9/6 23:20.
 * Email : 1481013718@qq.com
 * Description :
 */
data class CategoryDetailEntity(
    val categoryInfo: CategoryInfo,
    val tabInfo: TabInfo
) {

    data class CategoryInfo(
        val actionUrl: String, // eyepetizer://category/14/?title=%E5%B9%BF%E5%91%8A
        val dataType: String, // CategoryInfo
        val description: String, // 为广告人的精彩创意点赞
        val follow: Follow,
        val headerImage: String, // http://img.kaiyanapp.com/fc228d16638214b9803f46aabb4f75e0.png
        val id: Int, // 14
        val name: String // 广告
    ) {

        data class Follow(
            val followed: Boolean, // false
            val itemId: Int, // 14
            val itemType: String // category
        )
    }
}