package com.itljw.openeyes.ui.mine.adapter

import android.text.format.DateUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.db.Video
import com.itljw.openeyes.util.DateUtil
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_watch_history.view.*

/**
 * Created by JW on on 2019/5/28 22:06.
 * Email : 1481013718@qq.com
 * Description : 观看记录Adapter
 */
class WatchHistoryAdapter : BaseQuickAdapter<Video, BaseViewHolder>(
        R.layout.item_watch_history
) {
    override fun convert(helper: BaseViewHolder, video: Video?) {

        video?.run {
            GlideUtil.displayRoundImage(mContext, coverUrl, helper.itemView.ivWatchHistoryCover, 5, true)
            helper.itemView.tvWatchHistoryName.text = videoTitle
            helper.itemView.tvWatchHistoryTime.text = String.format("浏览时间：%s", browseTime.let {
                var pattern = "yyyy/MM/dd"
                if (DateUtils.isToday(it)) {  // 今天
                    pattern = "HH:mm"
                }
                DateUtil.millis2String(pattern, it)
            })
        }

    }
}