package com.itljw.openeyes.ui.notification.fragment

import android.graphics.Typeface
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.ui.notification.adapter.NotificationPagerAdapter
import com.itljw.openeyes.ui.search.SearchActivity
import kotlinx.android.synthetic.main.fragment_notification.*

/**
 * Created by JW on on 2018/7/14 00:25.
 * Email : 1481013718@qq.com
 * Description :
 */
class NotificationFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_notification
    }

    companion object {
        fun newInstance(): NotificationFragment =
                NotificationFragment()
    }

    override fun doBusiness() {

        val titles = arrayOf(getString(R.string.notification_official), getString(R.string.notification_interaction))

        tvNotificationTitle.typeface = Typeface.createFromAsset(resources.assets, "fonts/Lobster-1.4.otf")
        vpNotification.adapter = NotificationPagerAdapter(childFragmentManager, titles)
        mStlNotification.setViewPager(vpNotification)

        // 搜索
        ivNotificationSearch.setOnClickListener { SearchActivity.launch(mContext) }

    }

}