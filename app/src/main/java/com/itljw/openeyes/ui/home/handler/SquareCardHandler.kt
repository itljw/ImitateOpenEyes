package com.itljw.openeyes.ui.home.handler

import android.net.Uri
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.SquareCardMultiItem
import com.itljw.openeyes.ui.home.adapter.FollowCardAdapter
import com.itljw.openeyes.ui.home.adapter.HorizontalScrollCardAdapter
import com.itljw.openeyes.util.FontUtil
import com.itljw.openeyes.wiget.SpaceItemDecoration
import dp2px
import kotlinx.android.synthetic.main.item_square_card.view.*

/**
 * Created by JW on on 2018/7/25 22:44.
 * Email : 1481013718@qq.com
 * Description :
 */
object SquareCardHandler {

    fun bindData(helper: BaseViewHolder, item: SquareCardMultiItem) {

        helper.itemView.apply {

            val header = item.header

            tvSubTitle.text = header.subTitle
            tvTitle.text = header.title

            // 设置字体
            header.subTitleFont?.let {
                tvSubTitle.typeface = FontUtil.getTypefaceByType(it)
            }

            header.font?.let {
                tvTitle.typeface = FontUtil.getTypefaceByType(it)
            }

            rvSquare.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    .apply {
                        recycleChildrenOnDetach = true
                    }


            // 重复添加问题
            if (rvSquare.itemDecorationCount == 0) { // 判断是否添加了分割线
                rvSquare.addItemDecoration(SpaceItemDecoration(context.dp2px(3).toInt()))
                PagerSnapHelper().attachToRecyclerView(rvSquare)
            }

            header.actionUrl?.let {
                val parseUrl = Uri.parse(it)

                when (parseUrl.authority) {
                    ActionType.TYPE_ACTION_CAMPAIGN -> { // 专题
                        val adapter = HorizontalScrollCardAdapter(item.itemList)
                        rvSquare?.adapter = adapter
                        adapter.setOnItemClickListener { _, _, position ->
                            val itemData = item.itemList[position]

                            ActionType.handleAction(context, itemData.data.actionUrl)
                        }
                    }
                    else -> { // 发现 - 精选feed
                        rvSquare.adapter = FollowCardAdapter(item.itemList)
                    }

                }
            }

            rlTitle.setOnClickListener {
                ActionType.handleAction(context, header.actionUrl)
            }

        }

    }
}