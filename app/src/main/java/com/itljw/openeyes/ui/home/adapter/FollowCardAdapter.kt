package com.itljw.openeyes.ui.home.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.AppContext
import com.itljw.openeyes.R
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.ui.home.entity.Item
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_follow_card.view.*


/**
 * Created by JW on on 2018/7/22 12:14.
 * Email : 1481013718@qq.com
 * Description :
 */
class FollowCardAdapter(datas: List<Item>) :
        BaseQuickAdapter<Item, BaseViewHolder>(R.layout.item_follow_card, datas) {
    override fun convert(helper: BaseViewHolder, item: Item?) {

        helper.itemView.apply {
            // 设置padding
            llRoot.setPadding(0, 0, 0, 0)

            val header = item?.data?.header
            val content = item?.data?.content

            if (content != null) {
                // 设置video背景
                content.data.cover?.detail?.let {
                    GlideUtil.displayRoundImage(context, it, ivVideoCover, 5, true)
                }

                val duration = content.data.duration

                // 设置时长
                tvVideoDuration.apply {
                    background.mutate().alpha = 255 / 2 // 设置透明度
                    text = String.format("%s:%s", String.format("%02d", duration / 60), String.format("%02d", duration % 60))
                }

                // 设置头像
                header?.icon?.let {
                    GlideUtil.displayCircleImageWithBorder(context, it, ivAvatar)
                }

                // 设置title
                tvTitle.text = header?.title

                // 设置描述
                tvReplyMsg.text = header?.description

                // 事件处理
                rlInfo.setOnClickListener {
                    ActionType.handleAction(mContext, header?.actionUrl)
                }

                ivVideoCover.setOnClickListener {
                    VideoDetailActivity.launch(AppContext.mContext, content.data.id.toInt())
                }
            } else {
                // 另一个数据data
                // 设置video背景
                item?.data?.image?.let {
                    GlideUtil.displayRoundImage(AppContext.mContext, it, ivVideoCover, 5, true)
                }

                // 隐藏视频相关
                helper.setVisible(R.id.tvVideoDuration, false)
                        .setVisible(R.id.rlInfo, false)

            }
        }


    }

}