package com.itljw.openeyes.ui.video.vm

import android.arch.lifecycle.MediatorLiveData
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.BaseObserver
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.home.entity.Data
import com.itljw.openeyes.ui.home.entity.RecommendEntity

/**
 * Created by JW on on 2018/9/22 00:05.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoDetailViewModel : BaseViewModel() {

    val videoRelatedLiveData by lazy { MediatorLiveData<RecommendEntity>() }
    val videoDetailLiveData by lazy { MediatorLiveData<Data>() }

    fun getVideoRelatedData(id: Int) {
        addResponseResource(DataManager.getVideoRelatedData(id),
                object : BaseObserver<RecommendEntity>() {
                    override fun onSuccess(data: RecommendEntity?) {
                        videoRelatedLiveData.value = data
                    }
                })
    }

    fun getVideoDetailData(id: Int) {

        addResponseResource(DataManager.getVideoData(id),
                object : BaseObserver<Data>() {
                    override fun onSuccess(data: Data?) {
                        videoDetailLiveData.value = data
                    }
                })
    }

}