package com.itljw.openeyes.ui.common

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.wiget.webview.WebFragment

/**
 * Created by JW on on 2018/9/5 21:37.
 * Email : 1481013718@qq.com
 * Description : Fragment容器
 */
class FragmentContainerActivity : BaseActivity() {

    private lateinit var fragment: Fragment
    private var title: String = ""
    private var url: String = ""

    override fun getLayoutId(): Int =
            R.layout.activity_fragment_container

    companion object {

        const val EXTRA_CLAZZ = "extra_clazz"
        const val EXTRA_URL = "extra_url"
        const val EXTRA_TITLE = "extra_title"

        // class
        fun launch(mContext: Context?, clazz: Class<*>) {
            val intent = Intent(mContext, FragmentContainerActivity::class.java)
            intent.putExtra(EXTRA_CLAZZ, clazz)
            mContext?.startActivity(intent)
        }

        // web
        fun launch(mContext: Context?, title: String, url: String) {
            val intent = Intent(mContext, FragmentContainerActivity::class.java)
            intent.putExtra(EXTRA_TITLE, title)
            intent.putExtra(EXTRA_URL, url)
            mContext?.startActivity(intent)
        }
    }

    override fun doBusiness() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fl_container, fragment)
                .commitAllowingStateLoss()
    }

    override fun handleIntent(intent: Intent) {
        val clazzExtra = intent.getSerializableExtra(EXTRA_CLAZZ)
        if (clazzExtra != null) {
            if (clazzExtra is Class<*>) {
                fragment = Class.forName(clazzExtra.name).newInstance() as Fragment
            }
        } else {
            title = intent.getStringExtra(EXTRA_TITLE)
            url = intent.getStringExtra(EXTRA_URL)
            fragment = WebFragment.newInstance(title, url)
        }
    }

}