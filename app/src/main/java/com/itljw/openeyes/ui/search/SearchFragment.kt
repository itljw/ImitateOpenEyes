package com.itljw.openeyes.ui.search

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.search.adapter.SearchAdapter
import com.itljw.openeyes.ui.search.entity.ContentMultiItem
import com.itljw.openeyes.ui.search.entity.TitleMultiItem
import com.itljw.openeyes.ui.search.interf.OnSearchItemClickListener
import com.itljw.openeyes.ui.search.vm.SearchViewModel
import get
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * Created by JW on on 2018/7/28 23:03.
 * Email : 1481013718@qq.com
 * Description :
 */
class SearchFragment : BaseFragment(), BaseQuickAdapter.OnItemChildClickListener {

    private val mSearchViewModel by lazy {
        ViewModelProviders.of(this).get<SearchViewModel>()
    }
    private val mHotKeywordList by lazy { mutableListOf<MultiItemEntity>() }
    private val mSearchAdapter by lazy { SearchAdapter(mHotKeywordList) }
    private lateinit var historyKeywordList: MutableList<String>

    override fun getLayoutId(): Int = R.layout.fragment_search

    companion object {
        fun newInstance(): SearchFragment =
             SearchFragment()
    }

    override fun doBusiness() {
        mMultipleStatusLayout = msvSearch

        initRecyclerView()
        handleSearchHistory()

        mSearchViewModel.getSearchHotKeyData()
    }

    /**
     * 处理搜索历史
     */
    private fun handleSearchHistory() {
        historyKeywordList = DataManager.getSearchHistoryKeyword()
        mHotKeywordList.clear()

        if (historyKeywordList.isNotEmpty() && historyKeywordList.size > 0) {
            // 历史数据
            mHotKeywordList.add(TitleMultiItem(TitleMultiItem.TYPE_SEARCH_HISTORY))
            mHotKeywordList.addAll(historyKeywordList.map { ContentMultiItem(it) })
        }

    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {
        rvSearch.adapter = mSearchAdapter
        mSearchAdapter.openLoadAnimation()

        // 事件
        mSearchAdapter.setOnItemClickListener { _, _, position ->
            val itemEntity = mHotKeywordList[position]

            if (itemEntity is ContentMultiItem) {
                DataManager.putSearchHistoryKeyword(itemEntity.content)
                onSearchItemClickListener?.onSearchItemClick(itemEntity.content)
            }
        }
        mSearchAdapter.onItemChildClickListener = this
        // 滑动监听
        rvSearch.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                hideSoftInput()
            }
        })
    }

    override fun handleObserver() {

        mSearchViewModel.searchHotKeyLiveData.observe(this, Observer { data ->

            mMultipleStatusLayout?.showContent()
            data?.let {
                // 添加搜索热词
                mHotKeywordList.add(TitleMultiItem(TitleMultiItem.TYPE_SEARCH_RESULT))
                for (item in it.asJsonArray.iterator()) {
                    mHotKeywordList.add(ContentMultiItem(item.asString))
                }
                mSearchAdapter.notifyDataSetChanged()
            }
        })

        mSearchViewModel.hotKeyLoadState.observe(this, Observer { state ->
            when (state) {
                LoadDataState.LOADING -> {
                    mMultipleStatusLayout?.showLoading()
                }

                LoadDataState.ERROR -> {
                    mMultipleStatusLayout?.showError()
                }
                else -> {
                }
            }
        })
    }

    /**
     * 关键词存入sp并刷新列表
     */
    fun putSearchHistoryKeyword(currentKeyword: String) {

        DataManager.putSearchHistoryKeyword(currentKeyword)
        // update
        if (!historyKeywordList.contains(currentKeyword)) {
            historyKeywordList.add(0, currentKeyword)

            // 判断是否有搜索历史的Title
            var isExistSearchHistoryItem = false
            for (item in mHotKeywordList) {

                if (item is TitleMultiItem
                        && TitleMultiItem.TYPE_SEARCH_HISTORY == item.type) {
                    isExistSearchHistoryItem = true
                    break
                }
            }

            if (isExistSearchHistoryItem) {
                mHotKeywordList.add(1, ContentMultiItem(currentKeyword))
            } else {
                // 添加Title ItemType
                mHotKeywordList.add(0, TitleMultiItem(TitleMultiItem.TYPE_SEARCH_HISTORY))
                mHotKeywordList.add(1, ContentMultiItem(currentKeyword))
            }

            mSearchAdapter.notifyDataSetChanged()
        }

    }

    override fun onItemChildClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
        when (view?.id) {
            R.id.tv_item_search_delete -> {

                mContext?.let {
                    AlertDialog.Builder(it)
                            .setMessage(getString(R.string.search_delete_history_tip))
                            .setPositiveButton(getString(R.string.common_confirm)) { _, _ ->
                                // 删除历史记录
                                DataManager.clearSearchHistoryKeyword()

                                // 移除数据 +1是Title的数量
                                mHotKeywordList.removeAll(mHotKeywordList.subList(0, historyKeywordList.size + 1))
                                historyKeywordList.clear() // 清空

                                mSearchAdapter.notifyDataSetChanged()
                            }
                            .setNegativeButton(getString(R.string.common_cancel), null)
                            .show()
                }

            }
        }
    }

    var onSearchItemClickListener: OnSearchItemClickListener? = null


}