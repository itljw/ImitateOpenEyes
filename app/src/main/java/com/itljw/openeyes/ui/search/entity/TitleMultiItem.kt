package com.itljw.openeyes.ui.search.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.ui.search.adapter.SearchAdapter

/**
 * Created by JW on on 2018/8/5 19:52.
 * Email : 1481013718@qq.com
 * Description :
 */
class TitleMultiItem(val type:Int) : MultiItemEntity {

    companion object {
        const val TYPE_SEARCH_RESULT = 0x11
        const val TYPE_SEARCH_HISTORY = 0x22

    }

    override fun getItemType(): Int = SearchAdapter.TYPE_ITEM_TITLE
}