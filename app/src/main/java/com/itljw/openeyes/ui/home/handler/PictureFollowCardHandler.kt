package com.itljw.openeyes.ui.home.handler

import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.entity.PictureFollowCardMultiItem
import com.itljw.openeyes.util.DateUtil
import com.itljw.openeyes.util.GlideUtil
import inflater
import kotlinx.android.synthetic.main.item_picture_follow_card.view.*

/**
 * Created by JW on on 2018/7/26 00:00.
 * Email : 1481013718@qq.com
 * Description :
 */
object PictureFollowCardHandler {

    fun bindData(helper: BaseViewHolder, item: PictureFollowCardMultiItem) {

        helper.itemView.apply {
            val data = item.data.content.data
            val owner = data.owner
            val mContext = helper.itemView.context

            // 收藏数和回复数
            tvCollectCount.text = data.consumption.collectionCount.toString()
            tvReplyCount.text = data.consumption.replyCount.toString()
            tvPublishTime.text = DateUtil.formatIsTodayDate(data.releaseTime)

            GlideUtil.displayCircleImageWithBorder(mContext, owner.avatar, ivAvatar)

            tvName.text = owner.nickname
            tvReplyMsg.text = data.description

            data.tags?.let {
                // 设置tag
                llTags.removeAllViews()

                var tagList = it

                if (it.size > 3) {
                    tagList = it.subList(0, 3)
                }

                for (tagItem in tagList) {
                    llTags.addView(
                            (mContext.inflater.inflate(R.layout.item_works_tag, llTags, false) as TextView)
                                    .apply {
                                        text = tagItem.name
                                    }
                    )
                }
            }

            GlideUtil.displayRoundImage(mContext, data.url, ivVideoCover, 5, true)

            helper.addOnClickListener(R.id.ivVideoCover)
            helper.addOnClickListener(R.id.ll_picture_follow_card)
        }


    }

}