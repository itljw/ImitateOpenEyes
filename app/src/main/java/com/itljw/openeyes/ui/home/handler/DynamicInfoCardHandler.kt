package com.itljw.openeyes.ui.home.handler

import android.text.format.DateUtils
import android.view.View
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.DynamicInfoCardMultiItem
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.util.DateUtil
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_dynamic_info_card.view.*

/**
 * Created by JW on on 2018/7/26 00:08.
 * Email : 1481013718@qq.com
 * Description :
 */
object DynamicInfoCardHandler {
    fun bindData(helper: BaseViewHolder, item: DynamicInfoCardMultiItem) {

        helper.itemView.apply {

            val data = item.data

            if ("DynamicFollowCard" == data.dataType) { // brief 作者详情动态
                // 隐藏视频
                rlVideo.visibility = View.GONE
                rlBrief.visibility = View.VISIBLE

                data.briefCard?.apply {
                    // 设置标题、描述
                    ivBriefTitle.text = title
                    tvBriefDesc.text = description

                    GlideUtil.displayRoundImage(context, icon, ivBriefIcon, 5, true)
                }

            } else {
                rlVideo.visibility = View.VISIBLE
                rlBrief.visibility = View.GONE

                val simpleVideo = data.simpleVideo // video信息

                // 设置视频封面图
                GlideUtil.displayRoundImage(context, simpleVideo?.cover?.detail
                        ?: "", ivVideoCover, 5, true)

                // 设置播放时长
                val duration = simpleVideo?.duration
                if (duration != null) {
                    with(tvVideoDuration) {
                        background.mutate().alpha = 255 / 2 // 设置时长透明度
                        text = String.format("%s:%s", String.format("%02d", duration / 60),
                                String.format("%02d", duration % 60)) // 格式化时间
                    }
                }

                // 设置分类
                tvVideoCategory.text = String.format("#%s", simpleVideo?.category ?: "")

                // 是否显示分类
                helper.setVisible(R.id.tvVideoCategory, simpleVideo?.category != null)
            }

            GlideUtil.displayCircleImage(context, data.user.avatar, ivAuthorAvatar)

            tvAuthorName.text = data.user.nickname // 设置昵称
            tvCommentTitle.text = data.text // 评论
            tvVideoTitle.text = data.simpleVideo?.title // 设置video title

            val reply = data.reply // 回复信息

            if (reply != null) {

                helper.setVisible(R.id.tvHotComment, reply.ifHotReply) // 是否热评

                // 设置是否点赞
                ivLike.setImageResource(
                        if (reply.liked)
                            R.drawable.ic_action_like_add_light
                        else
                            R.drawable.ic_action_like_add_grey
                )

                tvReplyMsg.text = data.reply.message // 设置回复消息
                tvLikeCount.text = data.reply.likeCount.toString() // 设置收藏数

                // 设置时间
                tvReplyTime.text = data.createDate.let {
                    var pattern = "yyyy/MM/dd"
                    if (DateUtils.isToday(it)) {  // 今天
                        pattern = "HH:mm"
                    }
                    DateUtil.millis2String(pattern, it)
                }
            } else {
                // 要哭,setVisible设置为false，原来是invisible
                helper.setVisible(R.id.tvHotComment, false) // 是否热评

                // 设置创建日期
                tvCreateDate.visibility = View.VISIBLE
                tvCreateDate.text = DateUtil.millis2String("yyyy/MM/dd", data.createDate)

                tvReplyMsg.visibility = View.GONE
                rlBottom.visibility = View.GONE
            }

//        helper.setVisible(R.id.rl_dynamic_info_card_bottom, reply != null)
//                .setVisible(R.id.tv_dynamic_info_card_desc, reply != null)
//                .setVisible(R.id.tv_dynamic_info_card_hot_comment, reply != null && reply.ifHotReply) // 是否热评

            // 事件处理
            helper.itemView.setOnClickListener {
                ActionType.handleAction(context, data.actionUrl)
            }

            ivAuthorAvatar.setOnClickListener {
                ActionType.handleAction(context, data.user.actionUrl)
            }

            rlBrief.setOnClickListener {
                data.briefCard?.let { briefCard ->
                    ActionType.handleAction(context, briefCard.actionUrl)
                }
            }

            rlVideo.setOnClickListener {
                data.simpleVideo?.let { simpleVideo ->
                    VideoDetailActivity.launch(context, simpleVideo.id)
                }
            }

        }
    }

}