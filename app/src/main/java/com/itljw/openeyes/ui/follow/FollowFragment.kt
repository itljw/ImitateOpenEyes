package com.itljw.openeyes.ui.follow


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Typeface
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.ui.common.entity.TabInfo
import com.itljw.openeyes.ui.follow.vm.FollowViewModel
import com.itljw.openeyes.ui.home.adapter.CategoryDetailPagerAdapter
import com.itljw.openeyes.ui.search.SearchActivity
import kotlinx.android.synthetic.main.fragment_follow.*

/**
 * Created by JW on on 2018/7/14 00:26.
 * Email : 1481013718@qq.com
 * Description :
 */
class FollowFragment : BaseFragment() {

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get(FollowViewModel::class.java)
    }

    private val mTabDataList: MutableList<TabInfo.Tab>  by lazy {
        mutableListOf<TabInfo.Tab>()
    }

    private val adapter by lazy {
        CategoryDetailPagerAdapter(childFragmentManager, mTabDataList)
    }

    override fun getLayoutId(): Int =
            R.layout.fragment_follow

    companion object {
        fun newInstance(): FollowFragment =
                FollowFragment()
    }

    override fun doBusiness() {

        mTvFollowTitle.typeface = Typeface.createFromAsset(activity?.assets, "fonts/Lobster-1.4.otf")

        mVpFollow.adapter = adapter

        mViewModel.getFollowList()

        mIvFollowSearch.setOnClickListener { SearchActivity.launch(mContext) }
    }

    override fun handleObserver() {
        mViewModel.mFollowListLiveData.observe(this, Observer { data ->

            val tabInfo = data?.tabInfo
            val defaultIdx = tabInfo?.defaultIdx ?: 0
            tabInfo?.tabList?.let {

                adapter.replaceData(it)
                mVpFollow.offscreenPageLimit = it.size - 1 // 设置缓存数量
                mStlFollow.setViewPager(mVpFollow)
                mVpFollow.setCurrentItem(defaultIdx, false) // 设置默认选中位置
            }
        })
    }

}