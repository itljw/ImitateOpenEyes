package com.itljw.openeyes.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.http.LoadDataState
import com.itljw.openeyes.ui.home.vm.CommentViewModel
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.ui.video.adapter.BottomCommentAdapter
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener
import isNumeric
import kotlinx.android.synthetic.main.activity_comment.*
import toast

/**
 * Created by JW on on 2019/5/21 23:26.
 * Email : 1481013718@qq.com
 * Description : 评论
 */
class CommentActivity : BaseActivity(), OnLoadMoreListener {

    private var top = ""
    private var type = ""
    private var videoId = ""
    private var videoTitle = ""

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get(CommentViewModel::class.java)
    }

    private val mAdapter = BottomCommentAdapter()

    override fun getLayoutId(): Int =
            R.layout.activity_comment

    companion object {

        private const val EXTRA_TOP = "extra_top"
        private const val EXTRA_TYPE = "extra_type"
        private const val EXTRA_VIDEO_ID = "extra_video_id"
        private const val EXTRA_VIDEO_TITLE = "extra_video_title"

        fun launch(mContext: Context?, videoId: String, top: String, type: String, videoTitle: String) {
            mContext?.startActivity(
                    Intent(mContext, CommentActivity::class.java)
                            .putExtra(EXTRA_VIDEO_ID, videoId)
                            .putExtra(EXTRA_TOP, top)
                            .putExtra(EXTRA_TYPE, type)
                            .putExtra(EXTRA_VIDEO_TITLE, videoTitle)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_keep_still)
    }

    override fun handleIntent(intent: Intent) {
        super.handleIntent(intent)
        videoId = intent.getStringExtra(EXTRA_VIDEO_ID)
        top = intent.getStringExtra(EXTRA_TOP)
        type = intent.getStringExtra(EXTRA_TYPE)
        videoTitle = intent.getStringExtra(EXTRA_VIDEO_TITLE)
    }

    override fun doBusiness() {
        if (videoTitle.isEmpty()) {
            tvCommentTitle.visibility = View.GONE
        }
        tvCommentTitle.text = videoTitle

        initRecyclerView()

        mViewModel.getCommentRepliesData(if (videoId.isNumeric()) videoId.toInt() else 0, type, top)

        tvCommentWorksDetail.setOnClickListener {
            // 进入作品详情页
            VideoDetailActivity.launch(mContext, if (videoId.isNumeric()) videoId.toInt() else 0)
        }

        tvCommentPublish.setOnClickListener {
            toast("发表新评论")
        }

        ivCommentDown.setOnClickListener { finish() }
    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {
        rvComment.layoutManager = LinearLayoutManager(this)
        rvComment.adapter = mAdapter
        mAdapter.openLoadAnimation()

        srlComment.setEnableRefresh(false) // 禁用下拉
        srlComment.setOnLoadMoreListener(this@CommentActivity)
    }

    override fun handleObserver() {
        super.handleObserver()

        mViewModel.mCommentLoadState.observe(this, Observer { state ->
            when (state) {
                LoadDataState.LOAD_MORE -> {
                    srlComment.setNoMoreData(false)
                }
                LoadDataState.NO_MORE_DATA -> {
                    srlComment.setNoMoreData(true)
                }
                else -> {
                }
            }
        })

        mViewModel.mLoadMoreLiveData.observe(this, Observer { dataList ->
            dataList?.let {
                mAdapter.addData(mAdapter.data.size, it)
                srlComment.finishLoadMore()
            }
        })

        mViewModel.mRefreshDataLiveData.observe(this, Observer { dataList ->

            dataList?.let {
                mAdapter.replaceData(it)
            }
        })
    }

    override fun onLoadMore(refreshLayout: RefreshLayout) {
        mViewModel.getNextCommentData()
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_keep_still, R.anim.slide_out_bottom)
    }

}