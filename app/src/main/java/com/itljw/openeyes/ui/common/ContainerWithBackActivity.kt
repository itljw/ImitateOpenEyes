package com.itljw.openeyes.ui.common

import android.content.Context
import android.content.Intent
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.ui.home.CategoryFragment
import kotlinx.android.synthetic.main.layout_title_bar.*

/**
 * Created by JW on on 2019/4/18 00:10.
 * Email : 1481013718@qq.com
 * Description : 带back按钮的Fragment容器
 */
class ContainerWithBackActivity : BaseActivity() {

    private var title = ""
    private var url: String = ""

    override fun getLayoutId(): Int =
            R.layout.activity_container_with_back

    companion object {

        const val EXTRA_URL = "extra_url"
        const val EXTRA_TITLE = "extra_title"

        fun launch(mContext: Context?, title: String, url: String) {
            val intent = Intent(mContext, ContainerWithBackActivity::class.java)
            intent.putExtra(EXTRA_TITLE, title)
            intent.putExtra(EXTRA_URL, url)
            mContext?.startActivity(intent)
        }

    }

    override fun doBusiness() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fl_container, CategoryFragment.newInstance(url))
                .commitAllowingStateLoss()

        tvTitle.text = title
        ivBack.setOnClickListener { finish() }
    }

    override fun handleIntent(intent: Intent) {
        title = intent.getStringExtra(EXTRA_TITLE)
        url = intent.getStringExtra(EXTRA_URL)
    }

}