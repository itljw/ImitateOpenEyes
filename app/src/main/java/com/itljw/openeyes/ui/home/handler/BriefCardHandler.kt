package com.itljw.openeyes.ui.home.handler

import android.view.View
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.BriefCardMultiItem
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_brief_card.view.*

/**
 * Created by JW on on 2018/7/26 00:06.
 * Email : 1481013718@qq.com
 * Description :
 */
object BriefCardHandler {
    fun bindData(helper: BaseViewHolder, item: BriefCardMultiItem) {

        helper.itemView.apply {
            val data = item.data

            GlideUtil.displayRoundImage(context, data.icon, ivBriefCardIcon, 5, true)

            // 标题、描述
            tvBriefCardTitle.text = data.title
            tvBriefCardDesc.text = data.description

            // 是否显示关注
            tvBriefCardFollow.visibility = if (data.follow?.followed != false) View.GONE else View.VISIBLE

            helper.itemView.setOnClickListener {
                ActionType.handleAction(context, data.actionUrl)
            }
        }


    }
}