package com.itljw.openeyes.ui.common.entity

/**
 * Created by JW on on 2019/5/20 23:10.
 * Email : 1481013718@qq.com
 * Description :
 */
class TabPagerEntity(
        val tabInfo: TabInfo
)

data class TabInfo(
        val defaultIdx: Int, // 0
        val tabList: List<Tab>
) {

    data class Tab(
            val apiUrl: String, // http://baobab.kaiyanapp.com/api/v4/categories/detail/playlist?id=14
            val id: Int, // 3
            val name: String, // 专辑
            val nameType: Int, // 0
            val tabType: Int // 0
    )
}