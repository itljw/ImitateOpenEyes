package com.itljw.openeyes.ui.main

import android.content.Context
import android.graphics.Rect
import com.itljw.openeyes.AppContext
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.event.CloseFloatVideoEvent
import com.itljw.openeyes.event.FloatVideoEvent
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.wiget.FloatVideoView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import startActivity
import toast


class MainActivity : BaseActivity() {

    private var lastTime: Long = 0L
    private var videoView: FloatVideoView? = null
    private var visibleHeight = 0
    private var visibleWidth = 0

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    companion object {

        // 间隔时间
        private const val INTERVAL_TIME = 2000

        fun launchActivity(mContext: Context) {
            mContext.startActivity<MainActivity>()
        }
    }

    override fun doBusiness() {
        EventBus.getDefault().register(this)

        val visibleDisplayRect = Rect()
        window.decorView.getWindowVisibleDisplayFrame(visibleDisplayRect)

        visibleHeight = Math.abs(visibleDisplayRect.bottom - visibleDisplayRect.top) // 可视高度
        visibleWidth = Math.abs(visibleDisplayRect.right) // 可视宽度

        supportFragmentManager.beginTransaction()
                .replace(R.id.fl_container, MainFragment.newInstance()).commit()
    }

    @Suppress("unused")
    @Subscribe
    fun onStartFloatVideo(videoEvent: FloatVideoEvent) {
        // 悬浮播放
        videoView = FloatVideoView(mContext).apply {
            startPlayVideo(videoEvent.playUrl, videoEvent.currentPosition)
            initWindowParams(visibleHeight, visibleWidth, videoEvent.videoWidth, videoEvent.videoHeight)
            show()
            setOnClickListener {
                VideoDetailActivity.launchWithPosition(this@MainActivity, videoEvent.videoId, getCurrentPosition())
            }
        }
    }

    @Suppress("unused")
    @Subscribe
    fun onCloseFloatVideo(@Suppress("UNUSED_PARAMETER") event: CloseFloatVideoEvent) {
        videoView?.remove() // 移除悬浮控件
    }

    override fun onBackPressed() {

        if (System.currentTimeMillis() - lastTime < INTERVAL_TIME) {
            AppContext.exit()
        } else {
            toast(getString(com.itljw.openeyes.R.string.main_double_click_exit))
            lastTime = System.currentTimeMillis()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }


}
