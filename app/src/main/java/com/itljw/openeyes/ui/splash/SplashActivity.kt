package com.itljw.openeyes.ui.splash

import android.content.pm.PackageManager
import android.graphics.Typeface
import android.support.v4.app.ActivityCompat
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.ScaleAnimation
import animationListener
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.ui.main.MainActivity
import com.itljw.openeyes.util.AppUtils
import kotlinx.android.synthetic.main.activity_splash.*

/**
 * Created by JW on on 2018/7/9 00:39.
 * Email : 1481013718@qq.com
 * Description :
 */
class SplashActivity : BaseActivity() {

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    companion object {
        private const val ANIMATION_DURATION = 3000L
        private const val REQUEST_CODE_PERMISSION = 0x11
    }

    override fun doBusiness() {
        val appNameTypeface: Typeface = Typeface.createFromAsset(this.assets, "fonts/Lobster-1.4.otf")
        tvSplashAppName.typeface = appNameTypeface
        tvSplashAppName.text = resources.getString(R.string.app_name)

        tvSplashAuthor.text = resources.getString(R.string.app_author)
        tvSplashAuthor.typeface = appNameTypeface
        tvSplashVersionName.text = String.format("v%s", AppUtils.getVersionName(this))
        tvSplashVersionName.typeface = appNameTypeface

        val alphaAnimation = AlphaAnimation(0.5f, 1.0f)
        val scaleAnimation = ScaleAnimation(1.0f, 1.1f, 1.0f, 1.1f,
                ScaleAnimation.RELATIVE_TO_SELF.toFloat(), ScaleAnimation.RELATIVE_TO_SELF.toFloat())

       // 
        ivSplashBg.startAnimation(
                // 参数为是否共用一个差值器
                AnimationSet(true).apply {
                    addAnimation(scaleAnimation)
                    addAnimation(alphaAnimation)
                    fillAfter = true
                    duration = ANIMATION_DURATION
                }
        )

        alphaAnimation.animationListener(animationEnd = {
            ActivityCompat.requestPermissions(this@SplashActivity, arrayOf(android
                    .Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE_PERMISSION)
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (REQUEST_CODE_PERMISSION == requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                MainActivity.launchActivity(this@SplashActivity)
                finish()
            }
        }
    }

}