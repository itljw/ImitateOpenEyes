package com.itljw.openeyes.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.constants.Const
import com.itljw.openeyes.event.NotifyEvent
import com.itljw.openeyes.ui.common.FragmentContainerActivity
import com.itljw.openeyes.ui.common.entity.TabInfo
import com.itljw.openeyes.ui.common.vm.TabPagerViewModel
import com.itljw.openeyes.ui.home.adapter.CategoryDetailPagerAdapter
import com.itljw.openeyes.ui.search.SearchActivity
import get
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by JW on on 2018/7/14 00:24.
 * Email : 1481013718@qq.com
 * Description : 首页
 */
class HomeFragment : BaseFragment() {

    private var defaultTabIndex = 1 // 默认tab位置

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get<TabPagerViewModel>()
    }

    private val mTabDataList: MutableList<TabInfo.Tab>  by lazy {
        mutableListOf<TabInfo.Tab>()
    }

    private val adapter by lazy {
        CategoryDetailPagerAdapter(childFragmentManager, mTabDataList)
    }

    override fun getLayoutId(): Int =
            R.layout.fragment_home

    companion object {
        fun newInstance(): HomeFragment =
                HomeFragment()
    }

    override fun doBusiness() {

        EventBus.getDefault().register(this)
        vpHome.adapter = adapter

        mViewModel.getPagerDataByUrl(Const.URL_HOME_TAB_LIST)

        ivHomeSearch.setOnClickListener {

            SearchActivity.launch(mContext)

            /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                 val optionsCompat = ActivityOptionsCompat
                         .makeSceneTransitionAnimation(activity!!, it, getString(R.string.transition_search_name))
                 startActivity(Intent(mContext, SearchActivity::class.java), optionsCompat.toBundle())
             } else {
                 startActivity(Intent(mContext, SearchActivity::class.java))
             }*/

        }

        ivCategoryList.setOnClickListener {
            //            EventBus.getDefault().post(StartFragmentEvent(CategoryListFragment.newInstance()))
            FragmentContainerActivity.launch(mContext, CategoryListFragment::class.java)
        }

    }

    override fun handleObserver() {
        super.handleObserver()
        mViewModel.tabPagerLiveData.observe(this, Observer { data ->

            val tabInfo = data?.tabInfo
            val defaultIdx = tabInfo?.defaultIdx ?: 0
            tabInfo?.tabList?.let {
                adapter.replaceData(it)

                tlHome.setViewPager(vpHome)
                vpHome.setCurrentItem(defaultIdx, false) // 设置在后才生效
            }

        })
    }

    @Suppress("unused")
    @Subscribe
    fun onReceiveEvent(notifyEvent: NotifyEvent<Int>) {
        // 切换tab
        defaultTabIndex = notifyEvent.arg
        vpHome.setCurrentItem(defaultTabIndex, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

}