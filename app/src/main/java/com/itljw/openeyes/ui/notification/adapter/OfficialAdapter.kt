package com.itljw.openeyes.ui.notification.adapter

import android.databinding.DataBindingUtil
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.databinding.ItemOfficialBinding
import com.itljw.openeyes.ui.notification.entity.OfficialNotificationEntity
import java.util.*

/**
 * Created by JW on on 2018/7/20 22:54.
 * Email : 1481013718@qq.com
 * Description : 官方通知
 */
class OfficialAdapter :
        BaseQuickAdapter<OfficialNotificationEntity.Message, BaseViewHolder>(
                R.layout.item_official
        ) {

    override fun convert(helper: BaseViewHolder?, item: OfficialNotificationEntity.Message?) {

        helper?.run {
            val mBinding = DataBindingUtil.bind<ItemOfficialBinding>(itemView)
            mBinding?.item = item

            // 设置时间
            item?.date?.let { date ->

                val days = (Date().time - date) / (1000 * 60 * 60 * 24)

                var result = "很久以前"
                when {
                    days < 7 -> result = "${days}天前"
                    days >= 7 -> result = "一周前"
                    days >= 30 -> result = "一个月前"
                }
                mBinding?.tvOfficialTime?.text = result
            }

            itemView?.setOnClickListener {
                ActionType.handleAction(mContext, item?.actionUrl)
            }
        }

    }
}