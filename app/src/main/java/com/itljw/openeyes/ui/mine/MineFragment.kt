package com.itljw.openeyes.ui.mine

import android.content.Intent
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.http.DataManager
import kotlinx.android.synthetic.main.fragment_mine.*

/**
 * Created by JW on on 2018/7/14 00:26.
 * Email : 1481013718@qq.com
 * Description : 我的
 */
class MineFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_mine
    }

    companion object {
        fun newInstance(): MineFragment {
            return MineFragment()
        }
    }

    override fun doBusiness() {
        tvMineErrorInfo.setOnClickListener {
            startActivity(
                    Intent(mContext, ErrorInfoActivity::class.java)
                            .putExtra(ErrorInfoActivity.EXTRA_INFO, DataManager.getErrorInfo())
            )
        }

        rlMineOverlayWindow.setOnClickListener {
            switchMine.performClick()
        }

        switchMine.setOnCheckedChangeListener { _, isChecked ->
            DataManager.saveOverlayWindowStatus(isChecked)
        }

        tvMineWatchHistory.setOnClickListener {
            WatchHistoryActivity.launch(mContext)
        }

    }

    override fun onResume() {
        super.onResume()
        switchMine.isChecked = DataManager.getOverlayWindowStatus()
    }
}