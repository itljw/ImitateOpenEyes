package com.itljw.openeyes.ui.video.adapter

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.R
import com.itljw.openeyes.entity.ReplyMultiItem
import com.itljw.openeyes.entity.TextCardMultiItem
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.ui.home.handler.ReplyHandler
import com.itljw.openeyes.ui.home.handler.TextCardHandler

/**
 * Created by JW on on 2019/3/3 01:20.
 * Email : 1481013718@qq.com
 * Description :
 */
class BottomCommentAdapter :
        BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder>(arrayListOf()) {

    init {
        addItemType(HomeConstant.ITEM_TYPE_TEXT_CARD, R.layout.item_text_card)
        addItemType(HomeConstant.ITEM_TYPE_REPLY, R.layout.item_reply)
    }

    override fun convert(helper: BaseViewHolder?, item: MultiItemEntity?) {
        when (helper?.itemViewType) {
            HomeConstant.ITEM_TYPE_TEXT_CARD -> { // 文本
                if (item is TextCardMultiItem) {
                    TextCardHandler.bindData(helper, item, R.color.color_white)
                }
            }

            HomeConstant.ITEM_TYPE_REPLY -> { // 评论回复
                if (item is ReplyMultiItem) {
                    ReplyHandler.bindData(helper, item)
                }
            }
        }
    }
}