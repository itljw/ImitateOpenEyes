package com.itljw.openeyes.ui.home.handler

import android.support.v4.content.ContextCompat
import android.view.View
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.TextCardMultiItem
import dp2px
import kotlinx.android.synthetic.main.item_text_card.view.*

/**
 * Created by JW on on 2018/7/24 23:57.
 * Email : 1481013718@qq.com
 * Description : TextCard 数据处理类
 */
object TextCardHandler {

    fun bindData(helper: BaseViewHolder, item: TextCardMultiItem, textColor: Int = R.color.color_black) {

        helper.itemView.apply {

            val type = item.data.type
            var isFooter = false

            if (type.contains("footer")) { // Footer布局

                isFooter = true
                tvFooter.text = item.data.text
                rlRoot.setPadding(context.dp2px(15).toInt(), context.dp2px(15).toInt(), 0, 0)
            } else { // header

                // 判断是否关注 未关注则显示关注Text
                tvFollow.visibility = if (item.data.follow != null && !item.data.follow.followed) View.VISIBLE else View.GONE

                // 通过是否有actionUrl，判断是否显示arrow
                ivArrow.visibility = if (!item.data.actionUrl.isNullOrEmpty()) View.VISIBLE else View.GONE

                // 设置title
                // header也有不同的样式区别
                if (type.contains("header4")) {
                    rlRoot.setPadding(context.dp2px(15).toInt(), context.dp2px(15).toInt(), 0, 0)

                    tvTitle.textSize = 16f
                    // 视频热门评论color为white，全部作者color为black
                    tvTitle.setTextColor(ContextCompat.getColor(context, textColor))
                }
                tvTitle.text = item.data.text

            }
            // 是否显示header/footer部分
            helper.setVisible(R.id.rlHeader, !isFooter).setVisible(R.id.tvFooter, isFooter)

            // 事件处理
            rlRoot.setOnClickListener {
                ActionType.handleAction(context, item.data.actionUrl)
            }
        }
    }
}
