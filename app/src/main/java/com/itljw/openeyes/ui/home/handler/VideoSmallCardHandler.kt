package com.itljw.openeyes.ui.home.handler

import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.entity.VideoSmallCardMultiItem
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.item_video_small_card.view.*

/**
 * Created by JW on on 2018/7/26 00:04.
 * Email : 1481013718@qq.com
 * Description :
 */
object VideoSmallCardHandler {
    fun bindData(helper: BaseViewHolder, item: VideoSmallCardMultiItem) {

        helper.itemView.apply {

            val data = item.data

            // 设置video背景图
            if (data.cover != null) {
                GlideUtil.displayRoundImage(context, data.cover.detail, ivVideoCover, 5, true)
            }
            // 设置video时长
            val duration = data.duration

            // 设置时长
            tvVideoDuration.apply {
                // 设置透明度
                background.mutate().alpha = 255 / 2
                text = String.format("%s:%s", String.format("%02d", duration / 60), String.format("%02d", duration % 60))
            }

            // 设置类别
            tvVideoCategory.text = String.format("#%s", data.category)
            tvVideoTitle.text = data.description

            // 事件处理
            rlVideoInfo.setOnClickListener {
                VideoDetailActivity.launch(context, item.data.id.toInt()) // 视频详情
            }

        }

    }


}