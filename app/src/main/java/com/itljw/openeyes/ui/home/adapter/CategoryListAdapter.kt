package com.itljw.openeyes.ui.home.adapter

import com.chad.library.adapter.base.BaseItemDraggableAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.ui.home.entity.Item
import com.itljw.openeyes.util.GlideUtil

/**
 * Created by JW on on 2018/9/4 21:27.
 * Email : 1481013718@qq.com
 * Description :
 */
class CategoryListAdapter : BaseItemDraggableAdapter<Item, BaseViewHolder>(
        R.layout.item_category_list, arrayListOf()) {

    override fun convert(helper: BaseViewHolder, item: Item?) {
        helper.setText(R.id.tv_item_category_list_title, item?.data?.title)
                ?.setText(R.id.tv_item_category_list_desc, item?.data?.description)

        item?.data?.icon?.let {
            GlideUtil.displayRoundImage(mContext, it, helper.getView(R.id.iv_item_category_list_icon), 5, true)
        }

    }
}