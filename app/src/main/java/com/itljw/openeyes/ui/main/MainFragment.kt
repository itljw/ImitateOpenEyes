package com.itljw.openeyes.ui.main

import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import com.itljw.openeyes.R
import com.itljw.openeyes.entity.TabEntity
import com.itljw.openeyes.base.BaseFragment
import com.itljw.openeyes.ui.main.adapter.MainPagerAdapter
import kotlinx.android.synthetic.main.fragment_main.*
import pagerChange

/**
 * Created by JW on on 2018/7/12 23:44.
 * Email : 1481013718@qq.com
 * Description :
 */
class MainFragment : BaseFragment() {

    // 未被选中的图标
    private val iconUnSelectIds = intArrayOf(R.drawable.ic_tab_strip_icon_feed, R.drawable.ic_tab_strip_icon_follow,
            R.drawable.ic_tab_strip_icon_category, R.drawable.ic_tab_strip_icon_profile)
    // 被选中的图标
    private val iconSelectIds = intArrayOf(R.drawable.ic_tab_strip_icon_feed_selected, R.drawable.ic_tab_strip_icon_follow_selected,
            R.drawable.ic_tab_strip_icon_category_selected, R.drawable.ic_tab_strip_icon_profile_selected)

    private var currentIndex = 0

    private val tabEntities by lazy {
        ArrayList<CustomTabEntity>()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_main
    }

    companion object {
        fun newInstance(): MainFragment =
                MainFragment()
    }

    override fun doBusiness() {

        val titles = arrayOf(
                resources.getString(R.string.tab_home),
                resources.getString(R.string.tab_follow),
                resources.getString(R.string.tab_notification),
                resources.getString(R.string.tab_mine)
        )

        for (i in titles.indices) {
            tabEntities.add(TabEntity(titles[i], iconSelectIds[i], iconUnSelectIds[i]))
        }
        // 设置tab数据
        tlMain.setTabData(tabEntities)

        // 设置ViewPager
        vpMain.adapter = MainPagerAdapter(fragmentManager, titles)
        vpMain.offscreenPageLimit = titles.size // 设置缓存数量
        vpMain.setCurrentItem(currentIndex, false)

        // 设置Tab选中监听
        tlMain.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                vpMain.setCurrentItem(position, false)
                currentIndex = position
            }

            override fun onTabReselect(position: Int) {}
        })

        // 设置ViewPager的选中
        vpMain.pagerChange(pageSelected = { position ->
            tlMain.currentTab = position
        })

    }

}