package com.itljw.openeyes.ui.video.entity

import com.itljw.openeyes.ui.common.entity.TabInfo

/**
 * Created by JW on on 2019/3/1 21:55.
 * Email : 1481013718@qq.com
 * Description :
 */
data class VideoTagsTabEntity(
        val tabInfo: TabInfo,
        val tagInfo: TagInfo
) {
    data class TagInfo(
            val actionUrl: String, // null
            val bgPicture: String, // http://img.kaiyanapp.com/f871a14d8c2350a8c7224c90924da46b.jpeg?imageMogr2/quality/60/format/jpg
            val childTabList: List<Any>,
            val dataType: String, // TagInfo
            val description: String,
            val follow: Follow,
            val headerImage: String, // http://img.kaiyanapp.com/f871a14d8c2350a8c7224c90924da46b.jpeg?imageMogr2/quality/60/format/jpg
            val id: Int, // 540
            val lookCount: Int, // 8757
            val name: String, // 汪星人
            val recType: Int, // 0
            val shareLinkUrl: String, // http://www.kaiyanapp.com/campaign/tag_square/tag_square.html?tid=540&utm_campaign=routine&utm_medium=share&pageSource=tagSquare
            val tagDynamicCount: Int, // 1162
            val tagFollowCount: Int, // 1050
            val tagVideoCount: Int // 72
    ) {
        data class Follow(
            val followed: Boolean, // false
            val itemId: Int, // 540
            val itemType: String // tag
        )
    }
}