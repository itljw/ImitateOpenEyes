package com.itljw.openeyes.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.view.View
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.ui.common.entity.TabInfo
import com.itljw.openeyes.ui.home.adapter.CategoryDetailPagerAdapter
import com.itljw.openeyes.ui.home.entity.AuthorDetailEntity
import com.itljw.openeyes.ui.home.vm.AuthorDetailViewModel
import com.itljw.openeyes.ui.video.dialog.VideoMoreDialogFragment
import com.itljw.openeyes.util.GlideUtil
import get
import kotlinx.android.synthetic.main.activity_author_detail.*
import toast

/**
 * Created by JW on on 2019/4/27 21:01.
 * Email : 1481013718@qq.com
 * Description : 作者详情页
 */
class AuthorDetailActivity : BaseActivity() {

    private var id: Int = 0
    private var tabIndex: Int = 0
    private var userType: String = ""
    private var pgcInfo: AuthorDetailEntity.PgcInfo? = null

    private val mViewModel by lazy { ViewModelProviders.of(this).get<AuthorDetailViewModel>() }

    private val mTabList by lazy { mutableListOf<TabInfo.Tab>() }
    private val adapter by lazy { CategoryDetailPagerAdapter(supportFragmentManager, mTabList) }

    override fun getLayoutId(): Int =
            R.layout.activity_author_detail

    companion object {

        private const val EXTRA_ID = "extra_id"
        private const val EXTRA_USER_TYPE = "extra_user_type"
        private const val EXTRA_TAB_INDEX = "extra_tab_index"

        fun launch(mContext: Context?, id: Int, userType: String = "PGC", tabIndex: Int = 0) {
            mContext?.startActivity(
                    Intent(mContext, AuthorDetailActivity::class.java)
                            .putExtra(EXTRA_ID, id)
                            .putExtra(EXTRA_USER_TYPE, userType)
                            .putExtra(EXTRA_TAB_INDEX, tabIndex)
            )
        }
    }

    override fun handleIntent(intent: Intent) {
        super.handleIntent(intent)

        id = intent.getIntExtra(EXTRA_ID, 0)
        userType = intent.getStringExtra(EXTRA_USER_TYPE)
        tabIndex = intent.getIntExtra(EXTRA_TAB_INDEX, 0)
    }

    override fun doBusiness() {

        vpAuthorDetail.adapter = adapter

        mViewModel.getAuthorDetailData(id, userType)

        // 返回键点击事件
        ivAuthorDetailBack.setOnClickListener { finish() }

        // 更多事件
        ivAuthorDetailMore.setOnClickListener {
            val dataList = ArrayList<String>().also { it.add(getString(R.string.author_hide_author)) }

            VideoMoreDialogFragment.show(supportFragmentManager, getString(R.string.video_more_operation), dataList)
                    .onVideoDialogItemClickListener = object : VideoMoreDialogFragment.OnVideoDialogItemClickListener {
                override fun onItemClick(position: Int) {
                    toast("屏蔽该作者")
                }
            }
        }

        // AppbarLayout滑动监听
        ablAuthorDetail.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            // 设置透明度
            val alpha = Math.min(Math.abs(verticalOffset) / appBarLayout.totalScrollRange.toFloat(), 1.0f) * 255
            tvAuthorDetailTopTitle.visibility = if (alpha > 0) View.VISIBLE else View.GONE
            rlAuthorDetailTopBack.background.alpha = alpha.toInt()
            tvAuthorDetailTopTitle.background.alpha = alpha.toInt()

            val isChangeImg = alpha > 255 / 2
            ivAuthorDetailBack.setImageResource(if (isChangeImg) R.drawable.ic_action_back else R.drawable.ic_action_back_white)
            ivAuthorDetailMore.setImageResource(if (isChangeImg) R.drawable.ic_menu_more else R.drawable.ic_menu_more_white)
            ivAuthorDetailShare.setImageResource(if (isChangeImg) R.drawable.ic_action_share_grey else R.drawable.ic_action_share)
        }

        // 事件处理
        mTbFollow.setOnClickListener {
            // 关注
            pgcInfo?.let { pgcInfo ->
                ActionType.handleAction(this@AuthorDetailActivity, pgcInfo.myFollowCountActionUrl)
            }
        }

        mTbFans.setOnClickListener {
            // 粉丝
            pgcInfo?.let { pgcInfo ->
                ActionType.handleAction(mContext, pgcInfo.followCountActionUrl)
            }
        }

    }

    override fun handleObserver() {
        super.handleObserver()
        mViewModel.mAuthorDetailLiveData.observe(this, Observer { data ->
            data?.let { authorDetail ->

                pgcInfo = authorDetail.pgcInfo ?: authorDetail.userInfo

                pgcInfo?.apply {
                    cover?.let { cover ->
                        GlideUtil.displayImage(this@AuthorDetailActivity, cover, ivAuthorDetailBg)
                    }

                    GlideUtil.displayCircleImageWithBorder(this@AuthorDetailActivity,
                            icon, ivAuthorDetailAvatar, 2, R.color.color_white)

                    tvAuthorDetailName.text = name
                    tvAuthorDetailTopTitle.text = name
                    tvAuthorDetailBrief.text = brief
                    mTvAuthorDetailDesc.text = description

                    mTbWorks.setTopText(videoCount.toString()).setBottomText("作品")
                    mTbFollow.setTopText(myFollowCount.toString()).setBottomText("关注")
                    mTbFans.setTopText(followCount.toString()).setBottomText("粉丝")
                }

                // 设置tab数据
                val tabList = authorDetail.tabInfo.tabList
                adapter.replaceData(tabList)

                vpAuthorDetail.offscreenPageLimit = tabList.size - 1 // 设置缓存数量

                tlAuthorDetail.setViewPager(vpAuthorDetail)
                vpAuthorDetail.setCurrentItem(tabIndex, false) // 设置默认选中位置
            }
        })
    }
}