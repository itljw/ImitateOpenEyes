package com.itljw.openeyes.ui.follow.entity

import java.io.Serializable


data class DynamicDataEntity(
        val itemList: List<Item>,
        val count: Int, // 10
        val total: Int, // 0
        val nextPageUrl: String, // http://baobab.kaiyanapp.com/api/v6/community/tab/dynamicFeeds?start=10&num=10
        val adExist: Boolean // false
) {

    data class Item(
            val type: String, // DynamicInfoCard
            val data: Data,
            val tag: Any, // null
            val id: Int, // 0
            val adIndex: Int // -1
    ) {

        data class Data(
                val dataType: String, // DynamicReplyCard
                val dynamicType: String, // reply
                val text: String, // 评论:
                val actionUrl: String, // eyepetizer://replies/video?videoId=114706&top=1018176504237916160&videoTitle=%E8%83%B8%E5%A4%AA%E5%B0%8F%E6%80%8E%E4%B9%88%E5%8A%9E%EF%BC%9F%E6%80%A5%EF%BC%8C%E5%9C%A8%E7%BA%BF%E7%AD%89%EF%BC%81
                val user: User,
                val mergeNickName: Any, // null
                val mergeSubTitle: Any, // null
                val merge: Boolean, // false
                val createDate: Long, // 1531587170000
                val simpleVideo: SimpleVideo,
                val reply: Reply
        ) {

            data class User(
                    val uid: Int, // 300755026
                    val nickname: String, // 永恒之火
                    val avatar: String, // http://thirdwx.qlogo.cn/mmopen/vi_32/TjGzbFhrwIaJWLnaRDRtbRNibQibohaEWeRlhV9QKjW3g9bAMicXxkQWxCyYTXgzeHibkUXEEdwpicP5hSUr9FWGuug/132
                    val userType: String, // NORMAL
                    val ifPgc: Boolean, // false
                    val description: Any, // null
                    val area: Any, // null
                    val gender: Any, // null
                    val registDate: Long, // 1497010828000
                    val releaseDate: Any, // null
                    val cover: Any, // null
                    val actionUrl: String?, // eyepetizer://pgc/detail/300755026/?title=%E6%B0%B8%E6%81%92%E4%B9%8B%E7%81%AB&userType=NORMAL&tabIndex=0
                    val followed: Boolean, // false
                    val limitVideoOpen: Boolean, // true
                    val library: String, // BLOCK
                    val uploadStatus: String, // NORMAL
                    val bannedDate: Any, // null
                    val bannedDays: Int // 0
            ):Serializable


            data class Reply(
                    val id: Long, // 1018176504237916160
                    val videoId: Int, // 114706
                    val videoTitle: String, // 胸太小怎么办？急，在线等！
                    val message: String, // 从小见惯了大胸，所以长大了性取向正常，但对大胸没什么兴趣。有没有我这样的？
                    val likeCount: Int, // 8
                    val showConversationButton: Boolean, // false
                    val parentReplyId: Long, // 0
                    val rootReplyId: Long, // 1018176504237916160
                    val ifHotReply: Boolean, // true
                    val liked: Boolean, // false
                    val parentReply: Any, // null
                    val user: User
            ) : Serializable {

                data class User(
                        val uid: Int, // 300755026
                        val nickname: String, // 永恒之火
                        val avatar: String, // http://thirdwx.qlogo.cn/mmopen/vi_32/TjGzbFhrwIaJWLnaRDRtbRNibQibohaEWeRlhV9QKjW3g9bAMicXxkQWxCyYTXgzeHibkUXEEdwpicP5hSUr9FWGuug/132
                        val userType: String, // NORMAL
                        val ifPgc: Boolean, // false
                        val description: Any, // null
                        val area: Any, // null
                        val gender: Any, // null
                        val registDate: Long, // 1497010828000
                        val releaseDate: Any, // null
                        val cover: Any, // null
                        val actionUrl: String, // eyepetizer://pgc/detail/300755026/?title=%E6%B0%B8%E6%81%92%E4%B9%8B%E7%81%AB&userType=NORMAL&tabIndex=1
                        val followed: Boolean, // false
                        val limitVideoOpen: Boolean, // true
                        val library: String, // BLOCK
                        val uploadStatus: String, // NORMAL
                        val bannedDate: Any, // null
                        val bannedDays: Int // 0
                ) : Serializable
            }


            data class SimpleVideo(
                    val id: Int, // 114706
                    val resourceType: String, // video
                    val uid: Int, // 0
                    val title: String, // 胸太小怎么办？急，在线等！
                    val description: String, // 因为胸小，她被出轨了！因为胸小，她没有恋爱生活，女主人公被生活打击的快崩溃了，最后遇见的梦中男神，最后竟也要求她去隆胸，她会妥协吗？
                    val cover: Cover?,
                    val category: String?, // 音乐
                    val playUrl: String, // http://baobab.kaiyanapp.com/api/v1/playUrl?vid=114706&resourceType=video&editionType=default&source=aliyun
                    val duration: Int, // 338
                    val releaseTime: Long, // 1531584075000
                    val consumption: Any, // null
                    val collected: Boolean, // false
                    val actionUrl: String, // eyepetizer://ugcResourceDetail?id=114706&resourceType=video
                    val onlineStatus: String, // ONLINE
                    val count: Int // 0
            ) : Serializable {

                data class Cover(
                        val feed: String, // http://img.kaiyanapp.com/29a4afcde42b2d1f5cc2dd2d48e95ad7.png?imageMogr2/quality/60/format/jpg
                        val detail: String, // http://img.kaiyanapp.com/29a4afcde42b2d1f5cc2dd2d48e95ad7.png?imageMogr2/quality/60/format/jpg
                        val blurred: String, // http://img.kaiyanapp.com/3bb88f806b45fec6b6f18982878407cb.png?imageMogr2/quality/60/format/jpg
                        val sharing: Any, // null
                        val homepage: String // http://img.kaiyanapp.com/29a4afcde42b2d1f5cc2dd2d48e95ad7.png?imageView2/1/w/720/h/560/format/jpg/q/75|watermark/1/image/aHR0cDovL2ltZy5rYWl5YW5hcHAuY29tL2JsYWNrXzMwLnBuZw==/dissolve/100/gravity/Center/dx/0/dy/0|imageslim
                ) : Serializable
            }
        }
    }
}