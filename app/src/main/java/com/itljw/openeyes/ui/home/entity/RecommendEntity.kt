package com.itljw.openeyes.ui.home.entity

import com.itljw.openeyes.ui.follow.entity.DynamicDataEntity
import java.io.Serializable

// 实际可使用一个kt文件，将所有的实体都放进来...

data class RecommendEntity(
        val adExist: Boolean, // false
        val count: Int, // 8
        val itemList: List<Item>,
        val nextPageUrl: String?, // http://baobab.kaiyanapp.com/api/v5/index/tab/allRec?page=2&isTag=true&adIndex=8
        val total: Int // 0
)

data class Item(
        val type: String,
        val data: Data,
        val tag: Any,
        val id: Int,
        val adIndex: Int
)

data class Data(

        val dataType: String,
        val id: String, // reply返回的数据会出现int转换异常，例：1101796909149847600
        val title: String,
        val description: String,
        val library: String,
        val tags: List<Tag>?,
        val consumption: Consumption,
        val resourceType: String,
        val slogan: Any,
        val provider: Provider,
        val category: String,
        val author: Author,
        val cover: Cover?,
        val playUrl: String,
        val thumbPlayUrl: Any,
        val duration: Int,
        val webUrl: WebUrl,
        val releaseTime: Long,
        val playInfo: List<PlayInfo>,
        val campaign: Any,
        val waterMarks: Any,
        val adTrack: Any,
        val type: String,
        val titlePgc: String,
        val descriptionPgc: String,
        val remark: String,
        val ifLimitVideo: Boolean,
        val searchWeight: Int,
        val idx: Int,
        val shareAdTrack: Any,
        val favoriteAdTrack: Any,
        val webAdTrack: Any,
        val date: Long,
        val promotion: Any,
        val label: Any,
        val labelList: List<Any>,
        val descriptionEditor: String,
        val collected: Boolean,
        val played: Boolean,
        val subtitles: List<Any>,
        val lastViewTime: Any,
        val playlists: Any,
        val src: Int,
        val header: Header,
        val itemList: List<Item>,
        val content: Content,
        val follow: Follow?,
        val text: String,
        val image: String,
        val owner: Owner,
        val url: String,
        val urls: ArrayList<String>,
        val icon: String,
        val reply: DynamicDataEntity.Item.Data.Reply?,
        val simpleVideo: DynamicDataEntity.Item.Data.SimpleVideo?,
        val createDate: Long,
        val user: DynamicDataEntity.Item.Data.User,
        val actionUrl: String?,
        // 回复添加
        val message: String,
        val videoTitle: String,
        val createTime: Long,
        val likeCount: Int,
        val liked: Boolean,
        val briefCard: BriefCard?

) : Serializable

data class PlayInfo(
        val height: Int,
        val width: Int,
        val urlList: List<Url>,
        val name: String,
        val type: String,
        val url: String
) : Serializable

data class Url(
        val name: String,
        val url: String,
        val size: Int
) : Serializable

data class Tag(
        val id: Int,
        val name: String,
        val actionUrl: String,
        val adTrack: Any,
        val desc: Any,
        val bgPicture: String,
        val headerImage: String,
        val tagRecType: String
) : Serializable

data class Provider(
        val name: String,
        val alias: String,
        val icon: String
) : Serializable

data class Consumption(
        val collectionCount: Int,
        val shareCount: Int,
        val replyCount: Int
) : Serializable

data class Cover(
        val feed: String,
        val detail: String,
        var blurred: String,
        val sharing: Any,
        val homepage: Any
) : Serializable

data class WebUrl(
        val raw: String,
        val forWeibo: String
) : Serializable

data class Author(
        val id: Int,
        val icon: String,
        val name: String,
        val description: String,
        val link: String,
        val latestReleaseTime: Long,
        val videoNum: Int,
        val adTrack: Any,
        val follow: Follow,
        val shield: Shield,
        val approvedNotReadyVideoCount: Int,
        val ifPgc: Boolean
) : Serializable

data class Shield(
        val itemType: String,
        val itemId: Int,
        val shielded: Boolean
) : Serializable

data class Follow(
        val itemType: String,
        val itemId: Int,
        val followed: Boolean
) : Serializable

data class Header(
        val actionUrl: String?,
        val font: String?,
        val id: Int,
        val subTitle: String,
        val subTitleFont: String?,
        val textAlign: String,
        val title: String,
        val description: String,
        val icon: String,
        val iconType: String,
        val showHateVideo: Boolean,
        val time: Long,
        val follow: Follow?,
        val issuerName: String
) : Serializable

data class Content(
        val id: Int,
        val type: String,
        val data: Data
) : Serializable

data class Owner(
        val actionUrl: String,
        val avatar: String,
        val cover: String,
        val followed: Boolean,
        val gender: String,
        val nickname: String,
        val uid: String,
        val userType: String
) : Serializable

data class BriefCard(
        val dataType: String,
        val id: Int,
        val icon: String,
        val iconType: String,
        val title: String,
        val subTitle: String,
        val description: String,
        val actionUrl: String

) : Serializable