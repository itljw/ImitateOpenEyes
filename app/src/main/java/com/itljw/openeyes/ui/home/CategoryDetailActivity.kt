package com.itljw.openeyes.ui.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.view.View
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.ui.common.entity.TabInfo
import com.itljw.openeyes.ui.home.adapter.CategoryDetailPagerAdapter
import com.itljw.openeyes.ui.home.vm.CategoryDetailViewModel
import com.itljw.openeyes.util.GlideUtil
import kotlinx.android.synthetic.main.activity_category_detail.*

/**
 * Created by JW on on 2018/9/6 22:33.
 * Email : 1481013718@qq.com
 * Description : 分类详情
 */
class CategoryDetailActivity : BaseActivity() {

    private var categoryId: Int = 0
    private val mTabList by lazy { mutableListOf<TabInfo.Tab>() }
    private val adapter by lazy { CategoryDetailPagerAdapter(supportFragmentManager, mTabList) }

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get(CategoryDetailViewModel::class.java)
    }

    override fun getLayoutId(): Int =
            R.layout.activity_category_detail

    companion object {

        private const val EXTRA_CATEGORY_ID = "extra_category_id"

        fun launch(mContext: Context?, categoryId: Int) {
            mContext?.startActivity(
                    Intent(mContext, CategoryDetailActivity::class.java)
                            .putExtra(EXTRA_CATEGORY_ID, categoryId)
            )
        }
    }

    override fun handleIntent(intent: Intent) {
        categoryId = intent.getIntExtra(EXTRA_CATEGORY_ID, 0)
    }

    override fun doBusiness() {

        tvTopTitle.typeface = Typeface.DEFAULT_BOLD // 设置粗体
        vpCategoryDetail.adapter = adapter

        // 获取数据
        mViewModel.getCategoryDetailById(categoryId)

        // AppBarLayout监听
        ablCategoryDetail.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            // 设置透明度
            val alpha = Math.min(Math.abs(verticalOffset) / appBarLayout.totalScrollRange.toFloat(), 1.0f) * 255
            rlTopBack.visibility = if (alpha > 0) View.VISIBLE else View.GONE
            rlTopBack.background.alpha = alpha.toInt()
        }

        // 返回键点击事件
        ivBackCategoryDetail.setOnClickListener { finish() }
        ivTopBack.setOnClickListener { finish() }
    }

    override fun handleObserver() {
        mViewModel.mCategoryDetailLiveData.observe(this, Observer { data ->
            data?.let {

                it.categoryInfo.apply {
                    // 设置顶部数据
                    tvCategoryDetailTitle.text = name
                    tvCategoryDetailDesc.text = description
                    tvTopTitle.text = name
                    GlideUtil.displayImageWithColor(mContext, headerImage, ivVideoTagsHeadImage, R.color.color_7E7E7E)
                }

                // 设置tab数据
                val tabList = it.tabInfo.tabList
                val defaultIdx = it.tabInfo.defaultIdx
                adapter.replaceData(tabList)

                vpCategoryDetail.offscreenPageLimit = tabList.size - 1 // 设置缓存数量
                tlCategoryDetail.setViewPager(vpCategoryDetail)
                vpCategoryDetail.setCurrentItem(defaultIdx, false) // 设置在后才生效

            }
        })
    }

}