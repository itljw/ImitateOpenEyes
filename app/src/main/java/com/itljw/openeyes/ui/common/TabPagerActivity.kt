package com.itljw.openeyes.ui.common

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.ui.common.entity.TabInfo
import com.itljw.openeyes.ui.common.vm.TabPagerViewModel
import com.itljw.openeyes.ui.home.adapter.CategoryDetailPagerAdapter
import get
import isEnglish
import kotlinx.android.synthetic.main.activity_tab_pager.*


/**
 * Created by JW on on 2019/5/20 22:22.
 * Email : 1481013718@qq.com
 * Description :
 */
class TabPagerActivity : BaseActivity() {

    private var apiUrl = ""
    private var title = ""

    private val mTabDataList: MutableList<TabInfo.Tab>  by lazy {
        mutableListOf<TabInfo.Tab>()
    }
    private val adapter by lazy {
        CategoryDetailPagerAdapter(supportFragmentManager, mTabDataList)
    }

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get<TabPagerViewModel>()
    }

    override fun getLayoutId(): Int =
            R.layout.activity_tab_pager

    companion object {
        private const val EXTRA_URL = "extra_url"
        private const val EXTRA_TITLE = "extra_title"

        fun launch(mContext: Context?, apiUrl: String, title: String) {
            mContext?.startActivity(
                    Intent(mContext, TabPagerActivity::class.java)
                            .putExtra(EXTRA_URL, apiUrl)
                            .putExtra(EXTRA_TITLE, title)
            )
        }
    }

    override fun handleIntent(intent: Intent) {
        super.handleIntent(intent)
        apiUrl = intent.getStringExtra(EXTRA_URL)
        title = intent.getStringExtra(EXTRA_TITLE)
    }

    override fun doBusiness() {

        tvTabPagerTitle.text = title

        if (title.isEnglish()) { // 如果是全英文，则设置字体
            tvTabPagerTitle.typeface = Typeface.createFromAsset(assets, "fonts/Lobster-1.4.otf")
        }

        vpTabPager.adapter = adapter

        mViewModel.getPagerDataByUrl(apiUrl)

        ivTabPagerBack.setOnClickListener { finish() }
    }

    override fun handleObserver() {
        super.handleObserver()

        mViewModel.tabPagerLiveData.observe(this, Observer { data ->
            val tabInfo = data?.tabInfo
            val defaultIdx = tabInfo?.defaultIdx ?: 0
            tabInfo?.tabList?.let {
                adapter.replaceData(it)

                vpTabPager.offscreenPageLimit = it.size - 1 // 设置缓存数量
                tlTabPager.setViewPager(vpTabPager)
                vpTabPager.setCurrentItem(defaultIdx, false) // 设置默认选中位置
            }
        })
    }

}