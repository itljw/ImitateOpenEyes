package com.itljw.openeyes.ui.home.handler

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.view.View
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.VideoCollectionWithBriefMultiItem
import com.itljw.openeyes.ui.home.adapter.VideoCollectionBriefAdapter
import com.itljw.openeyes.util.GlideUtil
import com.itljw.openeyes.wiget.SpaceItemDecoration
import dp2px
import kotlinx.android.synthetic.main.item_video_collection_with_brief.view.*

/**
 * Created by JW on on 2018/7/26 00:09.
 * Email : 1481013718@qq.com
 * Description :
 */
object VideoCollectionWithBriefHandler {
    fun bindData(helper: BaseViewHolder, item: VideoCollectionWithBriefMultiItem) {

        helper.itemView.apply {

            val header = item.header
            val itemList = item.itemList

            // 设置icon
            GlideUtil.displayCircleImageWithBorder(context, header.icon, ivVideoBriefIcon)

            // 设置是否关注
            if (header.follow != null) {
                tvVideoBriefFollow.visibility = if (!header.follow.followed) View.VISIBLE else View.GONE
            }

            // 设置标题和描述信息
            tvVideoBriefTitle.text = header.title
            tvVideoBriefDesc.text = header.description

            // 事件处理
            rlVideoBriefTop.setOnClickListener {
                ActionType.handleAction(context, header.actionUrl)
            }

            // 设置RecyclerView
            rvVideoBrief.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

            // 重复添加问题
            if (rvVideoBrief.itemDecorationCount == 0) { // 判断是否添加了分割线
                rvVideoBrief.addItemDecoration(SpaceItemDecoration(context.dp2px(3).toInt()))
                PagerSnapHelper().attachToRecyclerView(rvVideoBrief)
            }

            rvVideoBrief.adapter = VideoCollectionBriefAdapter(itemList)
        }

    }

}