package com.itljw.openeyes.ui.home.handler

import android.text.format.DateUtils
import android.widget.TextView
import com.chad.library.adapter.base.BaseViewHolder
import com.itljw.openeyes.R
import com.itljw.openeyes.constants.ActionType
import com.itljw.openeyes.entity.AutoPlayFollowCardMultiItem
import com.itljw.openeyes.ui.video.VideoDetailActivity
import com.itljw.openeyes.util.DateUtil
import com.itljw.openeyes.util.GlideUtil
import inflater
import kotlinx.android.synthetic.main.item_picture_follow_card.view.*

/**
 * Created by JW on on 2018/7/26 00:19.
 * Email : 1481013718@qq.com
 * Description :
 */
object AutoPlayFollowCardHandler {
    fun bindData(helper: BaseViewHolder, item: AutoPlayFollowCardMultiItem) {

        helper.itemView.apply {

            val header = item.header
            val content = item.content

            // 收藏数和回复数
            tvCollectCount.text = content.data.consumption.collectionCount.toString()
            tvReplyCount.text = content.data.consumption.replyCount.toString()

            // 发布时间
            tvPublishTime.text = content.data.releaseTime.let {
                var pattern = "yyyy/MM/dd"
                if (DateUtils.isToday(it)) {  // 今天
                    pattern = "HH:mm"
                }
                DateUtil.millis2String(pattern, it)
            }

            GlideUtil.displayCircleImageWithBorder(context, header.icon, ivAvatar)

            // 发布人
            tvName.text = header.issuerName
            tvReplyMsg.text = content.data.description

            // 标签
            content.data.tags?.let {
                // 设置tag
                llTags.removeAllViews()

                var tagList = it

                if (it.size > 3) { // 最多显示三个
                    tagList = it.subList(0, 3)
                }

                for (tagItem in tagList) {
                    llTags.addView(
                            (context.inflater.inflate(R.layout.item_works_tag, llTags, false) as TextView).apply {
                                text = tagItem.name
                                setOnClickListener { ActionType.handleAction(context, tagItem.actionUrl) }
                            }
                    )
                }
            }

            // 视频封面图
            content.data.cover?.let {
                GlideUtil.displayRoundImage(context, it.detail, ivVideoCover, 5, true)
            }

            // item事件
            setOnClickListener {
                VideoDetailActivity.launch(context, content.data.id.toInt())  // 跳转视频详情页面
            }
        }


    }
}