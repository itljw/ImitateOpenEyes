package com.itljw.openeyes.ui.home.vm

import android.arch.lifecycle.MediatorLiveData
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.base.BaseViewModel
import com.itljw.openeyes.http.*
import com.itljw.openeyes.ui.home.entity.RecommendEntity
import com.itljw.openeyes.util.CategoryDealDataHelper


/**
 * Created by JW on on 2018/7/26 22:05.
 * Email : 1481013718@qq.com
 * Description :
 */
class CategoryViewModel : BaseViewModel() {

    val mRefreshDataLiveData by lazy { MediatorLiveData<List<MultiItemEntity>>() }
    val mLoadMoreDataLiveData by lazy { MediatorLiveData<List<MultiItemEntity>>() }
    val mCategoryLoadState by lazy { MediatorLiveData<LoadDataState>() }

    private var nextPageUrl = ""

    /**
     * 获取数据
     * @param isRefresh 是否是下拉刷新
     */
    fun getCategoryData(url: String, isRefresh: Boolean = true) {

        // 下拉刷新时不显示loading
        mCategoryLoadState.value = if (!isRefresh) LoadDataState.LOADING else LoadDataState.LOAD_MORE
        nextPageUrl = "" // 重置

        addResponseResource(DataManager.getHomeCategoryData(url), object : BaseObserver<RecommendEntity>() {
            override fun onSuccess(data: RecommendEntity?) {
                data?.let {

                    val dealDataList = CategoryDealDataHelper.dealData(it.itemList)

                    if (dealDataList.isEmpty()) {
                        mCategoryLoadState.value = LoadDataState.EMPTY
                        return@let
                    }
                    nextPageUrl = it.nextPageUrl ?: ""
                    mRefreshDataLiveData.value = dealDataList
                }
            }

            override fun onError(code: Int?, msg: String?) {
                super.onError(code, msg)
                mCategoryLoadState.value = LoadDataState.ERROR
            }
        })
    }

    /**
     * 获取下一页数据
     */
    fun getNextCategoryData() {
        if (nextPageUrl.isNotEmpty()) {
            mCategoryLoadState.value = LoadDataState.LOAD_MORE
            addResponseResource(DataManager.getHomeCategoryData(nextPageUrl), object : BaseObserver<RecommendEntity>() {
                override fun onSuccess(data: RecommendEntity?) {
                    data?.let {

                        val dealDataList = CategoryDealDataHelper.dealData(it.itemList)

                        if (dealDataList.isEmpty()) {
                            mCategoryLoadState.value = LoadDataState.EMPTY
                            return@let
                        }
                        mLoadMoreDataLiveData.value = dealDataList
                        nextPageUrl = it.nextPageUrl ?: ""
                    }
                }

                override fun onError(code: Int?, msg: String?) {
                    super.onError(code, msg)
                    mCategoryLoadState.value = LoadDataState.ERROR
                }
            })
        } else { // 没有更多数据
            mCategoryLoadState.value = LoadDataState.NO_MORE_DATA
        }
    }

}