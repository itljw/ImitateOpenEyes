package com.itljw.openeyes.ui.home.adapter

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.R
import com.itljw.openeyes.entity.*
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.ui.home.handler.*

/**
 * Created by JW on on 2018/7/26 22:20.
 * Email : 1481013718@qq.com
 * Description :
 */
class CategoryAdapter :
        BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder>(null) {

    init {
        addItemType(HomeConstant.ITEM_TYPE_TEXT_CARD, R.layout.item_text_card)
        addItemType(HomeConstant.ITEM_TYPE_FOLLOW_CARD, R.layout.item_follow_card)
        addItemType(HomeConstant.ITEM_TYPE_VIDEO_SMALL_CARD, R.layout.item_video_small_card)
        addItemType(HomeConstant.ITEM_TYPE_SQUARE_CARD, R.layout.item_square_card)
        addItemType(HomeConstant.ITEM_TYPE_BANNER, R.layout.item_banner)
        addItemType(HomeConstant.ITEM_TYPE_PICTURE_FOLLOW_CARD, R.layout.item_picture_follow_card)
        addItemType(HomeConstant.ITEM_TYPE_HORIZONTAL_SCROLL_CARD, R.layout.item_horizontal_scroll_card)
        addItemType(HomeConstant.ITEM_TYPE_BRIEF_CARD, R.layout.item_brief_card)
        addItemType(HomeConstant.ITEM_TYPE_VIDEO_COLLECTION_WITH_BRIEF, R.layout.item_video_collection_with_brief)
        addItemType(HomeConstant.ITEM_TYPE_DYNAMIC_INFO_CARD, R.layout.item_dynamic_info_card)
        addItemType(HomeConstant.ITEM_TYPE_AUTO_PLAY_FOLLOW_CARD, R.layout.item_picture_follow_card)
    }

    override fun convert(helper: BaseViewHolder?, item: MultiItemEntity?) {
        when (helper?.itemViewType) {
            HomeConstant.ITEM_TYPE_TEXT_CARD -> { // 文本
                if (item is TextCardMultiItem) {
                    TextCardHandler.bindData(helper, item)
                }
            }
            HomeConstant.ITEM_TYPE_FOLLOW_CARD -> { // 上图下作品信息
                if (item is FollowCardMultiItem) {
                    FollowCardHandler.bindData(helper, item, true)
                }
            }

            HomeConstant.ITEM_TYPE_VIDEO_SMALL_CARD -> { // 左图片右文本
                if (item is VideoSmallCardMultiItem) {
                    VideoSmallCardHandler.bindData(helper, item)
                }
            }

            HomeConstant.ITEM_TYPE_SQUARE_CARD -> { // 开眼精选 轮播图
                if (item is SquareCardMultiItem) {
                    SquareCardHandler.bindData(helper, item)
                }
            }
            HomeConstant.ITEM_TYPE_BANNER -> { // 图片->跳网页
                if (item is BannerMultiItem) {
                    BannerHandler.bindData(helper, item)
                }
            }

            HomeConstant.ITEM_TYPE_PICTURE_FOLLOW_CARD -> { // 与关注.作品ui相似
                if (item is PictureFollowCardMultiItem) {
                    PictureFollowCardHandler.bindData(helper, item)
                }
            }

            HomeConstant.ITEM_TYPE_AUTO_PLAY_FOLLOW_CARD -> { // 自动播放视频的（ui和作品差不多）

                if (item is AutoPlayFollowCardMultiItem) {
                    AutoPlayFollowCardHandler.bindData(helper, item)
                }
            }

            HomeConstant.ITEM_TYPE_HORIZONTAL_SCROLL_CARD -> { // 横向滑动
                if (item is HorizontalScrollCardMultiItem) {
                    HorizontalScrollCardHandler.bindData(helper, item)
                }
            }

            HomeConstant.ITEM_TYPE_BRIEF_CARD -> { // 左边为类别信息，右边为关注Text
                if (item is BriefCardMultiItem) {
                    BriefCardHandler.bindData(helper, item)
                }
            }

            HomeConstant.ITEM_TYPE_VIDEO_COLLECTION_WITH_BRIEF -> { // 上brief下轮播图
                if (item is VideoCollectionWithBriefMultiItem) {
                    VideoCollectionWithBriefHandler.bindData(helper, item)
                }
            }
            HomeConstant.ITEM_TYPE_DYNAMIC_INFO_CARD -> { // 动态UI
                if (item is DynamicInfoCardMultiItem) {
                    DynamicInfoCardHandler.bindData(helper, item)
                }
            }

        }
    }
}