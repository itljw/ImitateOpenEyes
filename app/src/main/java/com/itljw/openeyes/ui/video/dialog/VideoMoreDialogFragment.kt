package com.itljw.openeyes.ui.video.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v4.app.FragmentManager
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseBottomDialogFragment
import com.itljw.openeyes.ui.video.adapter.VideoMoreDialogAdapter
import getScreenWidth
import kotlinx.android.synthetic.main.dialog_video_more.*

/**
 * Created by JW on on 2019/3/8 00:14.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoMoreDialogFragment<T> : BaseBottomDialogFragment(), BaseQuickAdapter.OnItemClickListener {

    private var title: String = ""
    var dataList: List<T> = ArrayList()

    companion object {

        fun <T> show(fg: FragmentManager, title: String, dataList: List<T>): VideoMoreDialogFragment<T> {
            val dialogFragment = VideoMoreDialogFragment<T>()
            dialogFragment.title = title
            dialogFragment.dataList = dataList
            dialogFragment.show(fg, VideoMoreDialogFragment::class.java.simpleName)
            return dialogFragment
        }
    }

    override fun getLayoutId(): Int =
            R.layout.dialog_video_more

    override fun doBusiness() {

        val adapter = VideoMoreDialogAdapter(dataList)
        rvVideoMore.adapter = adapter
        adapter.onItemClickListener = this@VideoMoreDialogFragment

        tvVideoMoreTitle.text = title

        tvVideoMoreCancel.setOnClickListener { dismiss() }
    }

    override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
        onVideoDialogItemClickListener?.onItemClick(position)
        dismiss()
    }

    override fun onStart() {
        super.onStart()

        activity?.apply {

            val window = dialog.window
            val attributes = (window.attributes as WindowManager.LayoutParams).apply {
                gravity = Gravity.BOTTOM
                dimAmount = 0f
                width = getScreenWidth()
                height = ViewGroup.LayoutParams.WRAP_CONTENT
                windowAnimations = R.style.BottomDialogAnimation
            }
            // 不设置BackgroundDrawable，设置的宽度填充屏幕宽度不生效
            isCancelable = true
            window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
            window.attributes = attributes
        }
    }

    var onVideoDialogItemClickListener: OnVideoDialogItemClickListener? = null

    interface OnVideoDialogItemClickListener {
        fun onItemClick(position: Int)
    }

}