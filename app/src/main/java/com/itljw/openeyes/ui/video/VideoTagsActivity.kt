package com.itljw.openeyes.ui.video

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.view.View
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.ui.common.entity.TabInfo
import com.itljw.openeyes.ui.home.adapter.CategoryDetailPagerAdapter
import com.itljw.openeyes.ui.video.vm.VideoTagsViewModel
import com.itljw.openeyes.util.GlideUtil
import get
import kotlinx.android.synthetic.main.activity_video_tags.*

/**
 * Created by JW on on 2019/3/1 22:03.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoTagsActivity : BaseActivity() {

    private var tagId: Int = 0

    private val mTabList by lazy {
        mutableListOf<TabInfo.Tab>()
    }

    private val adapter by lazy {
        CategoryDetailPagerAdapter(supportFragmentManager, mTabList)
    }

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get<VideoTagsViewModel>()
    }

    override fun getLayoutId(): Int =
            R.layout.activity_video_tags

    companion object {
        private const val EXTRA_TAG_ID = "tag_id"
        fun launch(mContext: Context, tagId: Int) {
            mContext.startActivity(
                    Intent(mContext, VideoTagsActivity::class.java)
                            .putExtra(EXTRA_TAG_ID, tagId)
            )
        }
    }

    override fun handleIntent(intent: Intent) {
        tagId = intent.getIntExtra(EXTRA_TAG_ID, 0)
    }

    override fun doBusiness() {

        vpVideoTags.adapter = adapter

        // 请求
        mViewModel.getVideoTagsTabData(tagId)

        // AppBarLayout监听
        ablVideoTags.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            // 设置透明度
            val alpha = Math.min(Math.abs(verticalOffset) / appBarLayout.totalScrollRange.toFloat(), 1.0f) * 255
            rlVideoTagsTopBack.visibility = if (alpha > 0) View.VISIBLE else View.GONE
            rlVideoTagsTopBack.background.alpha = alpha.toInt()
        }

        // 返回键点击事件
        ivVideoTagsBack.setOnClickListener { finish() }
        ivVideoTagsTopBack.setOnClickListener { finish() }
    }

    override fun handleObserver() {
        mViewModel.mVideoTagsLiveData.observe(this, Observer { data ->
            data?.apply {
                // 设置顶部数据
                tvVideoTagsTitle.text = tagInfo.name
                tvVideoTagsDesc.text = String.format("%s作品 / %s关注者 / %s动态",
                        tagInfo.tagVideoCount, tagInfo.tagFollowCount, tagInfo.tagDynamicCount)
                tvVideoTagsTopTitle.text = tagInfo.name

                GlideUtil.displayImage(mContext, tagInfo.headerImage, ivVideoTagsHeadImage)

                // 设置tab数据
                val defaultIdx = tabInfo.defaultIdx
                adapter.replaceData(tabInfo.tabList)

                stlVideoTags.setViewPager(vpVideoTags)
                vpVideoTags.setCurrentItem(defaultIdx, false) // 设置在后才生效
            }
        })
    }
}