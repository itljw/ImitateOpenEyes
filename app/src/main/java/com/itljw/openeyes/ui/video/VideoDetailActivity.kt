package com.itljw.openeyes.ui.video

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.chad.library.adapter.base.BaseQuickAdapter
import com.itljw.openeyes.GlideApp
import com.itljw.openeyes.R
import com.itljw.openeyes.base.BaseActivity
import com.itljw.openeyes.db.AppRoomDatabase
import com.itljw.openeyes.db.Video
import com.itljw.openeyes.entity.VideoSmallCardMultiItem
import com.itljw.openeyes.event.CloseFloatVideoEvent
import com.itljw.openeyes.event.FloatVideoEvent
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.home.AuthorDetailActivity
import com.itljw.openeyes.ui.home.entity.Data
import com.itljw.openeyes.ui.video.adapter.VideoDetailAdapter
import com.itljw.openeyes.ui.video.dialog.BottomCommentDialogFragment
import com.itljw.openeyes.ui.video.dialog.VideoMoreDialogFragment
import com.itljw.openeyes.ui.video.vm.VideoDetailViewModel
import com.itljw.openeyes.util.CategoryDealDataHelper
import com.itljw.openeyes.util.GlideUtil
import com.itljw.openeyes.wiget.SimpleVideoPlayer
import com.scwang.smartrefresh.layout.util.DensityUtil
import com.shuyu.gsyvideoplayer.GSYVideoManager
import com.shuyu.gsyvideoplayer.player.PlayerFactory
import com.shuyu.gsyvideoplayer.utils.OrientationUtils
import get
import kotlinx.android.synthetic.main.activity_video_detail.*
import kotlinx.android.synthetic.main.layout_video_detail_header.*
import kotlinx.android.synthetic.main.layout_video_detail_tags.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import toast
import tv.danmaku.ijk.media.exo2.Exo2PlayerManager
import java.util.*


/**
 * Created by JW on on 2018/9/10 23:49.
 * Email : 1481013718@qq.com
 * Description : 视频详情
 */
class VideoDetailActivity : BaseActivity(), BaseQuickAdapter.OnItemChildClickListener,
        SimpleVideoPlayer.OnVideoPlayerClickListener, VideoMoreDialogFragment.OnVideoDialogItemClickListener {

    private var videoId = 0 // 视频id
    private var currentPosition = 0 // 当前视频播放进度
    private var currentPlayUrl: String = "" // 视频url

    private var videoData: Data? = null
    private var orientationUtils: OrientationUtils? = null
    private var mBottomCommentDialogFragment: BottomCommentDialogFragment? = null

    private val adapter by lazy { VideoDetailAdapter() }

    private val mViewModel by lazy {
        ViewModelProviders.of(this).get<VideoDetailViewModel>()
    }

    override fun getLayoutId(): Int =
            R.layout.activity_video_detail

    companion object {

        private const val EXTRA_VIDEO_ID: String = "extra_video_id"
        private const val EXTRA_CURRENT_POSITION: String = "extra_current_position"
        private const val REQUEST_CODE_OVERLAY_WINDOW = 0x11

        fun launch(mContext: Context?, videoId: Int) {
            mContext?.startActivity(Intent(mContext, VideoDetailActivity::class.java)
                    .putExtra(EXTRA_VIDEO_ID, videoId)
            )
        }

        /**
         * 带播放进度
         */
        fun launchWithPosition(mContext: Context?, videoId: Int, currentPosition: Int) {
            mContext?.startActivity(Intent(mContext, VideoDetailActivity::class.java)
                    .putExtra(EXTRA_VIDEO_ID, videoId)
                    .putExtra(EXTRA_CURRENT_POSITION, currentPosition)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_keep_still)
    }

    override fun handleIntent(intent: Intent) {
        videoId = intent.getIntExtra(EXTRA_VIDEO_ID, 0)
        currentPosition = intent.getIntExtra(EXTRA_CURRENT_POSITION, 0)
    }

    /*  override fun doBeforeContentView() {
          window.requestFeature(Window.FEATURE_NO_TITLE)
          if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
              window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
              window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
                      or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                      or ViewmVideoPlayerDetail.SYSTEM_UI_FLAG_LAYOUT_STABLE
                      or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                      or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
              window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
              window.statusBarColor = Color.TRANSPARENT
              window.navigationBarColor = Color.TRANSPARENT
          }
      }

      override fun setStatusBar() {
          if (VERSION.SDK_INT < VERSION_CODES.LOLLIPOP) super.setStatusBar()
      }*/

    override fun doBusiness() {

        EventBus.getDefault().post(CloseFloatVideoEvent()) // 如果有开启悬浮播放，则关闭

        initRecyclerView()

        // request
        mViewModel.getVideoDetailData(videoId)
        mViewModel.getVideoRelatedData(videoId)

        // 事件
        tvReply.setOnClickListener {
            // 弹出评论对话框
            videoData?.cover?.apply {
                mBottomCommentDialogFragment = BottomCommentDialogFragment.newInstance(videoData?.id?.toInt()
                        ?: 0, blurred).apply {
                    show(supportFragmentManager, BottomCommentDialogFragment::class.java.simpleName)
                }
            }
        }

        // 作者详情页
        rlAuthorInfo.setOnClickListener {
            AuthorDetailActivity.launch(this@VideoDetailActivity, videoData?.author?.id ?: 0)
        }
    }

    /**
     * 初始化RecyclerView
     */
    private fun initRecyclerView() {
        mRvVideoDetail.layoutManager = LinearLayoutManager(this)
        mRvVideoDetail.isNestedScrollingEnabled = false

        mRvVideoDetail.adapter = adapter
        adapter.openLoadAnimation()

        adapter.onItemChildClickListener = this
    }

    override fun handleObserver() {
        super.handleObserver()

        mViewModel.videoDetailLiveData.observe(this, Observer { data ->

            data?.let {
                videoData = it

                initVideoConfig() // 初始化配置

                handleVideoData(it) // 处理视频数据

                handleVideoTags(it)  // 设置tag

                // 保存观看记录
                GlobalScope.launch {
                    // 只能运行在子线程
                    AppRoomDatabase.getInstance(mContext)
                            .videoDao().addVideo(Video(videoId, it.title, it.cover?.detail
                                    ?: "", System.currentTimeMillis()))
                }
            }
        })

        mViewModel.videoRelatedLiveData.observe(this, Observer { data ->
            data?.let {
                val dealDataList = CategoryDealDataHelper.dealData(it.itemList)
                adapter.replaceData(dealDataList)
            }
        })
    }

    /**
     * 处理视频数据
     */
    private fun handleVideoData(data: Data) {

        // 设置背景
        videoData?.cover?.run {
            GlideApp.with(mContext)
                    .load(blurred)
                    .format(DecodeFormat.PREFER_ARGB_8888)
                    .transition(DrawableTransitionOptions().crossFade())
                    .into(mIvVideoDetailBg)
        }

        // 设置标题等
        tvTitle.setShowText(data.title)
        tvCategory.setShowText(String.format("#%s", data.category))
        tvReplyMsg.setShowText(data.description)

        // 设置收藏数等
        tvFavorites.text = data.consumption.collectionCount.toString()
        tvShare.text = data.consumption.shareCount.toString()
        tvReply.text = data.consumption.replyCount.toString()

        // 设置作者信息
        GlideUtil.displayCircleImage(this@VideoDetailActivity, data.author.icon, ivAvatar)
        tvAuthorName.text = data.author.name
        tvAuthorDesc.text = data.author.description

        // 是否关注
        tvAuthorFollow.visibility = if (data.author.follow.followed) View.GONE else View.VISIBLE
    }

    /**
     * 处理视频Tags
     */
    private fun handleVideoTags(data: Data) {
        var resultTags = data.tags
        if (resultTags != null && resultTags.isNotEmpty()) {
            llTagContainer.visibility = View.VISIBLE
            llVideoDetailHeaderRoot.visibility = View.VISIBLE

            // 最多只显示三个
            if (resultTags.size > 3) {
                resultTags = resultTags.subList(0, 3)
            }

            llTagContainer.removeAllViews() // 清空View

            for (index in 0 until resultTags.size) {
                val tag = resultTags[index]

                val view = LayoutInflater.from(this@VideoDetailActivity)
                        .inflate(R.layout.layout_video_detail_tags, llTagContainer, false)

                // layoutParams
                val layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT)
                layoutParams.weight = 1f
                if (index == resultTags.size - 1) {
                    layoutParams.rightMargin = 0
                } else {
                    layoutParams.rightMargin = DensityUtil.dp2px(10f)
                }
                view.layoutParams = layoutParams

                GlideUtil.displayRoundImage(this@VideoDetailActivity, tag.headerImage, view.ivTagImg,
                        5, true, R.color.color_88000000)
                view.tvTagName.text = String.format("#%s#", tag.name)

                // 设置点击事件 跳分类详情页面
                view.setOnClickListener { VideoTagsActivity.launch(mContext, tag.id) }

                llTagContainer.addView(view)
            }
        } else {
            llTagContainer.visibility = View.GONE
        }
    }

    /**
     * 初始化视频配置等
     */
    private fun initVideoConfig() {
        PlayerFactory.setPlayManager(Exo2PlayerManager::class.java) // 设置内核为EXO

        with(mVideoPlayerDetail) {

            currentPlayUrl = videoData?.playUrl ?: "" // 播放url
            setUp(currentPlayUrl, true, "")

            if (currentPosition != 0) {
                setCurrentPosition(currentPosition) // 播放进度
            }

            // 设置进度条
            setBottomProgressBarDrawable(ContextCompat.getDrawable(mContext, R.drawable.layer_video_seek_progress))
            setBottomShowProgressBarDrawable(
                    ContextCompat.getDrawable(mContext, R.drawable.layer_video_seek_progress),
                    ContextCompat.getDrawable(mContext, R.drawable.video_seek_thumb))

            //增加封面
            thumbImageView = ImageView(this@VideoDetailActivity).apply {
                scaleType = ImageView.ScaleType.CENTER_CROP
                videoData?.cover?.let {
                    GlideUtil.displayImage(this@VideoDetailActivity, it.detail, this)
                }

            }

            onVideoPlayerClickListener = this@VideoDetailActivity

            isRotateViewAuto = false
            isLockLand = false
            isShowFullAnimation = false // 不显示旋转动画
            isNeedLockFull = true
            seekRatio = 1f

            titleTextView.visibility = View.GONE  // 隐藏title
            backButton.visibility = View.VISIBLE  //设置返回键
            setIsTouchWiget(true)  //是否可以滑动调整
            lifecycle.addObserver(this) // 生命周期管理
        }

        //设置旋转
        orientationUtils = OrientationUtils(this, mVideoPlayerDetail)

        mVideoPlayerDetail.setVideoAllCallBack(object : SimpleVideoCallBack {
            override fun onPrepared(url: String, vararg objects: Any) {
                super.onPrepared(url, *objects)
                //开始播放了才能旋转和全屏
                orientationUtils?.isEnable = true
            }

            override fun onQuitFullscreen(url: String, vararg objects: Any) {
                super.onQuitFullscreen(url, *objects)
                orientationUtils?.backToProtVideo()
            }
        })

        //设置全屏按键功能,这是使用的是选择屏幕，而不是全屏
        mVideoPlayerDetail.fullscreenButton.setOnClickListener {
            orientationUtils?.resolveByClick()
            //第一个true是否需要隐藏actionbar，第二个true是否需要隐藏statusbar
            mVideoPlayerDetail.startWindowFullscreen(this, true, true)
            mBottomCommentDialogFragment?.dismiss() // 全屏需隐藏评论，额...否则，评论会在最上层，遮住视频
        }

        //设置返回按键功能
        mVideoPlayerDetail.backButton.setOnClickListener { onBackPressed() }
        mVideoPlayerDetail.startPlayLogic()
        mVideoPlayerDetail.hideAllWidgets()
    }

    // 更多操作
    override fun onMoreClick() {
        val dataList = ArrayList<String>().also { it.add(getString(R.string.video_switch_definition)) }

        VideoMoreDialogFragment.show(supportFragmentManager, getString(R.string.video_more_operation), dataList)
                .onVideoDialogItemClickListener = object : VideoMoreDialogFragment.OnVideoDialogItemClickListener {
            override fun onItemClick(position: Int) {

                VideoMoreDialogFragment.show(supportFragmentManager, getString(R.string.video_switch_definition), videoData?.playInfo
                        ?: Collections.emptyList())
                        .onVideoDialogItemClickListener = this@VideoDetailActivity
            }
        }
    }

    // 清晰度切换Dialog
    override fun onItemClick(position: Int) {

        videoData?.apply {
            val playInfo = playInfo[position]

            if (mVideoPlayerDetail.currentPlayer is SimpleVideoPlayer) {

                val currentPosition = mVideoPlayerDetail.currentPlayer.currentPositionWhenPlaying

                currentPlayUrl = playInfo.url
                // 全屏切换，需获取到当前的播放器重设url和进度
                mVideoPlayerDetail.currentPlayer.setUp(playUrl, true, "")
//                mVideoPlayerDetail.setCurrentPosition(currentPosition)
                mVideoPlayerDetail.currentPlayer.startPlayLogic()
                mVideoPlayerDetail.currentPlayer.setVideoAllCallBack(object : SimpleVideoCallBack {
                    override fun onPrepared(url: String, vararg objects: Any) {
                        super.onPrepared(url, *objects)
                        mVideoPlayerDetail.currentPlayer.seekTo(currentPosition.toLong())
                    }
                })
            }

        }

    }

    // 子View事件
    override fun onItemChildClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
        when (view?.id) {
            R.id.rlVideoInfo -> {
                // 刷新当前视频信息
                val multiItemEntity = this.adapter.data[position]
                if (multiItemEntity is VideoSmallCardMultiItem) {
                    videoData = multiItemEntity.data

                    videoId = videoData?.id?.toInt() ?: 0

                    // 滑动到顶部
                    mNsvVideoDetail.smoothScrollTo(0, 0)
                    mRvVideoDetail.smoothScrollToPosition(0)

                    llVideoDetailHeaderRoot.visibility = View.GONE
                    this.adapter.data.clear()
                    adapter?.notifyDataSetChanged()

                    mViewModel.getVideoDetailData(videoId)
                    mViewModel.getVideoRelatedData(videoId)
                }
            }
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        mVideoPlayerDetail.onConfigurationChanged(this, newConfig, orientationUtils, true, true)
    }

    override fun onDestroy() {
        super.onDestroy()
        orientationUtils?.releaseListener()
    }

    override fun onBackPressed() {

        // 如果评论对话框显示，则关闭对话框
        if (mBottomCommentDialogFragment != null && true == mBottomCommentDialogFragment?.dialog?.isShowing) {
            mBottomCommentDialogFragment?.dismiss()
            return
        }

        orientationUtils?.backToProtVideo()
        if (GSYVideoManager.backFromWindowFull(this)) {
            return
        }

        mVideoPlayerDetail.setVideoAllCallBack(null)

        if (showOverlayWindow()) return

        mVideoPlayerDetail.release() // 释放
        super.onBackPressed()
    }

    /**
     * 是否显示悬浮窗
     */
    private fun showOverlayWindow(): Boolean {

        val overlayWindowStatus = DataManager.getOverlayWindowStatus()
        if (!overlayWindowStatus) {
            return false
        }

        val currentVideoHeight = mVideoPlayerDetail.currentVideoHeight
        val currentVideoWidth = mVideoPlayerDetail.currentVideoWidth

        if (currentVideoHeight != 0 && currentVideoWidth != 0) {

            if (showOverlaysPermissionDialog()) {
                AlertDialog.Builder(this@VideoDetailActivity)
                        .setTitle(getString(R.string.video_open_overlay_permission))
                        .setMessage(getString(R.string.video_open_overlay_permission_tip))
                        .setPositiveButton(getString(R.string.video_open)) { _, _ ->
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))
                                startActivityForResult(intent, REQUEST_CODE_OVERLAY_WINDOW)
                            } else {
                                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SYSTEM_ALERT_WINDOW), REQUEST_CODE_OVERLAY_WINDOW)
                            }
                        }
                        .setNegativeButton(getString(R.string.video_unopened)) { _, _ ->
                            toast(getString(R.string.video_forbid_overlay_permission))
                            DataManager.saveOverlayWindowStatus(false)
                            super.onBackPressed()
                        }
                        .show()
            } else {
                EventBus.getDefault().post(FloatVideoEvent(videoId, currentPlayUrl, currentVideoHeight,
                        currentVideoWidth, mVideoPlayerDetail.currentPositionWhenPlaying))
                super.onBackPressed()
            }

            return true
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (REQUEST_CODE_OVERLAY_WINDOW == requestCode) {
            // Settings.canDrawOverlays(this)坑：部分手机会有延迟，开启后获取值还是false
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this)) {

                val currentVideoHeight = mVideoPlayerDetail.currentVideoHeight
                val currentVideoWidth = mVideoPlayerDetail.currentVideoWidth

                EventBus.getDefault().post(FloatVideoEvent(videoId, currentPlayUrl, currentVideoHeight,
                        currentVideoWidth, mVideoPlayerDetail.currentPositionWhenPlaying))

                super.onBackPressed()
            }
        }

    }

    /**
     * 是否需要弹申请权限Dialog
     */
    private fun showOverlaysPermissionDialog(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // 需申请权限
            !Settings.canDrawOverlays(this)
        } else {
            ContextCompat.checkSelfPermission(this, Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED
        }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_keep_still, R.anim.slide_out_bottom)
    }

}