package com.itljw.openeyes.util

import io.reactivex.FlowableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by JW on on 2018/7/15 16:58.
 * Email : 1481013718@qq.com
 * Description :
 */
object RxSchedulers {

    fun <T> ioMain(): FlowableTransformer<T, T> {
        return FlowableTransformer {
            it.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }

}