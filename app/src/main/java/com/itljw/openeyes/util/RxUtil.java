package com.itljw.openeyes.util;


import com.itljw.openeyes.http.HttpResult;
import com.itljw.openeyes.http.exception.ApiException;

import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by JW on 2017/12/29 12:13
 * Email : 1481013718@qq.com
 * Description : rx工具类
 */
public class RxUtil {

    /**
     * 调度处理
     *
     * @param <T> T
     * @return Publish
     */
    public static <T> FlowableTransformer<T, T> schedulersHelper() {
        return (Flowable<T> flowable) -> flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * 调度处理和提取data数据
     *
     * @param <T> T
     * @return Publish
     */
    public static <T> FlowableTransformer<HttpResult<T>, T> schedulersAndSwitchHelper() {
        return (flowable) -> flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(httpResult -> {
                    int code = httpResult.getCode();
                    if (200 == code) {
                        throw new ApiException(httpResult.getCode(), httpResult.getMsg());
                    }
                    return httpResult.getData();
                });
    }

    /**
     * 过滤数据 错误code抛异常
     * @param <T> T
     * @return 返回是否放行 true为放行 false为拦截
     */
    public static <T> Predicate<HttpResult<T>> filterData() {
        return httpResult->{
            int code = httpResult.getCode();
            if (200 != code) {
                throw new ApiException(httpResult.getCode(), httpResult.getMsg());
            }
            return true;
        };
    }


}
