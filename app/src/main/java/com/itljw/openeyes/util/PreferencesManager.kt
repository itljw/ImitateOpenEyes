package com.itljw.openeyes.util

/**
 * Created by JW on on 2018/9/8 16:24.
 * Email : 1481013718@qq.com
 * Description :
 */
interface PreferencesManager {

    /**
     * 获取搜索关键字
     */
    fun getSearchHistoryKeyword(): MutableList<String>

    /**
     * 存搜索关键字
     */
    fun putSearchHistoryKeyword(keyword: String)

    /**
     * 清空搜索关键字
     */
    fun clearSearchHistoryKeyword()

    /**
     * 保存错误日志
     */
    fun saveErrorInfo(errorInfo: String)

    /**
     * 获取错误日志
     */
    fun getErrorInfo(): String

    /**
     * 获取悬浮窗开启状态
     */
    fun getOverlayWindowStatus(): Boolean

    /**
     * 保存悬浮窗开启状态
     */
    fun saveOverlayWindowStatus(isOpen: Boolean)

}