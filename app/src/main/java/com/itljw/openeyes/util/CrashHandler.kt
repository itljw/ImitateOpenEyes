package com.itljw.openeyes.util

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Environment
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.itljw.openeyes.AppContext
import com.itljw.openeyes.http.DataManager
import com.itljw.openeyes.ui.mine.ErrorInfoActivity
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


/**
 * Created by JW on on 2018/9/2 17:33.
 * Email : 1481013718@qq.com
 * Description :
 */
class CrashHandler private constructor() : Thread.UncaughtExceptionHandler {

    // 获取系统默认的 UncaughtException 处理器
    private val mDefaultHandler: Thread.UncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()

    // 程序的 Context 对象
    private var mContext: Context? = null

    // 用来存储设备信息和异常信息
    private val crashInfos = HashMap<String, String>()

    // 用于格式化日期,作为日志文件名的一部分
    @SuppressLint("SimpleDateFormat")
    private val formatter = SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")

    private var mTips: String = ""
    private val stringBuffer by lazy { StringBuffer() }

    // 重启后跳转的Activity
    private var mRestartActivity: Class<*>? = null

    init {
        // 设置该 CrashHandler 为程序的默认处理器
        Thread.setDefaultUncaughtExceptionHandler(this)
    }

    companion object {

        val TAG = CrashHandler::class.java.simpleName!!

        val instance by lazy {
            CrashHandler()
        }
    }

    /**
     * 初始化CrashHandler
     */
    fun init(
            mContext: Context,
            restartActivity: Class<*> = ErrorInfoActivity::class.java,
            tip: String = "很抱歉，程序出现异常，即将退出..."
    ) {
        mRestartActivity = restartActivity
        this.mContext = mContext
        mTips = tip
    }


    /**
     * 当 UncaughtException 发生时会转入该函数来处理
     */
    @SuppressLint("WrongConstant")
    override fun uncaughtException(t: Thread?, e: Throwable?) {
        if (!handleException(e)) {
            // 如果用户没有处理则让系统默认的异常处理器来处理
            mDefaultHandler.uncaughtException(t, e)
        } else {

            val errorInfo = stringBuffer.toString()
            DataManager.saveErrorInfo(errorInfo) // 保存错误日志

            // 延迟一秒后重启应用
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                Log.e(CrashHandler::class.java.simpleName, "error : ", e)
            }

            val intent = Intent(mContext?.applicationContext, mRestartActivity)
            intent.putExtra(ErrorInfoActivity.EXTRA_INFO, errorInfo)
            val mAlarmManager = mContext?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            //重启应用，得使用PendingIntent
            val restartIntent = PendingIntent.getActivity(
                    mContext?.applicationContext, 0, intent,
                    Intent.FLAG_ACTIVITY_NEW_TASK)

            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> //Android6.0以上，包含6.0
                    mAlarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, System.currentTimeMillis() + 200, restartIntent) //解决Android6.0省电机制带来的不准时问题

                Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT -> //Android4.4到Android6.0之间，包含4.4
                    mAlarmManager.setExact(AlarmManager.RTC, System.currentTimeMillis() + 200, restartIntent) // 解决set()在api19上不准时问题

                else -> mAlarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 200, restartIntent)
            }
        }
        // 结束应用
        AppContext.exit()

    }

    private fun handleException(e: Throwable?): Boolean {
        if (e == null) {
            return false
        }

        // 使用 Toast 来显示异常信息
        thread{
            Looper.prepare()
            Toast.makeText(mContext, getTips(e), Toast.LENGTH_LONG).show()
            Looper.loop()
        }

        // 收集设备参数信息
        collectDeviceInfo(mContext)
        // 保存日志文件
        saveCrashInfo2File(e)
        //删除超过5个的日志文件
        delMoreThan5LogFile()

        return true
    }

    private fun getTips(e: Throwable): String {
        if (e is SecurityException) {
            e.message?.let {
                mTips = if (it.contains("android.permission.CAMERA")) {
                    "请授予应用相机权限，程序出现异常，即将退出."
                } else if (it.contains("android.permission.RECORD_AUDIO")) {
                    "请授予应用麦克风权限，程序出现异常，即将退出。"
                } else if (it.contains("android.permission.WRITE_EXTERNAL_STORAGE")) {
                    "请授予应用存储权限，程序出现异常，即将退出。"
                } else if (it.contains("android.permission.READ_PHONE_STATE")) {
                    "请授予应用电话权限，程序出现异常，即将退出。"
                } else if (it.contains("android.permission.ACCESS_COARSE_LOCATION") || it.contains("android.permission.ACCESS_FINE_LOCATION")) {
                    "请授予应用位置信息权，很抱歉，程序出现异常，即将退出。"
                } else {
                    "很抱歉，程序出现异常，即将退出，请检查应用权限设置。"
                }
            }
        }
        return mTips
    }

    /**
     * 收集设备参数信息
     *
     * @param ctx
     */
    private fun collectDeviceInfo(ctx: Context?) {
        try {
            val pm = ctx?.packageManager
            val pi = pm?.getPackageInfo(ctx.packageName,
                    PackageManager.GET_ACTIVITIES)

            if (pi != null) {
                val versionName = if (pi.versionName == null)
                    "null"
                else
                    pi.versionName
                val versionCode = pi.versionCode.toString() + ""
                crashInfos["versionName"] = versionName
                crashInfos["versionCode"] = versionCode
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e(TAG, "an error occured when collect package info", e)
        }

        val fields = Build::class.java.declaredFields
        for (field in fields) {
            try {
                field.isAccessible = true
                crashInfos[field.name] = field.get(null).toString()
                Log.d(TAG, field.name + " : " + field.get(null))
            } catch (e: Exception) {
                Log.e(TAG, "an error occured when collect crash info", e)
            }

        }
    }

    /**
     * 保存错误信息到文件中 *
     *
     * @param ex
     * @return 返回文件名称, 便于将文件传送到服务器
     */
    private fun saveCrashInfo2File(ex: Throwable): String? {

        stringBuffer.delete(0, stringBuffer.length)
        for ((key, value) in crashInfos) {
            stringBuffer.append("$key=$value\n")
        }

        val writer = StringWriter()
        val printWriter = PrintWriter(writer)
        ex.printStackTrace(printWriter)
        var cause: Throwable? = ex.cause
        while (cause != null) {
            cause.printStackTrace(printWriter)
            cause = cause.cause
        }
        printWriter.close()

        val result = writer.toString()
        stringBuffer.append(result)
        try {
            val timestamp = System.currentTimeMillis()
            val time = formatter.format(Date())
            val fileName = "crash-$time-$timestamp.log"

            if (android.os.Environment.getExternalStorageState() == android.os.Environment.MEDIA_MOUNTED) {
                val path = Environment.getExternalStorageDirectory().path + "/" + mContext?.packageName + "/crash/"
                val dir = File(path)

                // 6.0以上需要动态申请权限
                if (!dir.exists()) {
                    dir.mkdirs()
                }
                val fos = FileOutputStream(path + fileName)
                fos.write(stringBuffer.toString().toByteArray())
                fos.close()
            }

            return fileName
        } catch (e: Exception) {
            Log.e(TAG, "an error occured while writing file...", e)
        }

        return null
    }

    //只保留最近的5个log文件,超过的删除以前的
    private fun delMoreThan5LogFile() {
        if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            val path = Environment.getExternalStorageDirectory().path + "/" + mContext?.packageName + "/crash/"
            val dir = File(path)
            val logs = dir.listFiles { _, filename -> filename.startsWith("crash-") && filename.endsWith("log") }
            if (logs!!.size > 5) {
                Arrays.sort(logs) { lhs, rhs ->
                    val lhsNames = lhs.name.replace(".log", "").split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val rhsNames = rhs.name.replace(".log", "").split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (lhsNames.size == 3 && rhsNames.size == 3) {
                        lhsNames[2].compareTo(rhsNames[2])
                    } else 0
                }
                //保留5个
                var i = 0
                val len = logs.size - 5
                while (i < len) {

                    logs[i].delete()
                    i++
                }
            }
        }
    }


}