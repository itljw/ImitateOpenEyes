package com.itljw.openeyes.util

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.support.annotation.ColorRes
import android.support.v4.app.ActivityCompat
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.itljw.openeyes.GlideApp
import com.itljw.openeyes.R
import com.itljw.openeyes.wiget.GlideCircleTransform
import com.itljw.openeyes.wiget.GlideRoundTransform


/**
 * Created by JW on on 2018/7/16 20:44.
 * Email : 1481013718@qq.com
 * Description :
 */
object GlideUtil {

    /**
     * 加载图片
     *
     * @param url       图片url
     * @param imageView 控件
     */
    fun displayImage(mContext: Context, url: String, imageView: ImageView) {

        if (checkDestroy(mContext)) {
            return
        }

        GlideApp.with(mContext)
                .load(url)
                .placeholder(R.drawable.default_place_holder)
                .error(R.drawable.default_place_holder)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(imageView)

    }

    /**
     * 加载图片 可设置占位图
     *
     * @param url           图片url
     * @param imageView     控件
     * @param placeholderId 占位图
     */
    fun displayImage(mContext: Context, url: String, imageView: ImageView, placeholderId: Int) {

        GlideApp.with(mContext)
                .load(url)
                .placeholder(placeholderId)
                .error(placeholderId)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView)
    }

    fun displayImageWithColor(mContext: Context, url: String, imageView: ImageView,
                              @ColorRes colorRes: Int = R.color.color_line) {
        // 占位图颜色
        val placeDrawable = ColorDrawable(ActivityCompat.getColor(mContext, colorRes))

        GlideApp.with(mContext)
                .load(url)
                .placeholder(placeDrawable)
                .error(placeDrawable)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView)
    }

    /**
     * 加载图片 可设置占位图
     *
     * @param url           图片url
     * @param imageView     控件
     * @param placeholderId 占位图
     * @param imageWidth    图片宽度
     * @param imageHeight   图片高度
     */
    fun displayImage(mContext: Context, url: String, imageView: ImageView, placeholderId: Int, imageWidth: Int, imageHeight: Int) {

        GlideApp.with(mContext)
                .load(url)
                .placeholder(placeholderId)
                .error(placeholderId)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(imageWidth, imageHeight)
                .into(imageView)
    }

    /**
     * 加载图片
     *
     * @param resId     图片资源id
     * @param imageView 控件
     */
    fun displayImage(mContext: Context, resId: Int, imageView: ImageView) {

        GlideApp.with(mContext).load(resId).into(imageView)
    }

    /**
     * 加载图片 不进行缓存
     *
     * @param url       图片url
     * @param imageView 控件
     */
    fun displayNoCacheImage(mContext: Context, url: String, imageView: ImageView) {

        GlideApp.with(mContext)
                .load(url)
                .placeholder(R.drawable.default_place_holder)
                .error(R.drawable.default_place_holder)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView)
    }

    /**
     * 加载图片 不进行缓存
     *
     * @param url           图片url
     * @param imageView     控件
     * @param placeholderId 占位图id
     */
    fun displayNoCacheImage(mContext: Context, url: String, imageView: ImageView, placeholderId: Int) {

        GlideApp.with(mContext)
                .load(url)
                .placeholder(placeholderId)
                .error(placeholderId)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView)
    }

    /**
     * 加载圆形图片 不进行缓存
     *
     * @param url           图片url
     * @param imageView     控件
     * @param placeholderId 占位图id
     */
    fun displayNoCacheRoundImage(mContext: Context, url: String, imageView: ImageView, placeholderId: Int) {

        GlideApp.with(mContext)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .placeholder(placeholderId)
                .error(placeholderId)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView)
    }

    /**
     * 加载圆形图片
     *
     * @param url       图片url
     * @param imageView 控件
     */
    fun displayCircleImage(mContext: Context, url: String, imageView: ImageView) {

        if (checkDestroy(mContext)) {
            return
        }

        GlideApp.with(mContext).load(url).apply(RequestOptions.circleCropTransform()).dontAnimate().skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView)
    }


    fun displayCircleImageWithBorder(mContext: Context, url: String, imageView: ImageView, borderWidth: Int = 1, colorId: Int = R.color.color_F6F6F6) {

        if (checkDestroy(mContext)) {
            return
        }

        GlideApp.with(mContext).load(url).transform(
                GlideCircleTransform(borderWidth, mContext.resources.getColor(colorId))).dontAnimate().into(imageView)
    }

    /**
     * 加载圆角图片
     *
     * @param url           图片url
     * @param imageView     imageView
     * @param radius        圆角半径
     * @param isMemoryCache 是否进行内存缓存 true为缓存 false为不缓存
     */
    fun displayRoundImage(mContext: Context, url: String, imageView: ImageView, radius: Int, isMemoryCache: Boolean) {

        if (checkDestroy(mContext)) {
            return
        }

        GlideApp.with(mContext)
                .load(url)
                .placeholder(R.drawable.default_place_holder)
                .error(R.drawable.default_place_holder)
                .transform(GlideRoundTransform(radius))
                .skipMemoryCache(!isMemoryCache)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(imageView)
    }

    /**
     * 加载圆角图片
     *
     * @param url           图片url
     * @param imageView     imageView
     * @param radius        圆角半径
     * @param isMemoryCache 是否进行内存缓存 true为缓存 false为不缓存
     * @param colorRes      占位图颜色值
     */
    fun displayRoundImage(mContext: Context, url: String, imageView: ImageView, radius: Int,
                          isMemoryCache: Boolean, @ColorRes colorRes: Int = R.color.color_line) {

        if (checkDestroy(mContext)) {
            return
        }

        // 占位图颜色
        val placeDrawable = ColorDrawable(ActivityCompat.getColor(mContext, colorRes))

        GlideApp.with(mContext)
                .load(url)
                .placeholder(placeDrawable)
                .error(placeDrawable)
                .transform(GlideRoundTransform(radius))
                .skipMemoryCache(!isMemoryCache)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView)
    }

    /**
     * 加载图片 监听
     *
     * @param url          图片url
     * @param simpleTarget 加载监听
     */
    fun displayImageWithListener(mContext: Context, url: String, simpleTarget: SimpleTarget<Bitmap>, isMemoryCache: Boolean = true) {

        if (checkDestroy(mContext)) {
            return
        }

        GlideApp.with(mContext).asBitmap().load(url).skipMemoryCache(!isMemoryCache).into(simpleTarget)
    }

    /**
     * 加载图片 监听、可设置占位图
     *
     * @param url           图片url
     * @param placeholderId 占位图id
     * @param simpleTarget  加载监听
     */
    fun displayImageWithListener(mContext: Context, url: String, placeholderId: Int, simpleTarget: SimpleTarget<Bitmap>) {

        GlideApp.with(mContext).asBitmap().load(url).error(placeholderId).placeholder(placeholderId).into(simpleTarget)
    }

    /**
     * 加载图片 不缓存 带监听
     *
     * @param url           图片url
     * @param placeholderId 占位图id
     * @param simpleTarget  记载监听
     */
    fun displayNoCacheImageWithListener(mContext: Context, url: String, placeholderId: Int, simpleTarget: SimpleTarget<Bitmap>) {

        GlideApp.with(mContext).asBitmap().load(url).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).error(placeholderId).placeholder(placeholderId).into(simpleTarget)
    }

    /**
     * 清除内存缓存
     */
    fun clearMemory(mContext: Context) {
        GlideApp.get(mContext).clearMemory()
    }

    /**
     * 清除磁盘缓存
     */
    fun clearDiskCache(mContext: Context) {
        Thread { GlideApp.get(mContext).clearDiskCache() }.start()
    }

    /**
     * 停止Glide请求
     */
    fun pauseRequests(mContext: Context) {
        GlideApp.with(mContext).pauseRequests()
    }

    /**
     * 重新请求
     */
    fun resumeRequests(mContext: Context) {
        GlideApp.with(mContext).resumeRequests()
    }

    private fun checkDestroy(mContext: Context): Boolean {
        if (mContext is Activity) {
            if (mContext.isFinishing) {
                return true
            }
        }
        return false
    }


}