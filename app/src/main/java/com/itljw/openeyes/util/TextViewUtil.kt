package com.itljw.openeyes.util

import android.os.Build
import android.support.annotation.RequiresApi
import android.text.Layout
import android.text.StaticLayout
import android.text.TextDirectionHeuristics
import android.widget.TextView

/**
 * Created by JW on on 2019/5/23 21:23.
 * Email : 1481013718@qq.com
 * Description :
 */
object TextViewUtil {

    /**
     * sdk>=23
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    fun getStaticLayout23(textView: TextView, width: Int): StaticLayout {
        val builder = StaticLayout.Builder.obtain(
                textView.text,
                0, textView.text.length, textView.paint, width
        )
                .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                .setTextDirection(TextDirectionHeuristics.FIRSTSTRONG_LTR)
                .setLineSpacing(textView.lineSpacingExtra, textView.lineSpacingMultiplier)
                .setIncludePad(textView.includeFontPadding)
                .setBreakStrategy(textView.breakStrategy)
                .setHyphenationFrequency(textView.hyphenationFrequency)
                .setMaxLines(if (textView.maxLines == -1) Integer.MAX_VALUE else textView.maxLines)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setJustificationMode(textView.justificationMode)
        }
        if (textView.ellipsize != null && textView.keyListener == null) {
            builder.setEllipsize(textView.ellipsize)
                    .setEllipsizedWidth(width)
        }
        return builder.build()
    }

    fun getStaticLayout(textView: TextView, width: Int): StaticLayout {
        return StaticLayout(
                textView.text,
                0, textView.text.length,
                textView.paint, width, Layout.Alignment.ALIGN_NORMAL,
                textView.lineSpacingMultiplier,
                textView.lineSpacingExtra, textView.includeFontPadding, textView.ellipsize,
                width
        )
    }

}