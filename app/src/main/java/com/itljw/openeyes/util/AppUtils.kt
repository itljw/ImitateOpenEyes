package com.itljw.openeyes.util

import android.content.Context

/**
 * Created by JW on on 2018/7/9 23:23.
 * Email : 1481013718@qq.com
 * Description :
 */
object AppUtils {

    /**
     * 获取版本名
     */
    fun getVersionName(mContext: Context): String {
        return mContext.packageManager.getPackageInfo(mContext.packageName, 0).versionName
    }

    /**
     * 获取手机型号
     */
    fun getPhoneModel(): String? {
        return android.os.Build.MODEL
    }

}