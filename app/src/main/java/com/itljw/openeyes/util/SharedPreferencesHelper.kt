package com.itljw.openeyes.util

import android.content.Context
import com.itljw.openeyes.AppContext

/**
 * Created by JW on on 2018/9/8 15:31.
 * Email : 1481013718@qq.com
 * Description :
 */
object SharedPreferencesHelper {

    private val sharedPreferences by lazy {
        AppContext.mContext.getSharedPreferences("config", Context.MODE_PRIVATE)
    }

    /**
     * 存String类型
     */
    fun putString(key: String, value: String) {
        sharedPreferences.edit()
                .putString(key, value)
                .apply()
    }

    /**
     * 获取String类型
     */
    fun getString(key: String, defValue: String): String =
            sharedPreferences.getString(key, defValue)

    /**
     * 存Int类型
     */
    fun putInt(key: String, value: Int) {
        sharedPreferences.edit()
                .putInt(key, value)
                .apply()
    }

    /**
     * 获取Int类型
     */
    fun getInt(key: String, defValue: Int): Int =
            sharedPreferences.getInt(key, defValue)

    /**
     * 存Long类型
     */
    fun putLong(key: String, value: Long) {
        sharedPreferences.edit()
                .putLong(key, value)
                .apply()
    }

    /**
     * 获取Long类型
     */
    fun getLong(key: String, defValue: Long): Long =
            sharedPreferences.getLong(key, defValue)

    /**
     * 存Boolean类型
     */
    fun putBoolean(key: String, defValue: Boolean) =
            sharedPreferences.edit()
                    .putBoolean(key, defValue)
                    .apply()

    /**
     * 获取Boolean类型
     */
    fun getBoolean(key: String, defValue: Boolean = false) =
        sharedPreferences.getBoolean(key, defValue)

    /**
     * 清除数据
     */
    fun clear() {
        sharedPreferences.edit().clear().apply()
    }

}