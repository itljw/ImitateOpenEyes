package com.itljw.openeyes.util

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by JW on on 2018/7/16 22:27.
 * Email : 1481013718@qq.com
 * Description :
 */
object DateUtil {

    private const val DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss"

    fun millis2String(pattern: String = DEFAULT_PATTERN, millis: Long): String =
            SimpleDateFormat(pattern, Locale.getDefault()).format(Date(millis))

    fun formatIsTodayDate(millis: Long): String {
        // 设置时间
        var pattern = "yyyy/MM/dd"
        if (DateUtils.isToday(millis)) {  // 今天
            pattern = "HH:mm"
        }
      return  millis2String(pattern, millis)
    }

}