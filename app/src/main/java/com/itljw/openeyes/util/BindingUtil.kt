package com.itljw.openeyes.util

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.orhanobut.logger.Logger

/**
 * Created by JW on on 2018/7/20 00:45.
 * Email : 1481013718@qq.com
 * Description :
 */

@BindingAdapter("imageUrl")
fun loadCircleImage(imageView: ImageView, url: String) {
    Logger.d("loadCircleImage:$url")
    GlideUtil.displayCircleImage(imageView.context, url, imageView)
}

// 唉，多个参数不知道怎么搞... cannot find the setter for attribute 'app:imageWithBorderUrl' with parameter type java.lang.String on android.widget.ImageView
@BindingAdapter("imageWithBorderUrl")
fun loadCircleImageWithBorder(imageView: ImageView, url: String) {
    Logger.d("loadCircleImageWithBorder:$url")
    GlideUtil.displayCircleImageWithBorder(imageView.context, url, imageView)
}
