package com.itljw.openeyes.util

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.entity.*
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.ui.home.entity.Item

/**
 * Created by JW on on 2018/9/22 19:47.
 * Email : 1481013718@qq.com
 * Description :
 */
object CategoryDealDataHelper {

    fun dealData(itemList: List<Item>): List<MultiItemEntity> {
        val multiItemList = arrayListOf<MultiItemEntity>()
        for (item in itemList) {
            when (item.type) {
                HomeConstant.TYPE_TEXT_CARD -> multiItemList.add(TextCardMultiItem(item.data))

                HomeConstant.TYPE_FOLLOW_CARD ->
                    multiItemList.add(FollowCardMultiItem(item.data.header, item.data.content))

                HomeConstant.TYPE_VIDEO_SMALL_CARD ->
                    multiItemList.add(VideoSmallCardMultiItem(item.data))

                HomeConstant.TYPE_SQUARE_CARD_COLLECTION ->
                    multiItemList.add(SquareCardMultiItem(item.data.header, item.data.itemList))

                HomeConstant.TYPE_BANNER -> multiItemList.add(BannerMultiItem(item.data))

                HomeConstant.TYPE_PICTURE_FOLLOW_CARD ->
                    multiItemList.add(PictureFollowCardMultiItem(item.data))

                HomeConstant.TYPE_HORIZONTAL_SCROLL_CARD ->
                    multiItemList.add(HorizontalScrollCardMultiItem(item.data.itemList))

                HomeConstant.TYPE_BRIEF_CARD ->
                    multiItemList.add(BriefCardMultiItem(item.data))

                HomeConstant.TYPE_VIDEO_COLLECTION_WITH_BRIEF ->
                    multiItemList.add(VideoCollectionWithBriefMultiItem(item.data.header, item.data.itemList))

                HomeConstant.TYPE_DYNAMIC_INFO_CARD ->
                    multiItemList.add(DynamicInfoCardMultiItem(item.data))

                HomeConstant.TYPE_AUTO_PLAY_FOLLOW_CARD ->
                    multiItemList.add(AutoPlayFollowCardMultiItem(item.data.header, item.data.content))

                HomeConstant.TYPE_REPLY -> multiItemList.add(ReplyMultiItem(item.data))
            }
        }
        return multiItemList
    }

}