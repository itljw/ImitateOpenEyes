package com.itljw.openeyes.util

import android.graphics.Typeface
import com.itljw.openeyes.AppContext

/**
 * Created by JW on on 2018/7/22 12:05.
 * Email : 1481013718@qq.com
 * Description :
 */
object FontUtil {
    fun getTypefaceByType(type: String): Typeface? {

        return Typeface.createFromAsset(AppContext.mContext.assets,
                when (type) {
                    "lobster" -> "fonts/Lobster-1.4.otf"
                    "bigBold" -> "fonts/DIN-Condensed-Bold.ttf"
                    else -> "fonts/FZLanTingHeiS-DB1-GB-Regular.TTF"
                })
    }
}