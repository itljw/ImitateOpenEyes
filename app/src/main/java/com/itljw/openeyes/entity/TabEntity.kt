package com.itljw.openeyes.entity

import com.flyco.tablayout.listener.CustomTabEntity

/**
 * Created by JW on on 2018/7/13 21:04.
 * Email : 1481013718@qq.com
 * Description :
 */
class TabEntity(
        val title: String,
        private val selectedIconId: Int,
        private val unSelectedIconId: Int
) : CustomTabEntity {

    override fun getTabUnselectedIcon(): Int = unSelectedIconId

    override fun getTabSelectedIcon(): Int = selectedIconId

    override fun getTabTitle(): String = title
}