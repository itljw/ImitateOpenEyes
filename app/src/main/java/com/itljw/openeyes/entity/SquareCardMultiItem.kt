package com.itljw.openeyes.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.constants.HomeConstant.ITEM_TYPE_SQUARE_CARD
import com.itljw.openeyes.ui.home.entity.Header
import com.itljw.openeyes.ui.home.entity.Item

/**
 * Created by JW on on 2018/7/21 22:18.
 * Email : 1481013718@qq.com
 * Description :
 */
class SquareCardMultiItem(val header: Header, val itemList: List<Item>) : MultiItemEntity {

    override fun getItemType(): Int {
        return ITEM_TYPE_SQUARE_CARD
    }

}