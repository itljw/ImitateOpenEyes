package com.itljw.openeyes.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.ui.home.entity.Data

/**
 * Created by JW on on 2018/7/22 21:06.
 * Email : 1481013718@qq.com
 * Description :
 */
class DynamicInfoCardMultiItem(val data: Data) : MultiItemEntity {
    override fun getItemType(): Int {
        return HomeConstant.ITEM_TYPE_DYNAMIC_INFO_CARD
    }

}