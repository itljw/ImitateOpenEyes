package com.itljw.openeyes.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.constants.HomeConstant.ITEM_TYPE_FOLLOW_CARD
import com.itljw.openeyes.ui.home.entity.Content
import com.itljw.openeyes.ui.home.entity.Header

/**
 * Created by JW on on 2018/7/21 22:26.
 * Email : 1481013718@qq.com
 * Description :
 */
class FollowCardMultiItem(val header: Header, val content: Content) : MultiItemEntity {

    override fun getItemType(): Int {
        return ITEM_TYPE_FOLLOW_CARD
    }

}