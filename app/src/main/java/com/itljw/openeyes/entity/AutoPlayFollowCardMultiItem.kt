package com.itljw.openeyes.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.ui.home.entity.Content
import com.itljw.openeyes.ui.home.entity.Header

/**
 * Created by JW on on 2018/7/24 22:07.
 * Email : 1481013718@qq.com
 * Description :
 */
class AutoPlayFollowCardMultiItem(val header: Header, val content: Content) : MultiItemEntity {
    override fun getItemType(): Int {
        return HomeConstant.ITEM_TYPE_AUTO_PLAY_FOLLOW_CARD
    }

}