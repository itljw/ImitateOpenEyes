package com.itljw.openeyes.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.ui.home.entity.Data

/**
 * Created by JW on on 2018/7/22 20:47.
 * Email : 1481013718@qq.com
 * Description :
 */
class BriefCardMultiItem(val data: Data) : MultiItemEntity {
    override fun getItemType(): Int {
        return HomeConstant.ITEM_TYPE_BRIEF_CARD
    }
}