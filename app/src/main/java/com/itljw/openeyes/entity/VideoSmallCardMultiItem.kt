package com.itljw.openeyes.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.constants.HomeConstant.ITEM_TYPE_VIDEO_SMALL_CARD
import com.itljw.openeyes.ui.home.entity.Data

/**
 * Created by JW on on 2018/7/21 22:27.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoSmallCardMultiItem(val data: Data) : MultiItemEntity {

    override fun getItemType(): Int {
        return ITEM_TYPE_VIDEO_SMALL_CARD
    }

}