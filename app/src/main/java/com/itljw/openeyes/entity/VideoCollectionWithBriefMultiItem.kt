package com.itljw.openeyes.entity

import com.chad.library.adapter.base.entity.MultiItemEntity
import com.itljw.openeyes.constants.HomeConstant
import com.itljw.openeyes.ui.home.entity.Header
import com.itljw.openeyes.ui.home.entity.Item

/**
 * Created by JW on on 2018/7/22 20:49.
 * Email : 1481013718@qq.com
 * Description :
 */
class VideoCollectionWithBriefMultiItem(val header: Header, val itemList: List<Item>) :MultiItemEntity {
    override fun getItemType(): Int {
        return HomeConstant.ITEM_TYPE_VIDEO_COLLECTION_WITH_BRIEF
    }
}