package com.itljw.openeyes

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.HttpException
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.util.ContentLengthInputStream
import com.bumptech.glide.util.Synthetic
import okhttp3.*
import java.io.IOException
import java.io.InputStream
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

/**
 * Created by JW on on 2018/7/16 20:37.
 * Email : 1481013718@qq.com
 * Description : https图片证书忽略
 */
@GlideModule
class MyGlideModule : AppGlideModule() {

    /**
     * 如果你直接使用的V4，建议重写该方法，返回false。避免再次解析Manifest了
     * 因为V4现在采用了注解的方式，而不是之前的在Manifest中配置
     */
    override fun isManifestParsingEnabled(): Boolean = false

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {

        registry.replace(GlideUrl::class.java, InputStream::class.java, OkHttpUrlLoader.Factory(getOkHttpClient()))

    }


    /**
     * 获取忽略证书的OkHttpClient
     */
    private fun getOkHttpClient(): OkHttpClient? {

        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, getTrustManager(), SecureRandom())

        return OkHttpClient.Builder().sslSocketFactory(sslContext.socketFactory, getTrustManager()[0])
                .hostnameVerifier { _, _ -> true }.build()

    }

    /**
     * 忽略证书
     */
    @SuppressLint("TrustAllX509TrustManager")
    private fun getTrustManager(): Array<X509TrustManager> {
        val x509TrustManager = object : X509TrustManager {

            override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {}

            override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {}

            override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
        }

        return arrayOf(x509TrustManager)
    }

    class OkHttpUrlLoader(private val client: Call.Factory) : ModelLoader<GlideUrl, InputStream> {

        override fun handles(url: GlideUrl): Boolean {
            return true
        }

        override fun buildLoadData(model: GlideUrl, width: Int, height: Int, options: Options): ModelLoader.LoadData<InputStream>? {
            return ModelLoader.LoadData(model, OkHttpStreamFetcher(client, model))
        }

        class Factory
        /**
         * Constructor for a new Factory that runs requests using given client.
         *
         * @param client this is typically an instance of `OkHttpClient`.
         */
        @JvmOverloads constructor(private val client: Call.Factory? = getInternalClient()) : ModelLoaderFactory<GlideUrl, InputStream> {

            override fun build(multiFactory: MultiModelLoaderFactory): ModelLoader<GlideUrl, InputStream> {
                return OkHttpUrlLoader(client!!)
            }

            override fun teardown() {
                // Do nothing, this instance doesn't own the client.
            }

            companion object {
                @Volatile
                private var internalClient: Call.Factory? = null

                private fun getInternalClient(): Call.Factory? {
                    if (internalClient == null) {
                        synchronized(Factory::class.java) {
                            if (internalClient == null) {
                                internalClient = OkHttpClient()
                            }
                        }
                    }
                    return internalClient
                }
            }
        }
        /**
         * Constructor for a new Factory that runs requests using a static singleton client.
         */
    }

    class OkHttpStreamFetcher(private val client: Call.Factory, private val url: GlideUrl) : DataFetcher<InputStream> {
        @Synthetic
        internal var stream: InputStream? = null
        @Synthetic
        internal var responseBody: ResponseBody? = null
        @Volatile
        private var call: Call? = null

        override fun loadData(priority: Priority, callback: DataFetcher.DataCallback<in InputStream>) {
            val requestBuilder = Request.Builder().url(url.toStringUrl())
            for ((key, value) in url.headers) {
                requestBuilder.addHeader(key, value)
            }
            val request = requestBuilder.build()

            call = client.newCall(request)
            call!!.enqueue(object : okhttp3.Callback {
                override fun onFailure(call: Call, e: IOException) {
                    if (Log.isLoggable(TAG, Log.DEBUG)) {
                        Log.d(TAG, "OkHttp failed to obtain result", e)
                    }
                    callback.onLoadFailed(e)
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call, response: Response) {
                    responseBody = response.body()
                    if (response.isSuccessful) {
                        val contentLength = responseBody!!.contentLength()
                        stream = ContentLengthInputStream.obtain(responseBody!!.byteStream(), contentLength)
                        callback.onDataReady(stream)
                    } else {
                        callback.onLoadFailed(HttpException(response.message(), response.code()))
                    }
                }
            })
        }

        override fun cleanup() {
            try {
                if (stream != null) {
                    stream!!.close()
                }
            } catch (e: IOException) {
                // Ignored
            }

            if (responseBody != null) {
                responseBody!!.close()
            }
        }

        override fun cancel() {
            val local = call
            local?.cancel()
        }

        override fun getDataClass(): Class<InputStream> {
            return InputStream::class.java
        }

        override fun getDataSource(): DataSource {
            return DataSource.REMOTE
        }

        companion object {
            private const val TAG = "OkHttpFetcher"
        }
    }


}