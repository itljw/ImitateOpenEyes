import android.app.Activity
import android.app.Service
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.itljw.openeyes.AppContext
import com.itljw.openeyes.util.TextViewUtil
import java.io.File
import java.io.FileOutputStream


/**
 * Created by JW on on 2018/7/11 00:10.
 * Email : 1481013718@qq.com
 * Description : kotlin 扩展function
 */

/*********************************      View    *****************************************/

/**
 * Extension method to provide simpler access to {@link View#getResources()#getString(int)}.
 */
fun View.getString(stringResId: Int): String = resources.getString(stringResId)

/**
 * Extension method to show a keyboard for View.
 */
fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}

/**
 * Try to hide the keyboard and returns whether it worked
 * https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
 */
fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
    }
    return false
}

/**
 * Extension method to set View's height.
 */
fun View.setHeight(value: Int) {
    val lp = layoutParams
    lp?.let {
        lp.height = value
        layoutParams = lp
    }
}

/**
 * Extension method to set View's width.
 */
fun View.setWidth(value: Int) {
    val lp = layoutParams
    lp?.let {
        lp.width = value
        layoutParams = lp
    }
}

/**
 * 判断View是否在点击范围内
 */
fun View.inRangeOfView(ev: MotionEvent): Boolean {
    val location = IntArray(2)
    getLocationOnScreen(location)
    val x = location[0]
    val y = location[1]
    return !(ev.rawX < x || ev.rawX > x + width || ev.rawY < y || ev.rawY > y + height)
}

/**
 * Set an onclick listener
 */
fun <T : View> T.click(block: (T) -> Unit) = setOnClickListener { block(it as T) }

/**
 * Extension method to set OnClickListener on a view.
 */
fun <T : View> T.longClick(block: (T) -> Boolean) = setOnLongClickListener { block(it as T) }

/**
 * Like findViewById but with type interference, assume the view exists
 */
inline fun <reified T : View> View.find(@IdRes id: Int): T = findViewById(id) as T

/*********************************      Context    *****************************************/

/**
 * Extension method to get LayoutInflater
 */
inline val Context.inflater: LayoutInflater
    get() = LayoutInflater.from(this)

/**
 * Extension method to get a new Intent for an Activity class
 */
inline fun <reified T : Any> Context.intent() = Intent(this, T::class.java)

/**
 * Extension method to startActivity for Context.
 */
inline fun <reified T : Activity> Context?.startActivity() = this?.startActivity(Intent(this, T::class.java))

/**
 * Extension method to start Service for Context.
 */
inline fun <reified T : Service> Context?.startService() = this?.startService(Intent(this, T::class.java))

/**
 * Extension method to show toast for Context.
 */
fun Context?.toast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT) = this?.let { Toast.makeText(it, text, duration).show() }

/**
 * Extension method to show toast for Context.
 */
fun Context?.toast(@StringRes textId: Int, duration: Int = Toast.LENGTH_SHORT) = this?.let { Toast.makeText(it, textId, duration).show() }

/**
 * Extension method to Get Color for resource for Context.
 */
fun Context.getColor(@ColorRes id: Int) = ContextCompat.getColor(this, id)

/**
 * Extension method to Get Drawable for resource for Context.
 */
fun Context.getDrawable(@DrawableRes id: Int) = ContextCompat.getDrawable(this, id)

/**
 * dp2px
 */
fun Context.dp2px(dpValue: Int): Float = resources.displayMetrics.density * dpValue

/**
 * sp2px
 */
fun Context.sp2px(spValue: Int): Float = resources.displayMetrics.density * spValue

/**
 * 获取屏幕高度
 */
fun Context.getScreenHeight(): Int {
    return resources.displayMetrics.heightPixels
}

/**
 * 获取屏幕宽度
 */
fun Context.getScreenWidth(): Int {
    return resources.displayMetrics.widthPixels
}

/*********************************      Fragment    *****************************************/

/**
 * Extension method to display toast text for Fragment.
 */
fun Fragment?.toast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT) = this?.let { activity.toast(text, duration) }

/**
 * Extension method to display toast text for Fragment.
 */
fun Fragment?.toast(@StringRes textId: Int, duration: Int = Toast.LENGTH_SHORT) = this?.let { activity.toast(textId, duration) }

/*********************************      String    *****************************************/

/**
 * Extension method to check if String is Phone Number.
 */
fun String.isPhone(): Boolean {
    val p = "^1([34578])\\d{9}\$".toRegex()
    return matches(p)
}


/**
 * Extension method to check if String is Email.
 */
fun String.isEmail(): Boolean {
    val p = "^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w+)+)\$".toRegex()
    return matches(p)
}

/**
 * Extension method to check if String is Number.
 */
fun String.isNumeric(): Boolean {
    val p = "^[0-9]+$".toRegex()
    return matches(p)
}

/**
 * 是否是英文
 */
fun String.isEnglish(): Boolean {
    val p = "^[a-zA-Z]*".toRegex()
    return matches(p)
}

/**
 * Extension method to save Bitmap to specified file path.
 */
fun Bitmap.saveFile(path: String) {
    val f = File(path)
    if (!f.exists()) {
        f.createNewFile()
    }
    val stream = FileOutputStream(f)
    compress(Bitmap.CompressFormat.PNG, 100, stream)
    stream.flush()
    stream.close()
}

fun Any.appToast(msg: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(AppContext.mContext, msg, duration).show()
}

/**
 * 提前获取TextView的行数
 */
fun TextView.getPredictLineCount(textViewWidth: Int = context.getScreenWidth()): Int {

    val width = textViewWidth - compoundPaddingLeft - compoundPaddingRight

    val lines = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        TextViewUtil.getStaticLayout23(this, width)
    } else {
        TextViewUtil.getStaticLayout(this, width)
    }.lineCount

    return if (maxLines > lines) {
        lines
    } else maxLines
}

/**
 * 滑动监听
 */
fun ViewPager.pagerChange(
        pageScrollStateChanged: (Int) -> Unit = {},
        pageScrolled: (Int, Float, Int) -> Unit = { _, _, _ -> },
        pageSelected: (Int) -> Unit = {}
) {

    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) =
                pageScrollStateChanged.invoke(state)

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) =
                pageScrolled.invoke(position, positionOffset, positionOffsetPixels)

        override fun onPageSelected(position: Int) =
                pageSelected.invoke(position)
    })

}

fun EditText.textChange(
        afterTextChanged: (Editable?) -> Unit = {},
        beforeTextChanged: (CharSequence?, Int, Int, Int) -> Unit = { _, _, _, _ -> },
        onTextChanged: (CharSequence?, Int, Int, Int) -> Unit = { _, _, _, _ -> }
) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            beforeTextChanged.invoke(s, start, count, after)
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChanged.invoke(s, start, before, count)
        }

    })
}

fun Animation.animationListener(
        animationRepeat: (Animation?) -> Unit = {},
        animationEnd: (Animation?) -> Unit = {},
        animationStart: (Animation?) -> Unit = {}
) {
    setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) =
                animationRepeat.invoke(animation)

        override fun onAnimationEnd(animation: Animation?) =
                animationEnd.invoke(animation)

        override fun onAnimationStart(animation: Animation?) =
                animationStart.invoke(animation)

    })
}

inline fun <reified T : ViewModel> ViewModelProvider.get() = this.get(T::class.java)